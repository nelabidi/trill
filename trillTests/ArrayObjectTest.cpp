//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  ArrayObjectTest.cpp
// PURPOSE:
// DATE: 2015/09/01
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common.h"


namespace {

    using namespace trill;

    xdescribe("Array Object test")
    {
        Runtime rt;
        Context* ctx;
        void setUp()
        {
            ctx = nullptr;
        }
        void tearDown()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            ctx = nullptr;
        }
        void beforeEach()
        {
            ctx = rt.NewContext();
        }
        void afterEach()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            ctx = nullptr;
        }
        it("int array wihtout initializer")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int[10] t;for(int i=0;i<10;i++)t[i]=i;for(int i=0;i<10;i++)print(t[i]);")).toBeTrue();
            expect(capture).toContain("0123456789");
        }
        it("Two demension int array wihtout initializer")
        {
            ztest::Capture capture(std::cout);
            expect(ctx->Evaluate("int[3][10] t;for(int i=0;i<3;i++)for(int j=0;j<10;j++){t[i][j] = i*j; print(t[i][j]);}")).toBeTrue();
            expect(capture).toContain("00000000000123456789024681012141618");
        }


    };

    xdescribe("ArrayObject, ArrayType")
    {
        Type *itype, *array;
        Runtime rt;
        Context* ctx;
        TypeManager* tmgr;

        void setUp()
        {
            tmgr = new TypeManager(&rt);
            //int[10]
            ctx = rt.NewContext();
            itype = IntType::instance();
            array = tmgr->newArrayType(itype, 10);
        }
        void tearDown()
        {
            delete tmgr;
            //delete itype;
            //delete array;
        }
        it("create array type and array object of int[2][10]")
        {

            ArrayType* array2 = tmgr->newArrayType(array, 2);
            Object* obj = rt.newArrayObject(ctx, array2);

            expect(obj->is<IntArrayObject>()).toBeTrue();

            obj->as<IntArrayObject>().setAt(19, 1);
            expect(obj->as<IntArrayObject>().getAt(19)).toBeEqual(1);
            //using sub arrays
            Object* sub = obj->as<IntArrayObject>().get(1);
            expect(sub->is<IntArrayObject>()).toBeTrue();

            int i = sub->as<IntArrayObject>().getAt(9);
            expect(i == 1).toBeTrue();

        }

        it("create array type and array object of int[2][2][10]")
        {
            ArrayType* array2 = tmgr->newArrayType(array, 2);
            ArrayType* array3 = tmgr->newArrayType(array2, 2);

            Object* obj = rt.newArrayObject(ctx, array3);
            //simulate [1][1][5]
            expect(obj->is<IntArrayObject>()).toBeTrue();
            //direct access 1 * 20 + 1 * 10 + 5
            obj->as<IntArrayObject>().setAt(35, 5);
            expect(obj->as<IntArrayObject>().getAt(35)).toBeEqual(5);

            //using sub arrays
            Object* sub1 = obj->as<IntArrayObject>().get(1);
            expect(sub1->is<IntArrayObject>()).toBeTrue();

            Object* sub2 = sub1->as<IntArrayObject>().get(1);
            expect(sub2->is<IntArrayObject>()).toBeTrue();

            int i = sub2->as<IntArrayObject>().getAt(5);
            expect(i == 5).toBeTrue();

        }

    };


}