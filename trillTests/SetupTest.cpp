//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  SetupTest.cpp
// PURPOSE:
// DATE: 2015/08/09
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "common.h"
#include <thread>
#include <mutex>


namespace {
    std::mutex  gMutex;
    void mythread()
    {
        int i = 0;
        gMutex.lock();
        std::cout << "Hello from thread!\n";
        gMutex.unlock();
    }
    xdescribe("Setup Test")
    {
        it("Testing the envirenment")
        {
            /* gMutex.lock();
             std::thread t(mythread);
            t.detach();
             gMutex.unlock();*/


            expect(true).toBeEqual(true);
        }
    };
}


