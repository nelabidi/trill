//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  ExecuteTest.cpp
// PURPOSE: Test cases for the context Execute method
// DATE: 2015/11/26
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common.h"

namespace {
    using namespace trill;

    describe("Context")
    {
        Runtime rt;
        Context *ctx;
        void beforeEach()
        {
            ctx = rt.NewContext();
        }
        void afterEach()
        {
            rt.DestroyContext(ctx);
        }

        it("can execute a file")
        {
            std::string outFile(gOutDir);
            outFile += "decl.tr.sym";
            CompileOptions options;
            options.bDumpSymbols = true;
            options.szSymbolFile = outFile.c_str();
            ztest::Capture capture(std::cout);
            expect(ctx->Execute("..\\tests\\decl.tr", options)).toBeTrue();

        }
    };
}