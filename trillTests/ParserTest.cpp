//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  ParserTest.cpp
// PURPOSE:
// DATE: 2015/08/21
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common.h"



namespace {
    using namespace trill;

    xdescribe("Parser")
    {
        Runtime rt;
        //This case I use a file where I have many test cases to check the parser against.
        it("TestParser.ls")
        {
            FILE *f = nullptr;

            if (0 == fopen_s(&f, "../trillTests/scripts/TestParser.ls", "rb"))
            {
                SourceInfo* src = rt.NewSourceInfo("../trillTests/scripts/TestParser.ls", f);
                Context* ctx = rt.NewContext();
                expect(ctx->Evaluate(src)).toBeTrue();
                rt.DestroyContext(ctx);
                rt.DestroySourceInfo(src);
            }
        }
    };
}


