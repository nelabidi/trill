//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill, scripting language
// FILE:  OperatorsTest.cpp
// PURPOSE: Test case for different operators
// DATE: 2015/08/30
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common.h"

namespace {

    using namespace trill;
    int f(int i)
    {
        return i + 1;
    }
    //Assignment test case
    xdescribe("Assignenment Operators Test")
    {
        Runtime rt;
        Context* ctx;

        void setUp()
        {
            ctx = nullptr;
        }
        void tearDown()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            //ctx = nullptr;
        }
        void beforeEach()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            ctx = rt.NewContext();
        }
        void afterEach()
        {
            rt.DestroyContext(ctx);
            ctx = nullptr;
        }

        it("Assignenment")
        {
            //capture the output so we can test the result form the print functioin
            ztest::Capture capture(std::cout);
            //evaluate the script
            const char *script = "int a,b,c = 100; a = b = c; print(a,b,c);";
            expect(ctx->Evaluate(script)).toBeTrue();
            expect(capture).toContain("100, 100, 100");
        }
        it("Composed Assignenment *=")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=101; a*=10; print(a);")).toBeTrue();
            expect(capture).toContain("1010");
        }
        it("Composed Assignenment /=")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=1010; a/=10; print(a);")).toBeTrue();
            expect(capture).toContain("101");
        }
        it("Composed Assignenment -=")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=1010; a-=1; print(a);")).toBeTrue();
            expect(capture).toContain("1009");
        }
        it("Composed Assignenment +=")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=1010; a+=1; print(a);")).toBeTrue();
            expect(capture).toContain("1011");
        }
    };
    //
    xdescribe("++/-- Operators Test")
    {
        Runtime rt;
        Context* ctx;

        void setUp()
        {
            ctx = nullptr;
        }
        void tearDown()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            //ctx = nullptr;
        }
        void beforeEach()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            ctx = rt.NewContext();
        }
        void afterEach()
        {
            rt.DestroyContext(ctx);
            ctx = nullptr;
        }
        it("post inc i++")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=10; print(a++);print(a);")).toBeTrue();
            expect(capture).toContain("1011");
        }
        it("post dec i--")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=10; print(a--);print(a);")).toBeTrue();
            expect(capture).toContain("109");
        }
        it("inc ++i")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=10; print(++a);print(a);")).toBeTrue();
            expect(capture).toContain("1111");
        }
        it("dec --i")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int a=10; print(--a);print(a);")).toBeTrue();
            expect(capture).toContain("99");
        }
        it("inc, post dec ++i--, should fail")
        {
            ztest::Capture capture(std::cerr);
            expect(ctx->Evaluate("int a=10; print(++a--);print(a);")).toBeFalse();

        }
        it("dec, post inc --i++, should fail")
        {
            //std::ios_base::sync_with_stdio(true);
            ztest::Capture capture(std::cerr);
            expect(ctx->Evaluate("int a=10; print(--a++);print(a);")).toBeFalse();

        }

        it("inc, ++f(), should fail")
        {
            //std::ios_base::sync_with_stdio(true);
            ztest::Capture capture(std::cerr);
            expect(ctx->Evaluate("function f(int i) int { return i + 1;} int a=10; a = ++f(1); print(a);")).toBeFalse();
        }
    };

}