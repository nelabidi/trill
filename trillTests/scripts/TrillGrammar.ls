/**
 *  This is a test/grammar for trill
 */
 
/////////////////////////////////////////////////////////
//  Declaration
// 
// typeSpec : var |int | char | uchar | uint | float
// classOrFunction: class | function
/////////////////////////////////////////////////////////
 
//Variables:
// typeSpec  initializerList.
// initializerList: initializerList COMMA assignExpr.
// initializerList: assignExpr
 var a1, b1 = a1 = 1, c1 = b1 + 2;
 int i1 = 13, i2 = i1+1;
 float f1;
 char  ch = 'A';
 uchar uch = ch;
 uint  ui1 = -i1;
 
 var  v = a1.m1 = a1['m1'] + 3;
 
 
 var A =16;
 var a, c = b = 123 + 2,cc,gg = 12;
 
if( A < 0  || A > 15 ){
	a = 2;
	cc = gg + 1;
}else 
{
	if( a > 10)
		gg = 10;
} 
var b =true;
if(b);
 
 1+3;
 a = 1 + 2;
 b = 3.5 + 1;
 a = b = c;
 
function func (int a, b ) int
{
	return 0;
}
function fib (n) int
{
    if (n < 2)
	{
		return  1;
	}
    return fib( n - 2) + fib( n - 1);
};
//this creat unamed function with in as return type!
//int a = function() {}
 
 