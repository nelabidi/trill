//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  LexerTest.cpp
// PURPOSE:
// DATE: 2015/08/20
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "common.h"

#include "../trill/Compiler/grammar.h"
#include "../trill/Compiler/Lexer.h"

namespace {

    using namespace trill;

    describe("Lexer")
    {
        void setUp()
        {

        }
        void tearDown()
        {

        }
        //TODO: FINISH ME add more keywords
        it("int Keyword")
        {
            Lexer lex("int");
            Token tk;
            expect(lex.Next(tk)).toBeEqual(INT_TS);
        }
        it("float Keyword")
        {
            Lexer lex("float");
            Token tk;
            expect(lex.Next(tk)).toBeEqual(FLOAT_TS);
        }

        it("char Keyword")
        {
            Lexer lex("char");
            Token tk;
            expect(lex.Next(tk)).toBeEqual(CHAR_TS);
        }

    };
}

