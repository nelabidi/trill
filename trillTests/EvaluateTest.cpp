//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  ContextEvaluateTest.cpp
// PURPOSE:
// DATE: 2015/08/20
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "common.h"

namespace {
    using namespace trill;

    bool native_print(Context*, Variant* vp, unsigned nargs)
    {
        ASSERT(vp != nullptr);
        ASSERT(vp->isInt() && vp->Int() == 1);

        return true;
    }

    xdescribe("Context Evaluate Method")
    {
        Runtime rt;
        Context* ctx;
        void setUp()
        {
            ctx = rt.NewContext();
        }
        void tearDown()
        {
            rt.DestroyContext(ctx);
        }

        it("Evaluate Empty script")
        {
            expect(ctx->Evaluate("")).toBeTrue();
        }
        it("Evaluate declaration")
        {
            expect(ctx->Evaluate("var v;")).toBeTrue();
            expect(ctx->Evaluate("int i;")).toBeTrue();
            expect(ctx->Evaluate("char c;")).toBeTrue();
        }

        it("detect redefinition")
        {
            ztest::Capture capture(std::cerr);
            expect(ctx->Evaluate("var v;var v;")).toBeFalse();
        }
    };

    xdescribe("Context Native Method")
    {
        Runtime rt;
        Context* ctx;
        void setUp()
        {
            ctx = rt.NewContext();
        }
        void tearDown()
        {
            rt.DestroyContext(ctx);
        }
        it("Register and call a method from the script")
        {
            ctx->RegisterNative("printOne", native_print, 1);
            expect(ctx->Evaluate("printOne(1);")).toBeTrue();
        }
    };

    xdescribe("Context 'print' Method")
    {
        Runtime rt;
        Context* ctx;
        void setUp()
        {
            ctx = rt.NewContext();
        }
        void tearDown()
        {
            rt.DestroyContext(ctx);
        }
        it("has print method")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("print(123);")).toBeTrue();
            expect(capture).toContain("123");
        }
    };

}


