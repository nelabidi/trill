// trillTests.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#if defined _MSC_VER
#include <direct.h>
#elif defined __GNUC__
#include <sys/types.h>
#include <sys/stat.h>
#endif

void makeDir(const char* dir)
{
#if defined _MSC_VER
    _mkdir(dir);
#elif defined __GNUC__
    mkdir(dir, 0777);
#endif
}

const char *gOutDir = "..\\output\\";


int main(int argc, char* argv[])
{
    //make the output folder
    makeDir(gOutDir);
    return ztest::TestRunner::RunAll(argc, argv);
}



