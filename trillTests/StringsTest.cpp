//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  StringsTest.cpp
// PURPOSE:
// DATE: 2015/09/03
// NOTES:
//
//////////////////////////////////////////////////////////////////////////



#include "stdafx.h"
#include "common.h"



namespace {
    using namespace trill;

    describe("Strings test")
    {
        Runtime rt;
        Context* ctx;

        void setUp()
        {
            ctx = nullptr;
        }
        void tearDown()
        {
            if (ctx)
                rt.DestroyContext(ctx);
        }
        void beforeEach()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            ctx = rt.NewContext();
        }
        void afterEach()
        {
            rt.DestroyContext(ctx);
            ctx = nullptr;
        }

        it("single quote string is a char if len = 1")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("char c = 'c'; if(c=='c') print(1); else print(0);")).toBeTrue();
            expect(capture).toContain("1");
        }
        it("single quote string is int if len >1 <5")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int c = 'abc'; if( c == 'abc') print(1); else print(0);")).toBeTrue();
            expect(capture).toContain("1");
        }
        it("single quote string is string type if len 0 or len > 4")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("int c = 'a'; if( c == 'abc') print(1); else print(0);")).toBeTrue();
            expect(capture).toContain("0");
        }
        it("single quote string is string type")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("string s = 'my string'; print(s);")).toBeTrue();
            expect(capture).toContain("my string");
        }
        it("single quote string as parameter")
        {
            ztest::Capture capture(std::cout);

            expect(ctx->Evaluate("print('hello');")).toBeTrue();
            expect(capture).toContain("hello");
        }
    };
}
