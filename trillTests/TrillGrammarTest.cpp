//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  TrillGrammarTest.cpp
// PURPOSE:
// DATE: 2015/08/21
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common.h"

#include <chrono>
#include <iomanip>


class Timer
{
public:

    Timer() : resetTimer(true), dTime(0.0f) {}
    ~Timer() {}

    bool Init();
    void Reset()
    {
        resetTimer = true;
    }

    float Tick();
    float GetDelta()
    {
        return dTime;
    }

private:

    LARGE_INTEGER timerFrequency, lastTime, thisTime;
    float dTime, oof;
    bool resetTimer;
};

bool Timer::Init()
{
    resetTimer = true;
    dTime = 0.0f;
    bool result = QueryPerformanceFrequency(&timerFrequency) > 0;
    oof = 1.0f / ((float)timerFrequency.QuadPart);

    return result;
}

float Timer::Tick()
{
    QueryPerformanceCounter(&thisTime);

    if (resetTimer)
    {
        dTime = 0;
        resetTimer = false;
    }
    else
    {
        dTime = ((float)(thisTime.QuadPart - lastTime.QuadPart)) * oof;
    }
    lastTime = thisTime;

    return dTime;
}



int cputime(void)
{
    // return cpu time used by current process
    FILETIME ct, et, kt, ut;
    //if (GetVersion() < 0x80000000)
    {
        // are we running on recent Windows
        GetProcessTimes(GetCurrentProcess(), &ct, &et, &kt, &ut);
        return (ut.dwLowDateTime + kt.dwLowDateTime) / 10000; // include time in kernel
    }
    /*else
        return clock(); // for very old Windows*/
}

namespace {
    using namespace trill;

    xdescribe("Grammar")
    {
        Runtime rt;

        xit("TrillGrammar.ls")
        {
            FILE *f = nullptr;

            if (0 == fopen_s(&f, "../trillTests/scripts/TrillGrammar.ls", "rb"))
            {
                SourceInfo* src = rt.NewSourceInfo("../trillTests/scripts/TrillGrammar.ls", f);
                Context* ctx = rt.NewContext();
                expect(ctx->Evaluate(src)).toBeTrue();
                rt.DestroyContext(ctx);
                rt.DestroySourceInfo(src);
            }
        }

        xit("fib.trs")
        {
            FILE *f = nullptr;

            if (0 == fopen_s(&f, "../trillTests/scripts/fib.trs", "rb"))
            {
                SourceInfo* src = rt.NewSourceInfo("../trillTests/scripts/fib.trs", f);
                Context* ctx = rt.NewContext();
                RuntimeValue rval;
                //Timer t;
                //float start = 0.0;
                //t.Tick();
                int start = cputime();
                // auto start = std::chrono::high_resolution_clock::now();
                expect(ctx->Evaluate(src, &rval)).toBeTrue();
                //float finish = t.Tick();
                int finish = cputime();

                //std::cout << "return: " << rval.i << ", Elapsed: ";
                std::cout << "fib: ";
                std::cout << std::fixed << std::setprecision(6) << (float)(finish - start) / 1000.0 << " s" << std::endl;
                // auto finish = std::chrono::high_resolution_clock::now();
                //std::cout << rval.i << std::endl;
                // auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
                // std::cout << "Elapsed: " << microseconds.count() << std::endl;

                rt.DestroyContext(ctx);
                rt.DestroySourceInfo(src);
                fclose(f);
            }
        }
        xit("Ackerman.trs")
        {
            FILE *f = nullptr;

            if (0 == fopen_s(&f, "../trillTests/scripts/Ackerman.trs", "rb"))
            {
                SourceInfo* src = rt.NewSourceInfo("../trillTests/scripts/Ackerman.trs", f);
                fclose(f);
                Context* ctx = rt.NewContext();
                RuntimeValue rval;
                //Timer t;
                //float start = 0.0;
                //t.Tick();
                int start = cputime();
                // auto start = std::chrono::high_resolution_clock::now();
                expect(ctx->Evaluate(src, &rval)).toBeTrue();
                //float finish = t.Tick();
                int finish = cputime();

                // std::cout << "return: " << rval.i << ", Elapsed: ";
                std::cout << "Ackerman: ";
                std::cout << std::fixed << std::setprecision(6) << (float)(finish - start) / 1000.0 << " s" << std::endl;
                // auto finish = std::chrono::high_resolution_clock::now();
                //std::cout << rval.i << std::endl;
                // auto microseconds = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
                // std::cout << "Elapsed: " << microseconds.count() << std::endl;
                rt.DestroyContext(ctx);
                rt.DestroySourceInfo(src);

            }
        }

        xit("types.trs")
        {
            FILE *f = nullptr;

            if (0 == fopen_s(&f, "../trillTests/scripts/types.trs", "rb"))
            {
                SourceInfo* src = rt.NewSourceInfo("../trillTests/scripts/types.trs", f);
                Context* ctx = rt.NewContext();
                expect(ctx->Evaluate(src)).toBeTrue();
                rt.DestroyContext(ctx);
                rt.DestroySourceInfo(src);
                fclose(f);
            }
        }


    };
}

