//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  LoopsTest.cpp
// PURPOSE:
// DATE: 2015/08/30
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "common.h"

namespace {

    using namespace trill;

    describe("Loops Test")
    {
        Runtime rt;
        Context* ctx;

        void setUp()
        {
            ctx = nullptr;
        }
        void tearDown()
        {
            if (ctx)
                rt.DestroyContext(ctx);
        }
        void beforeEach()
        {
            if (ctx)
                rt.DestroyContext(ctx);
            ctx = rt.NewContext();
        }
        void afterEach()
        {
            rt.DestroyContext(ctx);
            ctx = nullptr;
        }
        it("has while loop, basic while loop test")
        {
            ztest::Capture capture(std::cout);
            expect(ctx->Evaluate("int a = 0; while(a<100) a++; print(a);")).toBeTrue();
            expect(capture).toContain("100");
        }
        it("nest while loop")
        {
            ztest::Capture capture(std::cout);
            expect(ctx->Evaluate("int a = 0, b = 0; while(a<100){ a++; int j = 0; while(j<15){ ++b; j++; }} print(b);")).toBeTrue();
            // expect(capture).toContain("100");
            expect(capture).toContain("1500");

        }

        it("has for loop, basic for loop test")
        {
            ztest::Capture capture(std::cout);
            expect(ctx->Evaluate("for (int i = 0; i < 10; i++) dump(i);")).toBeTrue();
            expect(capture).toContain("0123456789");
        }
        it("nested for loop test")
        {
            ztest::Capture capture(std::cout);
            expect(ctx->Evaluate("for (int i = 0; i < 10; i++) {dump(i);{int i = 3;} for(int j=0;j<9;j++)i; }")).toBeTrue();
            expect(capture).toContain("0123456789");
        }


    };


}