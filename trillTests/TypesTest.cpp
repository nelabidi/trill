//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  TypesTest.cpp
// PURPOSE:
// DATE: 2015/08/28
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "common.h"


namespace {
    using namespace trill;

    xdescribe("CharType specs")
    {
        it("has unique id equal to type specifier")
        {
            CharType type;
            expect(type.id() == TS_CHAR).toBeEqual(true);
        }
        it("has unique name equal to 'char'")
        {
            CharType type;
            expect(type.name()).toEqual("char");
        }
        it("has a size method return bytes rquired to store it")
        {
            CharType type;
            expect(type.size()).toEqual(1);
        }
        it("has a derivied method return false if it's not a type pointing to another type")
        {
            CharType type;
            expect(type.isDerived()).toBeFalse();
        }
    };

    xdescribe("Type/TypeBase specs")
    {
        Type *itype, *array;
        void setUp()
        {
            itype = new IntType();
            array = new ArrayType(itype, 10);
        }
        void tearDown()
        {
            delete itype;
            delete array;
        }
        it("using type pointer and array type")
        {

            expect(itype->id() == TS_INT).toBeTrue();
            expect(itype->name() == "int").toBeTrue();
            expect(itype->isDerived()).toBeEqual(false);
            expect(itype->size()).toBeEqual(sizeof(int));
            //array
            expect(array->isReference()).toBeTrue();
            expect(array->size()).toBeEqual(sizeof(int) * 10);
            expect(array->as<ArrayType>().length()).toEqual(10);
            expect(array->as<ArrayType>().derivedType() == itype ).toBeTrue();

        }

        it("can have array of array of int, ex: int[2][10]")
        {
            ArrayType* array2 = new ArrayType(array, 2);
            //array2
            expect(array2->isReference()).toBeTrue();
            expect(array2->size()).toBeEqual(sizeof(int) * 10 * 2);
            expect(array2->as<ArrayType>().length()).toEqual(2);
            expect(array2->as<ArrayType>().derivedType() == array).toBeTrue();
            delete array2;

        }
        it("runtime can create unique types, ex: int[2][10]")
        {
            Runtime rt;
            TypeManager mgr(&rt);

            ArrayType* array2 = mgr.newArrayType(array, 2);
            ArrayType* array1 = mgr.newArrayType(array, 2);
            //array2
            expect(array2 == array1).toBeTrue();

        }


    };


}

