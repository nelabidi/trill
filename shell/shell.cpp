// shell.cpp : Defines the entry point for Trill shell
//

#include "stdafx.h"
#include "../trill/trill.h"

using namespace trill;

void Usage()
{
    std::cout << "Usage:  trl  filepath\n";
    exit(1);
}

int _tmain(int argc, _TCHAR* argv[])
{
    int retVal = 0;

    //TODO:
    // - parse command line
    // - find and set options
    // - print usage if options are wrong or not enough
    // - find the source file
    // - set current directory to source file directory
    // instance of runtime/context/ load and execute the source file
    //---------
    //for now the command line have the source file all options are enabled
    if (argc < 2)
        Usage();


    FILE *f = nullptr;

    if (0 == fopen_s(&f, argv[1], "rb"))
    {
        Runtime rt;
        SourceInfo* src = rt.NewSourceInfo(argv[1], f);
        Context* context = rt.NewContext();
        if (!context->Evaluate(src))
            retVal = 1;
    }
    else
    {
        std::cerr << "error opening file '" << argv[1] << "'\n";
        retVal = 1;
    }

    return retVal;
}

