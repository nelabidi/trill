# Trill, Scripting language #
*Where speed and small footprint meet*
--------------------------------------


This is new scripting language. Inspired by JavaScript and C/C++ syntax, it's main goal is to make it easy to embed in C/C++ and maybe other languages, small footprint with decent performance.
Trill, uses a Register based virtual machine with a baseline optimizer and a hybrid type system. 

# How to get started #

* clone this repository, get it to compile, you will need visual studio 2013 or higher, Trill uses some C++11 syntax.
* make sure to update sub-modules, Trill depends on [zTestCpp](https://github.com/nelabidi/ztestcpp) Framework
* compile and run the specs, there is a project called shell (trl.exe) a tool to run your script from the command line.
* if you use [visual studio code IDE](https://code.visualstudio.com/) you may need [Trill extension](https://bitbucket.org/nelabidi/trill-vscode)

# You want to contribute ? #

* Any advice or contribution is welcomed, you can fork this repository and send pull requests.
* Create an Issue here or join the project on it livestream channel [Trill Channel](https://www.livecoding.tv/alphabeta55/)
* Join the project on [Taiga](https://tree.taiga.io/project/nelabidi-trill/)
* Help is always needed in a big project like this one, you can help write documentation, check the [Documentation issue](https://bitbucket.org/nelabidi/trill/issues/2/write-trill-documentation) for more information
* spread the word: trill is on [openhub.net](https://www.openhub.net/p/trill)

# License #

This project is licensed under Apache 2.0, it commercial friendly license.

Happy Scripting :)