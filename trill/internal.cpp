//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  internal.cpp
// PURPOSE:
// DATE: 2015/08/21
// NOTES:
//
//////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <cstdarg>

void ReportErrorAndDie(const char* filename, unsigned line, const char*msg)
{
    std::cerr << filename << "(" << line << "): " << msg << std::endl;
    exit(1);
}

void ReportErrorAndDie(const char* funcname, const char* filename, unsigned line, const char*msg)
{
    std::stringstream ss;
    using namespace std;
    ss << " " << msg << endl;
    ss << " File: " << filename << endl;
    ss << " Line: " << line << endl;
    ss << " Function: " << funcname << endl;

    cerr << ss.str();
    //std::cerr << filename << "(" << line << "): " << funcname << " : " << msg << std::endl;
    exit(1);
}

void ReportErrorAndDie(const char*msg, ...)
{
    va_list args;
    va_start(args, msg);
    vprintf_s(msg, args);
    va_end(args);
    exit(1);
}
