//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Operators.h
// PURPOSE:
// DATE: 2015/11/29
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

#pragma once


#ifndef Operators_h__
#define Operators_h__

#include "../Variant.h"

namespace trill {


    struct NativeOperatorsSpec
    {
        NativeFunctionPtr  callOp;
        NativeFunctionPtr  addOp;
        NativeFunctionPtr  subOp;
        NativeFunctionPtr  mulOp;
        NativeFunctionPtr  divOp;
        NativeFunctionPtr  negOp;
        NativeFunctionPtr  incOp;
        NativeFunctionPtr  decOp;
        NativeFunctionPtr  cmpOp;
        NativeFunctionPtr  getOp;
        NativeFunctionPtr  setOp;
        NativeOperatorsSpec()
        {
            callOp = addOp = subOp = mulOp =
                                         divOp = negOp = incOp = decOp =
                                                 incOp = cmpOp = getOp = setOp = nullptr;
        }
    };



} //end namespace trill


#endif // Operators_h__

