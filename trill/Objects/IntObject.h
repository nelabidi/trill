//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  IntObject.h
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#include "Object.h"

namespace trill {

    struct IntObject : Object
    {
        static const ObjectClassId classId = INT_CLASS;
        IntObject(Context *ctx, int value) :
            Object(ctx, INT_CLASS),
            _value(value)
        {
        }
        inline const int Value() const
        {
            return _value;
        }
        inline void setValue(int value)
        {
            _value = value;
        }
    private:
        int     _value;
    };


}


