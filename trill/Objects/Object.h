//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Object.h
// PURPOSE:
// DATE: 2015/08/24
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef Object_h__
#define Object_h__


namespace trill {

    struct Context;
    template<typename T> struct ArrayObject;

    //garbage collected object
    struct gcObject
    {


    };
    /// <summary>
    /// Object class id, defines a unique id to be used to identify the instance of
    /// a class
    /// </summary>
    enum ObjectClassId : int
    {
        UNDEFINED_CLASS,
        ARRAY_CLASS = 1,
        SUBARRAY_CLASS,
        INT_CLASS,
        FLOAT_CLASS,
    };

    //base class for VM objects
    struct Object : gcObject
    {
        Object(Context* ctx, ObjectClassId clsid):
            context_(ctx),
            classId_(clsid) {}

        template<typename T>
        inline bool is() const
        {
            return T::classId == classId_;
        }

        template <class T>
        T& as()
        {
            ASSERT(this->is<T>());
            return *static_cast<T*>(this);
        }

        template <class T>
        const T& as() const
        {
            ASSERT(this->is<T>());
            return *static_cast<const T*>(this);
        }

        inline Context * context() const
        {
            return context_;
        }
        virtual Object* get(unsigned index)
        {
            NOT_IMPLEMENTED();
            return nullptr;
        }

    protected:
        Context* context_;
        ObjectClassId classId_;
    };


    template<typename T>
    struct ArrayObject : Object
    {
        static const ObjectClassId classId = ARRAY_CLASS;
        typedef T  value_type;
        ArrayObject(Context *ctx, ArrayType* t) :
            Object(ctx, ARRAY_CLASS),
            type_(t),
            data_(nullptr),
            parent_(nullptr)
        {
        }

        T getAt(unsigned index)
        {
            //TODO: this should throw runtime out of range exception
            ASSERT(index < type_->size() / sizeof(T));
            return data_[index];
        }

        void setAt(unsigned index, T elem)
        {
            //TODO: runtime exception here too
            ASSERT(index < type_->size() / sizeof(T));
            data_[index] = elem;
        }

        Object* get(unsigned index)
        {
            //this should throw an exception in the current thread
            ASSERT(index < type_->length());
            ASSERT(type_->derivedType() && type_->derivedType()->is<ArrayType>());
            ArrayObject* subarray = context()->getRuntime()->newSubArray(this);
            subarray->parent_ = this;
            subarray->data_ = data_ + (type_->derivedType()->size() / sizeof(T)) *  index;
            return subarray;
        }
        void    set(Object* obj, unsigned index)
        {
            ASSERT(obj->is<ArrayObject<T> >());
            ASSERT(type_ == obj->as<ArrayObject<T> >().type_);
            //TODO: assert sub-array parent same value type as this
            //more type check should go here
            //copy elements
            memcpy(data_, obj->as<ArrayObject<T> >().data_, type_->size());
        }


    protected:
        T*              data_;
        ArrayType*      type_;
        ArrayObject*    parent_; //this can be used to detect child arrays
        friend struct Runtime;
    };


    typedef ArrayObject < char > CharArrayObject;
    typedef ArrayObject < int > IntArrayObject;
    typedef ArrayObject < float > FloatArrayObject;


} //end namespace trill




#endif // Object_h__