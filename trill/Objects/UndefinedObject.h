//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  UndefinedObject.h
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef UndefinedObject_h__
#define UndefinedObject_h__

#include "Objects/Object.h"

namespace trill {

    //the undefined object, one instance per context
    //doesn't get collected
    struct UndefinedObject : Object
    {
        static const ObjectClassId classId = UNDEFINED_CLASS;
        UndefinedObject(Context* ctx) : Object(ctx, UNDEFINED_CLASS) {}
    };


} //end namespace




#endif // UndefinedObject_h__