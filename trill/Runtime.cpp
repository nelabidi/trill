//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Runtime.cpp
// PURPOSE:
// DATE: 2015/08/20
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "internal.h"
#include "Utils/Crc32.h"
#include "Utils/Hash.h"
#include "Runtime.h"
#include "Context.h"
#include "Binds/SystemLib.h"


using namespace trill;

Runtime::Runtime() :
    _errorReporter(&_consoleErrorReporter)
{

}


Runtime::~Runtime()
{

}

Context* trill::Runtime::NewContext()
{
    Context* ctx = new Context(this);
    Binds::RegisterSystemLib(ctx);
    return ctx;

}

void trill::Runtime::DestroyContext(Context* ctx)
{
    delete ctx;
}

SourceInfo* trill::Runtime::NewSourceInfo(const char* src)
{
    int datalen = strlen(src) + 1;

    SourceInfo *srcInfo = new SourceInfo();
    srcInfo->bsize = datalen;
    srcInfo->source = _strdup(src);

    //int nameLen = strlen(UNNAMED_SOURCE);
    srcInfo->filename = _strdup(UNNAMED_SOURCE);
    srcInfo->id = Crc32(src);

    return srcInfo;

}

SourceInfo* trill::Runtime::NewSourceInfo(const char* name, FILE* file)
{
    //read the file
    fseek(file, 0, SEEK_END);
    long datalen = ftell(file);
    fseek(file, 0, SEEK_SET);

    SourceInfo *srcInfo = new SourceInfo();
    srcInfo->bsize = datalen;
    srcInfo->source = (char *)malloc(datalen + 1);
    fread(srcInfo->source, 1, datalen, file);
    //make it null terminated
    srcInfo->source[datalen] = 0;

    srcInfo->filename = _strdup(name);

    srcInfo->id = Crc32(srcInfo->source);
    //m_sourceInfoTable->Set(srcInfo->m_id, srcInfo);

    return srcInfo;
}

void trill::Runtime::DestroySourceInfo(SourceInfo *src)
{
    ASSERT(src != nullptr);
    free(src->filename);
    free(src->source);
    delete src;

}

char * trill::Runtime::newName(const char* ident, int len)
{
    unsigned hash = GetHash(ident, len);
    if (_strings.find(hash) == _strings.end())
    {
        _strings[hash] = std::string(ident, len);
    }
    return  (char*)_strings[hash].data();

}


Object* trill::Runtime::newArrayObject(Context*ctx, Type* t)
{
    Object *obj = nullptr;

    ASSERT(t->is<ArrayType>());

    Type* dt = t->derivedType();
    while (dt && dt->isDerived())
    {
        dt = dt->derivedType();
    }

    switch (dt->id())//t->derivedType()
    {
    case trill::TS_UNKNOWN:
        INTERNAL_ERROR("Array of type unknown");
        break;
    case trill::TS_BOOL:
    case trill::TS_CHAR:
    case trill::TS_UCHAR:
    case trill::TS_INT:
    case trill::TS_UINT:
    {
        IntArrayObject* array = new IntArrayObject(ctx, &t->as<ArrayType>());
        array->data_ = new int[t->as<ArrayType>().size() / sizeof(int)];
        obj = array;
    }
    break;
    case trill::TS_FLOAT:
    case trill::TS_STRING:
    case trill::TS_VAR:
    case trill::TS_ARRAY:
    case trill::TS_MAP:
        NOT_IMPLEMENTED();
        break;
    default:
        break;
    }
    return obj;
}

void * trill::Runtime::newAlignedBuffer(unsigned size, unsigned alignement)
{
    return _aligned_malloc(size , alignement);
}

void trill::Runtime::deleteAlignedBuffer(void*p)
{
    _aligned_free(p);
}

/*
SubArrayObject* trill::Runtime::newSubArrayObject(Object* parent)
{
    return new SubArrayObject(parent);

}
*/
