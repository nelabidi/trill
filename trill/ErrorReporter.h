//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  ErrorReporter.h
// PURPOSE:  Error reporting
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once
namespace trill {
    struct Location;
    //error reporting interface
    struct ErrorReporter
    {
        //file(line:column)
        const char *ErrorFormat = "%s(%d:%d):";
        static const char* FormatLocation(const char *filename, Location& loc);
        //Error reporting
        virtual void  ReportError(char *message) = 0;
        virtual void  ReportError(const char* filename, Location& location, char *message) = 0;
        virtual void  ReportWarning(const char* filename, Location& location, char *message) = 0;
        virtual bool  HasErrors() = 0;
        virtual void  Reset() = 0;
    };

    struct ConsoleErrorReporter : ErrorReporter
    {
        virtual void ReportError(char *message);
        virtual void ReportError(const char* filename, Location& location, char *message);
        virtual void ReportWarning(const char* filename, Location& location, char *message);
        virtual bool HasErrors();
        ConsoleErrorReporter():
            _nErrors(0),
            _nWarnings(0) {}

        virtual void Reset();

    private:
        int _nErrors;
        int _nWarnings;

    };

} //end namespace trill

