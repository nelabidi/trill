// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

//#include <SDKDDKVer.h>

#include <malloc.h> //allocator, malloc,free
#include <stdlib.h> //strtol
#include <stdio.h>
#include <tchar.h>

#include <assert.h>

//C++ headers
#include <exception>
#include <vector>
#include <string>
#include <sstream> //used in types.cpp
#include <unordered_map>
#include <iostream>

#include <fstream>
#include <cstdarg>


