//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  trill.h, main trill library include file
// PURPOSE:
// DATE: 2015/08/20
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef trill_h__
#define trill_h__

#include "stdafx.h"
#include "config.h"


//Forward declaration
namespace trill {

    struct Context;
    struct Runtime;
    //common used structures

}
#include "internal.h"
#include "Location.h"
#include "SourceInfo.h"
#include "Context.h"
#include "Runtime.h"



#endif // trill_h__