//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  internal.h
// PURPOSE:
// DATE: 2015/08/20
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef internal_h__
#define internal_h__

extern void ReportErrorAndDie(const char*msg, ...);
extern void ReportErrorAndDie(const char* filename, unsigned line, const char*msg);
extern void ReportErrorAndDie(const char* funcname, const char* filename, unsigned line, const char*msg);

#define NOT_IMPLEMENTED() ReportErrorAndDie(__FUNCTION__,__FILE__,__LINE__,"NOT IMPLEMENTED")
#define INTERNAL_ERROR(msg) ReportErrorAndDie(__FUNCTION__,__FILE__,__LINE__,"INTERNAL ERROR, " msg)


#endif // internal_h__