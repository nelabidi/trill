//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Context.cpp
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "internal.h"
#include "SourceInfo.h"
#include "Types.h"
#include "Objects/Object.h"
#include "Objects/UndefinedObject.h"
#include "Context.h"
#include "VM/Thread.h"
#include "VM/CodeGenerator.h"
#include "Compiler/Compiler.h"
#include "Runtime.h"


using namespace trill;


static bool Int(Context* ctx, Variant* vp, unsigned argc)
{
    ASSERT(argc == 1);
    CallArgs args(vp, argc);
    ASSERT(args[0].isInt());
    args.setReturnValue(ctx->newIntObject(args[0].Int()));
    return true;
}
//TODO: move this to FloatObject.cpp
static bool Float(Context* ctx, Variant* vp, unsigned argc)
{
    ASSERT(argc == 1);
    CallArgs args(vp, argc);
    ASSERT(args[0].isFloat());
    args.setReturnValue(ctx->newFloatObject(args[0].Float()));
    return true;
}


static void RegisterNativeTypes(Context* ctx)
{
    ctx->RegisterNative("Int", Int, 1);
    ctx->RegisterNative("Float", Float, 1);

}

trill::Context::Context(Runtime *rt)
    : _runtime(rt),
      _symbolFile(nullptr),
      _typeManger(rt),
      _undefined(this)
{
    //TODO: initialize global object, global functions here
    ::RegisterNativeTypes(this);
}

trill::Context::~Context()
{
    //TODO: destroy everything here
}



bool trill::Context::Evaluate(const char* source, void* /*= nullptr*/)
{

    SourceInfo* srcInfo = _runtime->NewSourceInfo(source);
    bool bSuccess = Evaluate(srcInfo);
    _runtime->DestroySourceInfo(srcInfo);

    return bSuccess;
}

bool trill::Context::Evaluate(SourceInfo* srcInfo, void* pRetVal /*= nullptr*/)
{
    //TODO:
    // - Create/Init Compiler source  -> Expr* list                     : creates/defines symbols
    // - Create/Init CodeGenerator Expr* -> Opcode/Instructions list    : lookup symbols -> ids in this table
    // - Create/Init Thread Instructions - > modify Context state       : return last expression value

    _runtime->GetErrorReporter()->Reset();
    Compiler compiler(this);

    if (compiler.compileSource(srcInfo, &_globalScope))
    {
        //dump symbols
        if (_symbolFile != nullptr)
        {
            compiler.DumpSymbols(_symbolFile);
        }
        //TODO: get script symbols from compiler, load them, call generate on every function
        //add property/member to global variable
        // Script        script(this);
        CodeGenerator generator(this);

        generator.generate(compiler.getScriptSymbols(), &_byteCode);

        //first symbol is always main function
        FunctionInfo mainfn = getFunctionInfo(compiler.getScriptSymbols()[0]->name());
        Thread mainThread(this);
        mainThread.Execute(mainfn);
        if (pRetVal)
        {
            *((RuntimeValue *)pRetVal) = mainThread.returnValue();
        }
    }
    else
    {
        return false;
    }

    return true;
}
/*
unsigned int trill::Context::addGlobal(char *name, TypeSpec type)
{
    RuntimeValue v;
    switch (type)
    {
    case trill::TS_UNKNOWN:
        INTERNAL_ERROR("Unknown type");
        break;
    case trill::TS_BOOL:
    case trill::TS_CHAR:
    case trill::TS_UCHAR:
    case trill::TS_INT:
    case trill::TS_UINT:
        v.i = 0;
        break;
    case trill::TS_FLOAT:
        v.f = 0.0;
        break;
    case trill::TS_STRING:
    case trill::TS_VAR:
    default:
        NOT_IMPLEMENTED();
        break;
    }

    if (_globalsMap.find(name) == _globalsMap.end())
    {
        _globalsMap[name] = _globals.size();
        _globals.push_back(v);
    }

    return _globalsMap[name];
}
*/
unsigned int trill::Context::addGlobal(VariableSymbol& var)
{
    RuntimeValue v;
    static const char * ps = "constant string";

    switch (var.typeId())
    {
    case trill::TS_UNKNOWN:
        INTERNAL_ERROR("Unknown type");
        break;
    case trill::TS_BOOL:
    case trill::TS_CHAR:
    case trill::TS_UCHAR:
    case trill::TS_INT:
    case trill::TS_UINT:
        v.i = 0;
        break;
    case trill::TS_FLOAT:
        v.f = 0.0;
        break;
    case TS_ARRAY:
        v.p = _runtime->newArrayObject(this, var.type());
        break;
    case trill::TS_VAR:
        v.p = &_undefined;
        break;
    case trill::TS_STRING:
        v.p = (void*)ps;
        break;
    default:
        NOT_IMPLEMENTED();
        break;
    }

    if (_globalsMap.find(var.name()) == _globalsMap.end())
    {
        _globalsMap[var.name()] = _globals.size();
        _globals.push_back(v);
    }

    return _globalsMap[var.name()];
}

bool trill::Context::RegisterNative(const char* name, NativeFunctionPtr ptr, unsigned nargs, TypeSpec returnType /*= TS_VAR*/)
{
    char* fnname = _runtime->newName(name, LStrlen(name));
    if (_globalScope.findSymbol(fnname) != nullptr)
    {
        //TODO: throw an error here
        return false;
    }
    NativeSymbol *native = new NativeSymbol();
    native->setFunctionPtr(ptr);
    native->setName(fnname);
    native->setScope(&_globalScope);
    native->setParent(nullptr);
    native->setnArgs(nargs);
    native->setType(GetBaseType(returnType));
    _globalScope.addSymbol(native);
    //add it to Function info as well
    FunctionInfo fninfo;
    fninfo.name = fnname;
    fninfo.nargs = nargs;
    fninfo.native = ptr;
    fninfo.nregs = 0;
    addFunction(fninfo);
    return true;
}



//throw an exception in the current thread
void trill::Context::ThrowException(const char* msg)
{
    std::cerr << msg << std::endl;
    //use internal error for now
    INTERNAL_ERROR("Exception");

}

bool trill::Context::Execute(const char* srcFile,
                             const CompileOptions& options /*= CompileOptions::Default()*/,
                             Variant* retVal /*= nullptr*/)
{
    bool bSuccess = false;
    //try to open the file
    FILE *f = nullptr;
    if (fopen_s(&f, srcFile, "rb") != 0)
    {
        ::ReportErrorAndDie("can't open file '%s' for read", srcFile);
    }
    _symbolFile = nullptr;
    if (options.bDumpSymbols && options.szSymbolFile != nullptr)
    {
        //try to open symbol file for write
        FILE *fs = nullptr;
        if (fopen_s(&fs, options.szSymbolFile, "w+") != 0)
            ::ReportErrorAndDie("can't open file '%s' for writing", options.szSymbolFile);
        _symbolFile = options.szSymbolFile;
        fclose(fs);
    }

    SourceInfo* src = _runtime->NewSourceInfo(srcFile, f);
    fclose(f);
    //TODO: cast to variant
    bSuccess = Evaluate(src, nullptr);

    _runtime->DestroySourceInfo(src);
    return bSuccess;
}

ArrayType* trill::Context::newArrayType(Type *derivedType, unsigned nelement /*= 0*/)
{
    return _typeManger.newArrayType(derivedType, nelement);
}


Object* trill::Context::newArrayObject(Type* t)
{
    return _runtime->newArrayObject(this, t);
}

TypeList* trill::Context::newTypeList(Type* t /*= nullptr*/)
{
    return _typeManger.newTypeList(t);
}

IntObject* trill::Context::newIntObject(int value)
{
    //TODO: this should call garbage collector
    //we use new for now
    return new IntObject(this, value);

}

FloatObject* trill::Context::newFloatObject(float f)
{
    //TODO: this needs to be garbage collected
    return new FloatObject(this, f);

}



