//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  BitSet.h
// PURPOSE:
// DATE: 2015/08/25
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef BitSet_h__
#define BitSet_h__
#pragma once

#include "../config.h"

namespace trill {

    struct BitSet
    {

        BitSet(unsigned bits = 0);
        ~BitSet();

        inline unsigned size() const
        {
            return _bsize;
        }
        inline bool at(unsigned index) const
        {
            ASSERT(index < _bsize);
            unsigned w = _data[word_index(index)];
            return  w & (1 << (bit_index(index))) ? true : false;
        }
        inline bool operator[](unsigned index) const
        {
            return at(index);
        }

        inline void set(unsigned index)
        {
            ASSERT(index < _bsize);
            _data[word_index(index)] |= (1 << (bit_index(index)));

        }
        inline void unset(unsigned index)
        {
            ASSERT(index < _bsize);
            _data[word_index(index)] &= (~(1 << (bit_index(index))));
        }
        inline void resize(unsigned bsize)
        {
            if (_bsize == bsize) return;
            _bsize = bsize;
            _data.resize((bsize + BitSet::word_bits - 1) / BitSet::word_bits);
        }
        void clear();


        bool Union(const BitSet& other);
        bool subtract(const BitSet& other);
        bool intersect(const BitSet& other);


        //or / union
        //BitSet& operator+=(const BitSet&);
        //subtraction
        BitSet& operator-(const BitSet&);
        /*friend BitSet operator^(const BitSet& , const BitSet& );*/


        bool operator==(const BitSet& other) const
        {
            return _bsize == other._bsize && _data == other._data;
        }
        inline void operator=(const BitSet& other)
        {
            resize(other._bsize);
            _data = other._data;
        }


    protected:
        static const unsigned word_bits = sizeof(unsigned) * 8;

        std::vector<unsigned> _data;
        unsigned _bsize;
        inline unsigned bit_index(unsigned index) const
        {
            return index % word_bits;
        }
        inline unsigned word_index(unsigned index) const
        {
            return index / word_bits;
        }
        friend std::ostream& operator<<(std::ostream&, BitSet&);

    };

    //for dumping a bitset
    std::ostream& operator<<(std::ostream&, BitSet&);
}
#endif // BitSet_h__

