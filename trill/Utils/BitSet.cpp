#include "stdafx.h"
#include "../config.h"
#include "BitSet.h"


using namespace trill;



trill::BitSet::BitSet(unsigned bits)
{
    //_bsize = bits;
    resize(bits);
    /*_data.resize((bits + BitSet::word_bits - 1) / BitSet::word_bits);
    for (auto & w : _data)
    {
        w = 0;
    }*/
}

BitSet::~BitSet()
{
}

void trill::BitSet::clear()
{

    for (unsigned i = 0; i < _data.size(); i++)
    {
        _data[i] = 0;
    }
}
/*
//substruction
BitSet operator-(const BitSet& left, const BitSet& other)
{

    ASSERT(left._bsize == other._bsize);

    BitSet result(left._bsize);
    for (unsigned i = 0; i < left._data.size(); i++)
    {
        result[i] = (left._data[i] & (!other._data[i]));
    }

    return result;
}

//union
trill::BitSet::BitSet trill::BitSet::operator+(const BitSet& left, const BitSet& other)
{
    ASSERT(left._bsize == other._bsize);
    BitSet result(left._bsize);

    for (unsigned i; i < _data.size(); i++)
    {
        _data[i] = left._data[i] | other._data[i];
    }
}

//intersection
trill::BitSet::BitSet trill::BitSet::operator^(const BitSet& left, const BitSet& other)
{
    ASSERT(left._bsize == other._bsize);

    BitSet result(left._bsize);
    for (unsigned i = 0; i < left._data.size(); i++)
    {
        result[i] = left._data[i] & other._data[i];
    }

    return result;
}*/

bool trill::BitSet::subtract(const BitSet& other)
{
    ASSERT(_bsize == other._bsize);
    bool retval = false;

    for (unsigned i = 0; i < _data.size(); i++)
    {
        unsigned w = _data[i];
        _data[i] = (w & (!other._data[i]));
        retval = (w != _data[i]);
    }
    return retval;
}

bool trill::BitSet::intersect(const BitSet& other)
{
    ASSERT(_bsize == other._bsize);
    bool retval = false;
    for (unsigned i = 0; i < _data.size(); i++)
    {
        unsigned w = _data[i];
        _data[i] &= other._data[i];
        retval = (w != _data[i]);
    }

    return retval;
}

bool trill::BitSet::Union(const BitSet& other)
{
    ASSERT(_bsize == other._bsize);

    bool retval = false;

    for (unsigned i = 0; i < _data.size(); i++)
    {
        unsigned w = _data[i];
        _data[i] = w | other._data[i];
        retval = (w != _data[i]);
    }

    return retval;
}

BitSet& trill::BitSet::operator-(const BitSet& other)
{
    subtract(other);
    return *this;
}

std::ostream& trill::operator<<(std::ostream& o, BitSet& bs)
{
    for (unsigned i = 0; i < bs._bsize; i++)
    {
        if (bs.at(i))
            o << 1;
        else
            o << 0;
    }

    return o;
}



