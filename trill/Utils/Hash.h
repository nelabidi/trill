//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Hash.h
// PURPOSE:
// DATE: 2015/08/22
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "../config.h"

namespace trill {

    static inline unsigned GetHash(const char *s, unsigned len = 0)
    {
        if (len == 0)  len = LStrlen(s);

        unsigned char* str = (unsigned char*)s;
        unsigned int h = len;  /* seed */
        /* if string is too long, don't hash all its chars */
        unsigned step = (len >> 5) | 1;
        for (; len >= step; len -= step)
            h = h ^ ((h << 5) + (h >> 2) + * (str++));

        return h;
    }


}