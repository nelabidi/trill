//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill, Scritping language
// FILE:  Types.h
// PURPOSE: define static type system and types factory
// DATE: 2015/08/28
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

/// <summary>
/// Trill have 2 kinds of types, value types and reference types:
/// * value types are: char, int and float are raw values, they fit in the VM registers.
///     NOTE: for now trill is 32 bits, which means we relay on sizeof(int) is 4 bytes.
/// * reference types: are instance of an object declared with keyword 'var'.
///     fits in VM register as (void*) again we relay on sizeof (void*) is 4 bytes,
///     they are wrapped in the "Object" interface.
/// * a special type string can act as both value and reference type depends on the context
/// </summary>

#ifndef Types_h__
#define Types_h__

#ifdef _MSC_VER
#pragma once
#endif

#include "config.h"

//This header file depends on string and unordred_map headers

namespace trill {

    //Forward declaration of Runtime class
    struct Runtime;

    //Type specifiers
    enum TypeSpec : int
    {
        TS_UNKNOWN = 0, // here to detect uninitialized member
        TS_BOOL, //not used yet
        TS_CHAR,
        TS_UCHAR, //not used yet
        TS_INT,
        TS_UINT,  //not used yet
        TS_FLOAT,
        TS_STRING, //for string constants
        //from here are reference types, order is important!!
        TS_VAR,     //variant/objects/instances
        TS_ARRAY,   //used for Array initializer/array declaration , array expression
        TS_MAP,     //table/map

    };
    //helper function to check if a type specifier can be treated as an integer
    inline bool IsIntType(TypeSpec ts)
    {
        return ts == TS_INT || ts == TS_UINT || ts == TS_BOOL || ts == TS_CHAR || ts == TS_UCHAR;
    }

    inline bool IsFloatType(TypeSpec ts)
    {
        return ts == TS_FLOAT;
    }
    //check if the passed type specifier can use dot '.' operator to access an object member
    inline bool hasDotOperator(TypeSpec ts)
    {
        return ts == TS_MAP || ts == TS_VAR || ts == TS_STRING;
    }
    //check if the passed type specifier can use the subscript operator '[]'
    inline bool hasSubscriptOperator(TypeSpec ts)
    {
        return ts == TS_ARRAY || ts == TS_MAP || ts == TS_VAR || ts == TS_STRING;
    }

    //forward declaration.
    struct Type;
    struct TypeImpl;
    struct CharType;
    struct IntType;
    struct VarType;

    /// <summary>
    /// Type: is an Interface of all Types, every Type derive from this class
    /// </summary>
    struct Type
    {
        Type(TypeSpec ts):
            type_(ts) {}

        //return the type id, we use the type specifier as a type id
        inline const TypeSpec id() const
        {
            return type_;
        }
        //return true if this type is a reference type
        inline bool isReference() const
        {
            return type_ > TS_STRING;
        }
        //return true if this type is derived type
        inline bool isDerived() const;
        //return the type this type is derived from
        inline Type* derivedType() const;
        //size is the binary size for this type
        inline unsigned size() const;
        //short name of this type
        inline const char* name() const;
        //long name of this type
        inline const char* fullName() const;
        inline void setFullName(char *name);
        //down-casting methods
        template<typename T>
        inline bool is() const
        {
            return T::type == type_;
        }
        //specialization of TypeImpl
        template<>
        inline bool is<TypeImpl>() const
        {
            return true;
        }
        //down-cast this type to a type T
        template <class T>
        T& as()
        {
            ASSERT(this->is<T>());
            return *static_cast<T*>(this);
        }
        template <class T>
        const T& as() const
        {
            ASSERT(this->is<T>());
            return *static_cast<const T*>(this);
        }
    protected:
        TypeSpec  type_;

    };

    /// <summary>
    /// define a type-list used by composed types, for example array type.
    /// </summary>
    struct TypeList
    {
        TypeList(Type* t = nullptr, TypeList* next = nullptr) :
            type_(t),
            next_(next) {}

        inline TypeList* next() const
        {
            return next_;
        }
        inline void setNext(TypeList* n)
        {
            next_ = n;
        }
        inline Type* type() const
        {
            return type_;
        }
        inline void setType(Type*t)
        {
            type_ = t;
        }
    protected:
        TypeList*     next_;
        Type*     type_;
    };

    //Type implementation
    struct TypeImpl : Type
    {
        TypeImpl(TypeSpec ts) : Type(ts),
            derivedType_(nullptr),
            size_(0),
            name_(""),
            fullName_(nullptr) {}

        inline const char * name() const
        {
            return name_;
        }
        inline const char * fullName() const
        {
            return fullName_;
        }
        inline void setFullName(char *name)
        {
            fullName_ = name;
        }
        inline const unsigned size() const
        {
            return size_;
        }
        inline const bool isDerived() const
        {
            return derivedType_ != nullptr;
        }
        inline Type* derivedType() const
        {
            return derivedType_;
        }

    protected:
        const char *    name_;
        char *          fullName_;
        unsigned        size_;
        Type*      derivedType_;
    };

    inline const char* Type::name() const
    {
        return as<TypeImpl>().name();
    }

    inline unsigned Type::size() const
    {
        return as<TypeImpl>().size();
    }
    inline bool Type::isDerived() const
    {
        return as<TypeImpl>().isDerived();
    }
    inline Type* Type::derivedType() const
    {
        return as<TypeImpl>().derivedType();
    }
    inline const char * Type::fullName() const
    {
        return as<TypeImpl>().fullName();
    }
    inline void Type::setFullName(char *name)
    {
        as<TypeImpl>().setFullName(name);
    }

    /// <summary>
    /// Char type implementation
    /// </summary>
    struct CharType : TypeImpl
    {
        static const TypeSpec type = TS_CHAR;

        CharType() :
            TypeImpl(TS_CHAR)
        {
            name_ = "char";
            size_ = sizeof(char);
        }
        static CharType* instance()
        {
            static CharType ctype;
            return &ctype;
        }
    };

    /// <summary>
    /// Int type implementation
    /// </summary>
    struct IntType : TypeImpl

    {
        static const TypeSpec type = TS_INT;

        IntType() : TypeImpl(TS_INT)
        {
            name_ = "int";
            size_ = sizeof(int);
        }
        static IntType* instance()
        {
            static IntType type;
            return &type;
        }
    };

    /// <summary>
    /// Float type
    /// </summary>
    struct FloatType : TypeImpl

    {
        static const TypeSpec type = TS_FLOAT;

        FloatType() : TypeImpl(TS_FLOAT)
        {
            name_ = "float";
            size_ = sizeof(float);
        }
        static FloatType* instance()
        {
            static FloatType type;
            return &type;
        }
    };
    /// <summary>
    /// var type denote any reference type.(Object instance)
    /// </summary>
    struct VarType : TypeImpl
    {
        static const TypeSpec type = TS_VAR;

        VarType() : TypeImpl(TS_VAR)
        {
            name_ = "Object";
            size_ = sizeof(void*);
        }
        static VarType* instance()
        {
            static VarType type;
            return &type;
        }
    };
    /// <summary>
    /// Array type, used internally during the initialization and the declaration of an
    /// array variable
    /// </summary>
    struct ArrayType: TypeImpl
    {
        static const TypeSpec type = TS_ARRAY;


        ArrayType(Type* derived, unsigned nelement = 0) :
            TypeImpl(TS_ARRAY),
            nelement_(nelement)
        {
            name_ = "Array";
            size_ = derived->size() * nelement;
            derivedType_ = derived;
        }
        //return the type this array is derived from
        inline Type* derivedType() const
        {
            return derivedType_;
        }
        //the length is the number of elements the array can hold
        inline unsigned length() const
        {
            return nelement_;
        }
    protected:
        unsigned   nelement_;

    };
    /// <summary>
    /// defines string type, any string constance have this type
    /// </summary>
    struct StringType : TypeImpl
    {
        static const TypeSpec type = TS_STRING;

        StringType(unsigned len = 0) :
            TypeImpl(TS_STRING)
        {
            name_ = "string";
            derivedType_ = CharType::instance();
        }
        static StringType* instance()
        {
            static StringType type;
            return &type;
        }
    protected:

    };

    //singleton class for types management
    struct TypeManager
    {
        TypeManager(Runtime *rt);
        ~TypeManager();

        StringType* newStringType();
        ArrayType*  newArrayType(Type *derivedType, unsigned nelement = 0);
        TypeList*   newTypeList(Type* t /*= nullptr*/);

    protected:
        Runtime* _runtime;
        TypeManager(const TypeManager&) = delete;
        TypeManager& operator=(const TypeManager&) = delete;

        std::vector<Type*> alltypes_;
        std::vector<TypeList*> _typeLists;
        std::unordered_map<char*, Type*>  _types;
    };

    //helper functions
    bool IsEqualType(Type* t1, Type* t2);
    bool IsCompatibleType(Type* src, Type* dst);
    Type* PromoteType(Type* t);
    Type* GetBaseType(TypeSpec ts);
    std::string TypeFullName(Type *t);

} //end namespace trill

#endif // Types_h__




