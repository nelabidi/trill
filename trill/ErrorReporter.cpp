//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  ErrorReporter.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Location.h"
#include "ErrorReporter.h"

#include <iostream>
#include <cstdarg>

using namespace trill;

const char* trill::ErrorReporter::FormatLocation(const char *filename, Location& loc)
{
    static char buffer[1024];
    sprintf_s(buffer, "%s(%d:%d):", filename, loc.lineno, loc.column);
    return buffer;
}

void trill::ConsoleErrorReporter::ReportError(char *message)
{
    std::cerr << "Error: " << message << std::endl;
    _nErrors++;
}

void trill::ConsoleErrorReporter::ReportError(const char* filename, Location& location, char *message)
{
    std::cerr << ErrorReporter::FormatLocation(filename, location) << " Error: " << message << std::endl;
    ++_nErrors;
}

void trill::ConsoleErrorReporter::ReportWarning(const char* filename, Location& location, char *message)
{

    std::cerr << ErrorReporter::FormatLocation(filename, location) << " Warning: " << message << std::endl;
    ++_nWarnings;
}

bool trill::ConsoleErrorReporter::HasErrors()
{
    return _nErrors != 0;
}

void trill::ConsoleErrorReporter::Reset()
{
    _nErrors = _nWarnings = 0;
}
