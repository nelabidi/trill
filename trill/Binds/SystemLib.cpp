//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  SystemLib.cpp
// PURPOSE: Implement sys io functions
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../internal.h"

#include "../Context.h"
#include "../Variant.h"
#include "SystemLib.h"

using namespace trill;

static bool Dump(Context*, Variant* vp, unsigned argc)
{
    Object *obj;
    for (unsigned i = 0; i < argc; i++)
    {
        if (i)
            std::cout << " ";

        switch (vp[i].kind())
        {
        case trill::VK_UNDEFINED:
            std::cout << "undefined";
            break;
        case trill::VK_NULL:
            std::cout << "null";
            break;
        case trill::VK_CHAR:
            std::cout << vp[i].Char();
            break;
        case trill::VK_INT:
            std::cout << vp[i].Int();
            break;
        case trill::VK_FLOAT:
            std::cout << vp[i].Float();
            break;
        case trill::VK_STRING:
            std::cout << vp[i].Str();
            break;
        case trill::VK_OBJECT:
            //check for native
            //TODO: this is ugly, toString function needs to be implemented for Object class
            //and can be overridden in subclasses
            obj = (Object*)vp[i].Ptr();
            if (obj->is<IntObject>())
            {
                IntObject& iobj = obj->as<IntObject>();
                std::cout << iobj.Value();
            }
            else if (obj->is<FloatObject>())
            {
                FloatObject& fobj = obj->as<FloatObject>();
                std::cout << fobj.Value();
            }
            else
                std::cout << "object";
            break;
        default:
            break;
        }

    }
    return true;
}

static bool Print(Context*ctx, Variant* vp, unsigned argc)
{
    Dump(ctx, vp, argc);
    std::cout << std::endl;
    return true;
}
/// <summary>
///
/// </summary>
/// <param name="ctx"></param>
void Binds::RegisterSystemLib(Context*ctx)
{
    //here register all native functions
    ctx->RegisterNative("print", Print, 1);
    ctx->RegisterNative("dump", Dump, 1);

}
