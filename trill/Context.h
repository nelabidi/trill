//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Context.h
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef Context_h__
#define Context_h__

#include "Types.h"
#include "Variant.h"
#include "Compiler/scope.h"  //global scope
#include "Compiler/Symbol.h"
#include "vm/Opcode.h"
#include "Objects/UndefinedObject.h"
#include "Objects/IntObject.h"
#include "Objects/FloatObject.h"
#include "Objects/Class.h"

namespace trill {

    struct Runtime;
    struct SourceInfo;


    struct CompileOptions
    {
        bool bDumpSymbols;
        const char *szSymbolFile;
        CompileOptions()
            : bDumpSymbols(false),
              szSymbolFile(nullptr) {}
        static const CompileOptions& Default()
        {
            static CompileOptions opt;
            return opt;
        }
    };

    struct FunctionInfo
    {
        unsigned entry;
        unsigned nargs;
        unsigned nregs;
        char*   name;
        NativeFunctionPtr   native;
        FunctionInfo() :
            native(nullptr)
        {}
    };

    struct NativeClassInfo
    {
        unsigned int id; //unique id to identify this class
        //TODO: for now we are going to same pointer passed in NativeClassSpec
        //NativeOperatorsSpec operators; // instance operators

    };

    union RuntimeValue
    {
        const char *s;
        float f;
        int   i;
        void* p;
    };


    struct Context
    {
        Context(Runtime *rt);
        ~Context();

        //Interface API
        bool Evaluate(const char* inlineSource, void* = nullptr);
        bool Evaluate(SourceInfo* src, void* = nullptr);
        bool Execute(const char* srcFile,
                     const CompileOptions& options = CompileOptions::Default(),
                     Variant* retVal = nullptr);
        // register native function
        bool RegisterNative(const char* name, NativeFunctionPtr ptr,
                            unsigned nargs, TypeSpec returnType = TS_VAR);
        bool RegisterNativeClass(NativeClassSpec* spec);

        //getters
        inline Runtime* getRuntime() const
        {
            return _runtime;
        }

        void ThrowException(const char* msg);

        IntObject* newIntObject(int value);
        FloatObject* newFloatObject(float f);

    protected:
        const char* _symbolFile;
        Runtime *   _runtime;
        GlobalScope _globalScope;
        std::vector<RuntimeValue>   _globals;
        std::vector<FunctionInfo>   _functions;
        std::vector<std::string>    _strings;
        std::unordered_map<char*, unsigned> _functionsMap;
        std::unordered_map<char*, unsigned> _globalsMap;
        VM::ByteCode    _byteCode;
        TypeManager _typeManger;
        UndefinedObject _undefined;

        inline RuntimeValue getGlobal(unsigned int index) const
        {
            ASSERT(index < _globals.size());
            return _globals[index];
        }
        inline void setGlobal(unsigned index, RuntimeValue value)
        {
            ASSERT(index < _globals.size());
            _globals[index] = value;
        }
        inline unsigned addStringConstant(const char* str)
        {
            _strings.push_back(std::string(str));
            return _strings.size() - 1;
        }
        inline const char* getString(unsigned index)
        {
            ASSERT(index < _strings.size());
            return _strings[index].c_str();
        }

        unsigned int addGlobal(VariableSymbol& var);
        inline unsigned int addFunction(FunctionInfo& fninfo)
        {
            if (_functionsMap.find(fninfo.name) == _functionsMap.end())
            {
                _functions.push_back(fninfo);
                _functionsMap[fninfo.name] = _functions.size() - 1;
            }
            return _functionsMap[fninfo.name];
        }

        inline FunctionInfo getFunction(unsigned int index) const
        {
            ASSERT(index < _functions.size());
            return _functions[index];
        }

        inline unsigned getFunction(char *name) const
        {
            ASSERT(_functionsMap.find(name) != _functionsMap.end());
            return _functionsMap.at(name);
        }

        inline FunctionInfo getFunctionInfo(char *name) const
        {
            return  getFunction(getFunction(name));
        }

        //Types
        ArrayType*  newArrayType(Type *derivedType, unsigned nelement = 0);
        TypeList*   newTypeList(Type* t = nullptr);

        //objects
        //objects factory
        Object*    newArrayObject(Type* t);


        friend struct Compiler;
        friend struct CodeGenerator;
        friend struct Thread;



    };

} //end namespace trill

#endif // Context_h__
