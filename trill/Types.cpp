//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill, Scritping language
// FILE:  Types.cpp
// PURPOSE: implement Type class helpers, TypeManager implementation
// DATE: 2015/11/27
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../internal.h"
#include "Types.h"
#include "Runtime.h"

using namespace  trill;

/// <summary>
/// Create a new string type
/// </summary>
/// <returns>a new instance of StringType object</returns>
StringType* trill::TypeManager::newStringType()
{
    StringType* t = new StringType();
    alltypes_.push_back(t);
    return t;
}

/// <summary>
/// Create a ArrayType object
/// </summary>
/// <param name="derivedType">derived type the aray is derived from</param>
/// <param name="nelement">number of element this array have</param>
/// <returns>new ArrayType instance</returns>
ArrayType* trill::TypeManager::newArrayType(Type *derivedType, unsigned nelement /*= 0*/)
{
    const char *name = derivedType->fullName();
    if (name == nullptr)
    {
        std::string fullname = TypeFullName(derivedType);
        //add it to types map
        char *unique_name = _runtime->newName(fullname.c_str(), fullname.size());
        if (_types.find(unique_name) == _types.end())
        {
            _types[unique_name] = derivedType;
        }
        derivedType->setFullName(unique_name);
        name = derivedType->fullName();
    }

    std::stringstream ss;
    ss << "[" << nelement << "]" << name;
    char *unique_name = _runtime->newName(ss.str().c_str(), ss.str().size());

    if (_types.find(unique_name) == _types.end())
    {
        ArrayType * array = new ArrayType(derivedType, nelement);
        alltypes_.push_back(array);
        array->setFullName(unique_name);
        //add it to types map
        _types[unique_name] = array;
    }
    return  (ArrayType*)_types[unique_name];
}

trill::TypeManager::TypeManager(Runtime *rt)
    : _runtime(rt)
{
    alltypes_.reserve(64);
}

trill::TypeManager::~TypeManager()
{
    for (auto t : alltypes_)
    {
        if (t->isReference() && t->id() != TS_VAR)
            delete t;
    }
    alltypes_.clear();
    for (auto l : _typeLists)
    {
        delete l;
    }
    _typeLists.clear();
}
/// <summary>
/// make new instance of type list
/// </summary>
/// <param name="t">type to create a list for</param>
/// <returns>TypeList instance</returns>
TypeList* trill::TypeManager::newTypeList(Type* t /*= nullptr*/)
{
    //TODO: use a memory manager for this
    TypeList * list = new TypeList(t);
    _typeLists.push_back(list);
    return list;
}

/// <summary>
/// Check if 2 type are equal, equal types are types with the same id or same derived types.
/// NOTE: this is a recursive function.
/// </summary>
/// <param name="t1">left type</param>
/// <param name="t2">right type</param>
/// <returns>true if t1 and t2 are equal</returns>
bool trill::IsEqualType(Type* t1, Type* t2)
{
    //check pointers
    if (t1 == t2) return true;
    //check id
    if (t1->id() != t2->id()) return false;
    //same id check derived types
    if (t1->isDerived() && t2->isDerived())
        return IsEqualType(t1->derivedType(), t2->derivedType());

    return false;
}
/// <summary>
/// Generate a full name for a type.
/// </summary>
/// <param name="t">type to generate a name for</param>
/// <returns>instnace of std::string representing the type full name </returns>
std::string trill::TypeFullName(Type *t)
{
    std::stringstream ss;
    if (t->isDerived())
    {
        while (t->isDerived())
        {
            //if array add length
            if (t->is<ArrayType>())
            {
                ss << "[" << t->as<ArrayType>().length() << "]";
            }
            t = t->derivedType();
        }
    }
    ss << t->name();
    return ss.str();
}

//Compatible types rules:
// 1 - if they are equal imply they are compatible.
// 2 - var type is compatible with any other reference type.
// 3 - integer types are compatible with each other: char , int ..etc

bool trill::IsCompatibleType(Type* src, Type* dst)
{
    if (IsEqualType(src, dst))
        return true;

    if (dst->id() == TS_VAR && src->isReference())
        return true;

    if (IsIntType(src->id()) && IsIntType(dst->id()))
        return true;

    return false;
}

/// <summary>
/// promoted types are sub types, they can fit and behave same as the super-type
/// example: a char type(size 1) can fit in int(size 4).
/// </summary>
/// <param name="t">the type to be promoted</param>
/// <returns>a promoted type</returns>
Type* trill::PromoteType(Type* t)
{
    switch (t->id())
    {
    case trill::TS_UNKNOWN:
        break;
    //any integer char or boolean can fit in int
    case trill::TS_BOOL:
    case trill::TS_CHAR:
    case trill::TS_UCHAR:
    case trill::TS_INT:
    case trill::TS_UINT:
        return IntType::instance();
        break;
    //we can't use float, or instance of on object the same way.
    case trill::TS_FLOAT:
    case trill::TS_STRING:
    case trill::TS_VAR:
    case trill::TS_ARRAY:
    case trill::TS_MAP:
    default:
        break;
    }
    return t;
}

//return base types for type specifier
/// <summary>
/// given a type specifier, finds it's base type
/// </summary>
/// <param name="ts">type specifer to find base type for</param>
/// <returns>a pointer to Type class instance</returns>
Type*  trill::GetBaseType(TypeSpec ts)
{
    switch (ts)
    {
    case trill::TS_BOOL:
    case trill::TS_CHAR:
    case trill::TS_UCHAR:
        return CharType::instance();
    case trill::TS_INT:
    case trill::TS_UINT:
        return IntType::instance();
    case trill::TS_FLOAT:
        return FloatType::instance();
    case trill::TS_STRING:
        return StringType::instance();
    //anything else is reference/var type
    case trill::TS_VAR:
    case trill::TS_ARRAY:
    case trill::TS_MAP:
    default:
        break;
    }
    return VarType::instance();
}