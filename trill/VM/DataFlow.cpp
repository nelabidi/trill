//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  DataFlow.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataFlow.h"


using namespace trill;



//DataFlow::DataFlow()
//{
//}
//
//
//DataFlow::~DataFlow()
//{
//}

void trill::DataFlow::calculateLiveness(CFG* cfg, unsigned int nlocals)
{
    cfg_ = cfg;
    nlocals_ = nlocals;

    //reset CFG and calculate nbblocks,nquads
    resetCFG();
    //build dfs backward order
    buildDFS();
    //generate Use/Gen def/kill sets
    calculateBBGenKill();

    //resize liveness vectors
    quadLivness_.resize(nquads_);

    //start from end upward using dfs order / calculate basic blocks live in/out
    bool changed = true;
    while (changed)
    {
        changed = false;

        for (auto bb : dfs_order_)
            //for (BBlock* bb = cfg_->end(); bb; bb = bb->prev())
        {
            BitSet& in = bbliveness_[bb->id()].liveIn();
            BitSet out;

            out.resize(nlocals_); //this clears all bits

            //calculate out for each successor : OUT = OUT U IN(successor)
            for (unsigned i = 0; i < bb->succCount(); i++)
            {
                BitSet& in_succ = bbliveness_[bb->getSucc(i)->id()].liveIn();
                out.Union(in_succ);
            }
            //set this basic block live out
            bbliveness_[bb->id()].setLiveOut(out);

            //Liveness equation: IN = GEN U (OUT - KILL);

            BitSet& gen = bbliveness_[bb->id()].gen();
            BitSet& kill = bbliveness_[bb->id()].kill();

            changed = in.Union(gen);
            out.subtract(kill);
            changed = changed || in.Union(out);
            //if (!(old_in == in))
            //{
            //    changed = true;
            //}
            //bbliveness_[bb->id()].setLiveIn(in);
        }
    }


    //calculate live at quads, quadlivness

    for (BBlock* bb = cfg_->end(); bb; bb = bb->prev())
    {
        //empty block
        if (bb->quadCount() == 0)
            continue;

        BitSet bbLiveOut = bbliveness_[bb->id()].liveOut();
        //LIVE = OUT - DEFS + USES
        for (unsigned i = bb->quadCount(); i > 0; --i)
        {
            Quad& q = bb->getQuad(i - 1);
            BitSet& qlive = quadLivness_[q.id()];
            qlive.resize(nlocals_);
            qlive.Union(bbLiveOut);
            //for each def update kill/gen
            for (Quad::Iterator it = q.defs(); it; it++)
            {
                qlive.unset(it.operand().index());
            }
            //uses
            for (Quad::Iterator it = q.uses(); it; it++)
            {
                qlive.set(it.operand().index());
            }

            bbLiveOut = qlive;
        }
    }





}

//
// numbers basic blocks and quads in the sequence the appear in the program
// clear basic blocks visited flags
// set nquads and nbblocks
void trill::DataFlow::resetCFG()
{
    //number and clear visited flag
    unsigned bid = 0, qid = 0;
    for (BBlock* bb = cfg_->start(); bb; bb = bb->next())
    {
        bb->setId(bid++);
        bb->setVisited(false);
        //number quads

        for (unsigned i = 0; i < bb->quadCount(); i++)
        {
            bb->getQuad(i).setId(qid++);
        }
    }

    nquads_ = qid;
    nbblocks_ = bid;
}

// build dfs order,
// NOTE: assume basic blocks visited flags are cleared
void trill::DataFlow::buildDFS(bool backward /*= true*/)
{
    std::vector<BBlock*> stack;
    stack.reserve(nbblocks_);
    dfs_order_.clear();
    dfs_order_.reserve(nbblocks_);
    if (backward)
    {
        //bottom up DFS
        BBlock *bb = cfg_->end();
        stack.push_back(bb);

        while (!stack.empty())
        {
            bb = stack.back();
            stack.pop_back();
            if (bb->visited())
                continue;
            bb->setVisited(true);
            dfs_order_.push_back(bb);
            //for each preds
            for (unsigned i = 0; i < bb->predCount(); i++)
            {
                stack.push_back(bb->getPred(i));
            }
        }
    }
    else
    {
        //top down DFS
        BBlock *bb = cfg_->start();
        stack.push_back(bb);

        while (!stack.empty())
        {
            bb = stack.back();
            stack.pop_back();
            if (bb->visited())
                continue;
            bb->setVisited(true);
            dfs_order_.push_back(bb);
            //for each successor
            for (unsigned i = 0; i < bb->succCount(); i++)
            {
                stack.push_back(bb->getSucc(i));
            }
        }
    }


}

void trill::DataFlow::calculateBBGenKill()
{

    bbliveness_.resize(nbblocks_);

    //start from end upward
    for (BBlock* bb = cfg_->end(); bb; bb = bb->prev())
    {
        unsigned int bb_id = bb->id();
        BitSet &kill = bbliveness_[bb_id].kill(),
                &gen = bbliveness_[bb_id].gen();

        gen.resize(nlocals_);
        kill.resize(nlocals_);

        //kill.clear();
        //gen.clear();
        //for each quad upward
        for (unsigned i = bb->quadCount(); i > 0; --i)
        {
            Quad& q = bb->getQuad(i - 1);
            //for each def update kill/gen : GEN = GEN - def, KILL = KILL + def
            for (Quad::Iterator it = q.defs(); it; it++)
            {
                gen.unset(it.operand().index());
                kill.set(it.operand().index());
            }
            //uses /GEN :  GEN = GEN + use, KILL = KILL - use
            for (Quad::Iterator it = q.uses(); it; it++)
            {
                gen.set(it.operand().index());
                kill.unset(it.operand().index());
            }
        }
        bbliveness_[bb_id].setBlock(bb);
        //bbliveness_[bb->id()].setGen(gen);
        //bbliveness_[bb->id()].setKill(kill);

        //clear and resize live in
        bbliveness_[bb_id].liveIn().resize(nlocals_);


        //bbliveness_[bb->id()].liveIn().clear();
        //bbliveness_[bb->id()].liveOut().resize(nextTempId_);
        //bbliveness_[bb->id()].liveOut().clear();
    }
}

void trill::DataFlow::dumpBBLiveness()
{
    for (BBlock* bb = cfg_->start(); bb; bb = bb->next())
    {
        std::cout << "\nBB" << bb->id() << std::endl;;
        //std::cout << " IN: "  << bbliveness_[bb->id()].liveIn() << std::endl;
        for (unsigned i = 0; i < bb->quadCount(); i++)
        {
            std::cout << bb->getQuad(i).id();
            std::cout << "\t" << quadLivness_[bb->getQuad(i).id()] << std::endl;
        }
        //std::cout << "   OUT: " << bbliveness_[bb->id()].liveOut() << std::endl;
    }
}
