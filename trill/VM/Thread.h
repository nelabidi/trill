//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Thread.h
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#pragma once


#include "Opcode.h"

namespace trill {
    using namespace VM;

    struct Thread
    {
        Thread(Context *);
        ~Thread();

        void Execute(FunctionInfo& fnInfo);
        RuntimeValue returnValue()
        {
            return _retValue;
        }

    private:
        Context* _context;
        Runtime*  _runtime;
        RuntimeValue* _stack;
        //ByteCode*     _bytecode;
        UByteCode       _ubytecode;
        UInstruction*   _insbuffer;
        //UByteCode*    _bytecode;
        //UByteCode     _ubytecode;
        unsigned      _base, _sp, _ip;
        RuntimeValue  _retValue;

        void          pushFrame(FunctionInfo& fnInfo, int nargs = 0);
        void          popFrame();
        void          run();
    };


}

