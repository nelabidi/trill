//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Opcode.h
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#pragma once

#include "../config.h"

#include "VM.h"
namespace VM {


#define OPCODE_BITS 8

    enum Opcode : unsigned char
    {
        //first opcodes are reserved for object operators
        //call operator, _call
        VM_CALL, //method in r0, number or params in r1, return in r2
        // _add,_sub,_mul,_div
        VM_ADD,
        VM_SUB,
        VM_MUL,
        VM_DIV,
        VM_MOD,
        //unary r0 = op r0
        VM_NEG,
        VM_INC,
        VM_DEC,
        // _cmp operator
        VM_CMP,
        //_get,_set
        VM_GET, //get a property r0 obj, r1 key, r2 target
        VM_SET, //set a property r0 obj, r1 key, r2 value

        //int arithmetics r0,r1,r2
        VM_IADD,
        VM_ISUB,
        VM_IMUL,
        VM_IDIV,
        VM_IMOD,
        //unary source and target is r0
        VM_INEG,
        VM_IINC,
        VM_IDEC,

        VM_IGET, //int array, r0 == array, r1 index, r2 = value
        VM_ISET, //int array, r0 == array, r1 index, r2 = value

        //float arithmetic
        VM_FADD,
        VM_FSUB,
        VM_FMUL,
        VM_FDIV,
        VM_FMOD,
        VM_FNEG,
        VM_FINC,
        VM_FDEC,

        VM_FGET, //float array, r0 == array, r1 index, r2 = value
        VM_FSET, //float array, r0 == array, r1 index, r2 = value

        //MOVE
        VM_MOV,  //register to global
        VM_MOVG,// global to register
        VM_MOVR,// Register to register


        //load constant to register / RM mode
        VM_LOADI, //integer in M
        VM_LOADIx, // integer in ip+1
        VM_LOADF, //float in ip+1
        VM_LOADS, //string index in ip + 1

        VM_ICMP,
        VM_FCMP,

        //control r0,r1, target = ip+1, instruction offset
        VM_JL,
        VM_JLE,
        VM_JG,
        VM_JGE,
        VM_JE,
        VM_JNE,
        //direct jump, ip+1 == instruction offset
        VM_JMP,

        //push param
        VM_PUSH,  //r0
        VM_PUSHI, // constant in disp
        VM_PUSHIx, //int constant in ip+1
        VM_PUSHF, //float in ip+1
        VM_PUSHS, //string index in ip + 1


        //call
        VM_CALLx, // function info in ip + 1, r1 = nparams, r2 return


        //return
        VM_RET,   // register in r0
        VM_RETI,  // int constant in disp
        VM_RETIx, // int constant in ip+1
        VM_RETF,  // float in ip+1
        VM_RETN,  // return null

        VM_NOP,//no operation
        VM_BP, //breakpoint
        VM_HLT,//stop
    };
    static_assert ( VM_HLT < (1 << OPCODE_BITS), "Opcode overflow");

    enum OpcodeMode
    {
        VM_3R,
        VM_RM,
        VM_JX,
        VM_DISP,
        VM_IIR,  //int ip+1, int,r2
    };
    enum OpcodeType
    {
        VM_NOTYPE,
        VM_VAR,
        VM_INT,
        VM_FLOAT,
    };

    struct OpcodeInfo
    {
        Opcode opcode;
        OpcodeMode mode;
        OpcodeType type;
        char*      name;

    };


    struct RMInstruction
    {
        unsigned opcode: 8;
        unsigned r : 6;
        unsigned m : 18;
        static const unsigned RM_LIMIT = 1 << 18;
    };


    /* struct JxInstruction
     {
         unsigned opcode : 8;
         unsigned r0 : 6;
         unsigned r1 : 6;
         unsigned disp : 12;
     static const unsigned JX_LIMIT = 1 << 12;
     };*/

    struct DispInstruction
    {
        unsigned opcode : 8;
        unsigned disp : 24;
        static const unsigned DISP_LIMIT = 1 << 24;
    };


    struct Instruction
    {
        Opcode  opcode;
        RegIndex r0 : 6;
        RegIndex r1 : 6;
        RegIndex r2 : 6;
        //unsigned char r0, r1, r2;

    };

    struct UInstruction
    {
        Opcode opcode;
        RegIndex r0;
        RegIndex r1;
        RegIndex r2;
        /*unsigned opcode, r0, r1, r2;*/
        unsigned m;
        unsigned disp;

        //constant in instruction
        union
        {
            const char *s;
            float f;
            int   i;
            unsigned ui;
        } v;
        inline float Float() const
        {
            return v.f;
        }
        inline int   Int() const
        {
            return v.i;
        }
        inline unsigned Index() const
        {
            return v.ui;
        }
        //unsigned dummy;
    };

    union ByteCodeEntry
    {
        Instruction     ins;
        RMInstruction   rm_ins;
        //JxInstruction   jx_ins;
        DispInstruction disp_ins;
        unsigned        index;
        int             i;
        float           f;
        const char*     s;
    };

    struct ByteCode
    {

        inline unsigned size() const
        {
            return _bytes.size();
        }
        inline unsigned emitIndex(unsigned ui)
        {
            ByteCodeEntry e;
            e.index = ui;
            _bytes.push_back(e);
            return size() - 1;
        }
        inline unsigned emit(float f)
        {
            ByteCodeEntry e;
            e.f = f;
            _bytes.push_back(e);
            return size() - 1;
        }
        inline unsigned emit(int i)
        {
            ByteCodeEntry e;
            e.i = i;
            _bytes.push_back(e);
            return size() - 1;
        }
        inline unsigned emit(Instruction ins)
        {
            ByteCodeEntry e;
            e.ins = ins;
            _bytes.push_back(e);
            return size() - 1;
        }
        inline unsigned emit(DispInstruction ins)
        {
            ByteCodeEntry e;
            e.disp_ins = ins;
            _bytes.push_back(e);
            return size() - 1;
        }
        inline unsigned emit(RMInstruction ins)
        {
            ByteCodeEntry e;
            e.rm_ins = ins;
            _bytes.push_back(e);
            return size() - 1;
        }

        inline void emitRM(Opcode op, RegIndex r, unsigned m)
        {
            //TODO: check if instruction is of RM Mod

            ASSERT(m < RMInstruction::RM_LIMIT);
            ByteCodeEntry e;
            e.rm_ins.opcode = op;
            e.rm_ins.r = r;
            e.rm_ins.m = m;
            _bytes.push_back(e);
        }
        inline void emitR0(Opcode op, RegIndex r)
        {
            ByteCodeEntry e;
            e.ins.opcode = op;
            e.ins.r0 = r;
            _bytes.push_back(e);
        }
        inline void emit3R(Opcode op, RegIndex r0, RegIndex r1, RegIndex r2)
        {
            ByteCodeEntry e;
            e.ins.opcode = op;
            e.ins.r0 = r0;
            e.ins.r1 = r1;
            e.ins.r2 = r2;
            _bytes.push_back(e);
        }

        inline void emitDisp(Opcode op, unsigned disp)
        {
            ASSERT(disp < DispInstruction::DISP_LIMIT);

            ByteCodeEntry e;
            e.disp_ins.opcode = op;
            e.disp_ins.disp = disp;
            _bytes.push_back(e);
        }

        inline void patchDisp(unsigned int index, unsigned disp)
        {
            ASSERT(disp < DispInstruction::DISP_LIMIT);
            ASSERT(index < size());
            _bytes[index].disp_ins.disp = disp;
        }

        /*inline void patch(unsigned int index, unsigned int value)
        {
            ASSERT(index < size() && _bytes[index].index == 0);
            _bytes[index].index = value;
        }*/


        void getInstruction(unsigned index, UInstruction& inst);

        inline int  getInt(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].i;
        }
        inline float  getFloat(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].f;
        }

        inline int  getIndex(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].index;
        }
        inline ByteCodeEntry& getAt(unsigned index)
        {
            ASSERT(index < size());
            return _bytes[index];
        }
    protected:
        std::vector<ByteCodeEntry> _bytes;
    };


    struct UByteCode
    {
        UByteCode() : _bytes(nullptr), size_(0) {}

        inline void Init(UInstruction* buffer, unsigned size)
        {
            _bytes = buffer;
            size_ = size;
        }

        void Unpack(ByteCode& bc);

        inline UInstruction& getInstruction(unsigned index)
        {
            ASSERT(index < size());
            return _bytes[index];
        }

        inline int  getInt(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].v.i;
        }
        inline float  getFloat(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].v.f;
        }

        inline int  getIndex(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].v.ui;
        }
        inline const char*  getString(unsigned index) const
        {
            ASSERT(index < size());
            return _bytes[index].v.s;
        }
        inline unsigned size() const
        {
            return size_;
        }

    protected:
        UInstruction* _bytes;
        unsigned size_;

    };


    /*struct ByteCodeEmitter
    {
        ByteCodeEmitter(ByteCode* bc = nullptr): _bytecode(bc) {}


    protected:
        ByteCode* _bytecode;
    };*/


} //end namespace VM