//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  CodeGenerator.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../internal.h"
#include "../Context.h"
#include "../Compiler/Expr.h"
#include "Script.h"
#include "CodeGenerator.h"
#include "CFGBuilder.h"
#include "DataFlow.h"
#include "RegAlloc.h"
#include "Opcode.h"

using namespace trill;

CodeGenerator::CodeGenerator(Context* ctx)
    : _context(ctx)
{

}

CodeGenerator::~CodeGenerator()
{
}

unsigned trill::CodeGenerator::getGlobalIndex(Symbol* sym)
{
    ASSERT(sym && IsGlobal(sym));
    return _context->addGlobal(sym->as<VariableSymbol>());
}
unsigned trill::CodeGenerator::getFunctionIndex(char *name)
{
    return  _context->getFunction(name);
}

void trill::CodeGenerator::generate(const std::vector<Symbol*>& symbols, ByteCode* bytecode)
{

    _bytecode = bytecode;
    //walk symbols, generate functions code/body
    //define allocate global objects
    Symbol * sym;

    //for (auto sym : symbols)
    for (size_t i = symbols.size();  i > 0; i--)
    {
        sym = symbols[i - 1];
        switch (sym->kind())
        {
        case trill::SK_VARIABLE:
            _context->addGlobal(sym->as<VariableSymbol>());
            break;
        case trill::SK_FUNCTION:
        {
            generateFunction(sym->as<FunctionSymbol>());
        }
        break;
        case  trill::SK_CONST:
            break;
        case trill::SK_CLASS:
            NOT_IMPLEMENTED();
            break;
        case trill::SK_UNDEFINED:
            INTERNAL_ERROR("Undefined symbol in global scope");
            break;
        case trill::SK_LABEL:
            INTERNAL_ERROR("Label in global scope");
            break;
        case trill::SK_NATIVE:
            INTERNAL_ERROR("Native symbol in script scope");
            break;
        default:
            INTERNAL_ERROR("Unknown symbol kind");
            break;
        }
    }
    _bytecode->emitR0(VM_HLT, 0);

}

void trill::CodeGenerator::generateFunction(FunctionSymbol& function )
{

    //build CFG
    //calculate liveness/Dataflow
    //allocate registers
    //walk every quad in cfg:
    // first pass:
    //  output VM opcode in script
    //second pass
    //  resolve labels

    CFGBuilder cfgBuilder(_context);

    _cfg.reset();
    cfgBuilder.buildCFG(function, _cfg);
    _regalloc.reset();
    //data flow and register allocation for locals
    if (cfgBuilder.nlocals() > 0)
    {
        DataFlow df;
        df.calculateLiveness(&_cfg, cfgBuilder.nlocals());
        _regalloc.allocateRegs(df);
    }

    FunctionInfo fnInfo;

    fnInfo.entry = _bytecode->size();
    fnInfo.nargs = function.nargs();
    fnInfo.nregs = _regalloc.usedRegs();
    fnInfo.name = function.name();
    fnInfo.native = nullptr;

    _context->addFunction(fnInfo);//add this function info

    _jumpPatch.clear();//clear jump patch
    //for each basic block in cfg
    for (BBlock* bb = _cfg.start(); bb; bb = bb->next())
    {
        //add label to the map
        LabelSymbol* label = bb->getLabel();
        if (label)
        {
            //labelsMap_[label->name()] = label;
            label->setAddress(nextInstructionIndex());
        }
        //for each quad
        BBlock::Accept(bb, this);
    }
    //apply patch
    for (auto p : _jumpPatch)
    {
        _bytecode->patchDisp(p.insIndex, p.label->address());
    }

    //df.dumpBBLiveness();
    //_cfg.dump();

}

void trill::CodeGenerator::OnMove(Quad& q)
{
    //source, reg/global
    if (q.src().isLocal())
    {
        RegIndex r = _regalloc.getReg(q.src().index());
        if (q.target().isGlobal())
        {
            unsigned gindex = getGlobalIndex(q.target().symbol());
            _bytecode->emitRM(VM_MOV, r, gindex);
        }
        else
        {
            ASSERT(q.target().isLocal());

            RegIndex r1 = _regalloc.getReg(q.target().index());
            _bytecode->emit3R(VM_MOVR, r, r1, 0);
        }
        return;
    }

    if (q.src().isGlobal())
    {
        ASSERT(q.target().isLocal());

        RegIndex  r = _regalloc.getReg(q.target().index());
        unsigned gindex = getGlobalIndex(q.src().symbol());
        _bytecode->emitRM(VM_MOVG, r, gindex);

        return;
    }

    INTERNAL_ERROR("move with unknown source");
}

void trill::CodeGenerator::OnLoad(Quad& q)
{
    ASSERT(q.src().isConstant());
    ASSERT(q.target().isLocal());

    RegIndex r = _regalloc.getReg(q.target().index());
    switch (q.src().kind())
    {
    case trill::QOK_ICONST:
        //if int fit in rm
        if ((unsigned)q.src().Int() < RMInstruction::RM_LIMIT)
        {
            unsigned m = (unsigned)q.src().Int();
            _bytecode->emitRM(VM_LOADI, r, m);
        }
        else
        {
            _bytecode->emitR0(VM_LOADIx, r);
            _bytecode->emit(q.src().Int());
        }
        break;
    case trill::QOK_FCONST:
        _bytecode->emitR0(VM_LOADF, r);
        _bytecode->emit(q.src().Float());
        break;
    case trill::QOK_SCONST:
        _bytecode->emitR0(VM_LOADS, r);
        _bytecode->emitIndex(_context->addStringConstant(q.src().str()));
        break;
    default:
        INTERNAL_ERROR("invalid load source operand");
        break;
    }
}

void trill::CodeGenerator::OnAdd(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.src1().type();
    Opcode op =  IsIntType(ts) ? VM_IADD : IsFloatType(ts) ? VM_FADD : VM_ADD;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnSub(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_ISUB : IsFloatType(ts) ? VM_FSUB : VM_SUB;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnMul(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_IMUL : IsFloatType(ts) ? VM_FMUL : VM_MUL;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnDiv(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_IDIV : IsFloatType(ts) ? VM_FDIV : VM_DIV;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnMod(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_IMOD : IsFloatType(ts) ? VM_FMOD : VM_MOD;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnNeg(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_INEG : IsFloatType(ts) ? VM_FNEG : VM_NEG;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnNot(Quad& q)
{
    throw std::exception("The method or operation is not implemented.");
}

void trill::CodeGenerator::OnJump(Quad& q)
{
    _bytecode->emitDisp (VM_JMP, 0);
    addJumpPatch(q.src().label());
}

void trill::CodeGenerator::emitJx(Opcode op, TypeSpec type, RegIndex r0, RegIndex r1)
{
    Opcode cmp = IsIntType(type) ? VM_ICMP : IsFloatType(type) ? VM_FCMP : VM_CMP;
    _bytecode->emit3R(cmp, r0, r1, 0);
    _bytecode->emitDisp(op, 0);
}

void trill::CodeGenerator::OnJL(Quad& q)
{
    ASSERT(q.src1().isLocal());
    ASSERT(q.src2().isLocal());
    ASSERT(q.src1().type() == q.src2().type());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    TypeSpec ts = q.src1().type();

    emitJx(VM_JL, ts, r0, r1);
    /*Opcode op = IsIntType(ts) ? VM_ICMP : IsFloatType(ts) ? VM_FCMP : VM_CMP;
    _bytecode->emit3R(op, r0, r1, 0);
    _bytecode->emitDisp(VM_JL, 0);*/

    addJumpPatch(q.target().label());


}

void trill::CodeGenerator::OnJLE(Quad& q)
{
    ASSERT(q.src1().isLocal());
    ASSERT(q.src2().isLocal());
    ASSERT(q.src1().type() == q.src2().type());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_ICMP : IsFloatType(ts) ? VM_FCMP : VM_CMP;

    _bytecode->emit3R(op, r0, r1, 0);
    _bytecode->emitDisp(VM_JLE, 0);

    addJumpPatch(q.target().label());
}

void trill::CodeGenerator::OnJE(Quad& q)
{
    ASSERT(q.src1().isLocal());
    ASSERT(q.src2().isLocal());
    ASSERT(q.src1().type() == q.src2().type());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_ICMP : IsFloatType(ts) ? VM_FCMP : VM_CMP;

    _bytecode->emit3R(op, r0, r1, 0);
    _bytecode->emitDisp(VM_JE, 0);

    addJumpPatch(q.target().label());
}

void trill::CodeGenerator::OnJNE(Quad& q)
{
    ASSERT(q.src1().isLocal());
    ASSERT(q.src2().isLocal());
    ASSERT(q.src1().type() == q.src2().type());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_ICMP : IsFloatType(ts) ? VM_FCMP : VM_CMP;

    _bytecode->emit3R(op, r0, r1, 0);
    _bytecode->emitDisp(VM_JNE, 0);

    addJumpPatch(q.target().label());
}

void trill::CodeGenerator::OnJG(Quad& q)
{
    ASSERT(q.src1().isLocal());
    ASSERT(q.src2().isLocal());
    ASSERT(q.src1().type() == q.src2().type());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_ICMP : IsFloatType(ts) ? VM_FCMP : VM_CMP;

    _bytecode->emit3R(op, r0, r1, 0);
    _bytecode->emitDisp(VM_JG, 0);

    addJumpPatch(q.target().label());
}

void trill::CodeGenerator::OnJGE(Quad& q)
{
    ASSERT(q.src1().isLocal());
    ASSERT(q.src2().isLocal());
    ASSERT(q.src1().type() == q.src2().type());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    TypeSpec ts = q.src1().type();
    Opcode op = IsIntType(ts) ? VM_ICMP : IsFloatType(ts) ? VM_FCMP : VM_CMP;

    _bytecode->emit3R(op, r0, r1, 0);
    _bytecode->emitDisp(VM_JGE, 0);

    addJumpPatch(q.target().label());
}

void trill::CodeGenerator::OnCall(Quad& q)
{
    ASSERT(q.target().isLocal());

    int r2 = _regalloc.getReg(q.target().index());

    if (q.src1().isLocal())
    {
        RegIndex r0 = _regalloc.getReg(q.src1().index());
        _bytecode->emit3R(VM_CALL, r0, q.src2().Int(), r2);
    }
    else
    {
        ASSERT(q.src1().isSymbol() && (q.src1().symbol()->is<FunctionSymbol>() || q.src1().symbol()->is<NativeSymbol>()));

        unsigned int gindex = getFunctionIndex(q.src1().symbol()->name());
        _bytecode->emit3R(VM_CALLx, 0, q.src2().Int(), r2);
        _bytecode->emitIndex(gindex);
    }
}

void trill::CodeGenerator::OnPush(Quad& q)
{
    if (q.src().isLocal())
    {
        _bytecode->emit3R (VM_PUSH, _regalloc.getReg(q.src1().index()), 0, 0);
    }
    else if (q.src().isConstant())
    {
        switch (q.src().kind())
        {
        case trill::QOK_ICONST:
        {
            //check if it fits in disp
            int c = q.src().Int();
            if ((unsigned)c < DispInstruction::DISP_LIMIT)
                _bytecode->emitDisp(VM_PUSHI, c);
            else
            {
                _bytecode->emit3R (VM_PUSHIx, 0, 0, 0 );
                _bytecode->emit(c);
            }
        }
        break;
        case trill::QOK_FCONST:
            _bytecode->emit3R(VM_PUSHF, 0, 0, 0);
            _bytecode->emit(q.src().Float());
            break;
        case trill::QOK_SCONST:
            _bytecode->emit3R(VM_PUSHS, 0, 0, 0);
            _bytecode->emitIndex(_context->addStringConstant(q.src().str()));
            break;
        default:
            INTERNAL_ERROR("Unknwon operand kind");
        }
    }
    else
        NOT_IMPLEMENTED();
}

void trill::CodeGenerator::OnRet(Quad& q)
{
    if (q.src().isEmpty())
    {
        _bytecode->emitR0(VM_RETN, 0);

    }
    else if (q.src().isLocal())
    {
        _bytecode->emitR0(VM_RET, _regalloc.getReg(q.src().index()));
    }
    else if (q.src().kind() == QOK_ICONST)
    {
        int c = q.src().Int();
        if ((unsigned)c < DispInstruction::DISP_LIMIT)
            _bytecode->emitDisp(VM_RETI, c);
        else
        {
            _bytecode->emitR0 (VM_RETIx, 0);
            _bytecode->emit(q.src().Int());

        }
    }
    else if (q.src().kind() == QOK_FCONST)
    {
        _bytecode->emitR0(VM_RETF, 0);
        _bytecode->emit(q.src().Float());
    }
    else
        INTERNAL_ERROR("unknwn operand for ret");
}

void trill::CodeGenerator::OnInc(Quad& q)
{
    ASSERT(q.src().isLocal());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    TypeSpec ts = q.src().type();
    Opcode op = IsIntType(ts) ? VM_IINC : IsFloatType(ts) ? VM_FINC : VM_INC;
    _bytecode->emitR0(op, r0 );
}

void trill::CodeGenerator::OnDec(Quad& q)
{
    ASSERT(q.src().isLocal());

    RegIndex r0 = _regalloc.getReg(q.src1().index());
    TypeSpec ts = q.src().type();
    Opcode op = IsIntType(ts) ? VM_IDEC : IsFloatType(ts) ? VM_FDEC : VM_DEC;
    _bytecode->emitR0(op, r0);
}

void trill::CodeGenerator::OnGet(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.target().type();
    Opcode op = IsIntType(ts) ? VM_IGET : IsFloatType(ts) ? VM_FGET : VM_GET;
    _bytecode->emit3R(op, r0, r1, r2);
}

void trill::CodeGenerator::OnSet(Quad& q)
{
    RegIndex r0 = _regalloc.getReg(q.src1().index());
    RegIndex r1 = _regalloc.getReg(q.src2().index());
    RegIndex r2 = _regalloc.getReg(q.target().index());
    TypeSpec ts = q.target().type();
    Opcode op = IsIntType(ts) ? VM_ISET : IsFloatType(ts) ? VM_FSET : VM_SET;
    _bytecode->emit3R(op, r0, r1, r2);
}














