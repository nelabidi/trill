//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  CodeGenerator.h
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#include "CFG.h"
#include "RegAlloc.h"

namespace trill {

    using namespace VM;


    struct CodeGenerator : QuadVisitor
    {
        CodeGenerator(Context *);
        ~CodeGenerator();

        void generate(const std::vector<Symbol*>& scriptSymbols, ByteCode* bytecode);
        void generateFunction(FunctionSymbol& function);

    private:
        Context*  _context;
        ByteCode* _bytecode;
        CFG       _cfg;
        RegAlloc  _regalloc;

        struct JumpPatch
        {
            unsigned insIndex;
            LabelSymbol* label;
        };
        std::vector<JumpPatch>  _jumpPatch;
        //in-lines
        inline void addJumpPatch(LabelSymbol* label)
        {
            _jumpPatch.push_back({ currentInstructionIndex(), label });
        }
        inline unsigned currentInstructionIndex()const
        {
            return _bytecode->size() - 1;
        }
        inline unsigned nextInstructionIndex()const
        {
            return _bytecode->size();
        }

        //helpers
        unsigned getGlobalIndex(Symbol* sym);
        unsigned getFunctionIndex(char *name);
        void emitJx(Opcode op, TypeSpec type, RegIndex r0, RegIndex r1);

        //quad visitor

        virtual void OnMove(Quad& q);
        virtual void OnLoad(Quad& q);
        virtual void OnAdd(Quad& q);
        virtual void OnSub(Quad& q);
        virtual void OnMul(Quad& q);
        virtual void OnMod(Quad& q);
        virtual void OnDiv(Quad& q);
        virtual void OnNeg(Quad& q);
        virtual void OnNot(Quad& q);
        virtual void OnJump(Quad& q);
        virtual void OnJL(Quad& q);
        virtual void OnJLE(Quad& q);
        virtual void OnJE(Quad& q);
        virtual void OnJNE(Quad& q);
        virtual void OnJG(Quad& q);
        virtual void OnJGE(Quad& q);
        virtual void OnCall(Quad& q);
        virtual void OnPush(Quad& q);
        virtual void OnRet(Quad& q);

        virtual void OnInc(Quad& q);

        virtual void OnDec(Quad& q);

        virtual void OnGet(Quad& q);

        virtual void OnSet(Quad& q);

    };

}