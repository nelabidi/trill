//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Quad.cpp
// PURPOSE:
// DATE: 2015/08/26
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../config.h"
#include "../internal.h"
#include "../Compiler/Symbol.h"

#include "Quad.h"


using namespace trill;
//implementation
/*Quad::Iterator Quad::uses()
{
    Iterator it(this);
    int isrc1 = -1, isrc2 = -1, isrc3 = -1;

    if (src1().kind() == QOK_SYMBOL)
    {
        if (src1().symbol()->kind() == SK_VARIABLE)
        {
            VariableSymbol *sym = (VariableSymbol*)src1().symbol();
            if (sym->storage() == SS_LOCAL || sym->storage() == SS_TEMP)
                isrc1 = 0;
        }
    }
    else if (src1().kind() == QOK_TEMP)
    {
        isrc1 = 0;
    }

    if (src2().kind() == QOK_SYMBOL)
    {
        if (src2().symbol()->kind() == SK_VARIABLE)
        {
            VariableSymbol *sym = (VariableSymbol*)src2().symbol();
            if (sym->storage() == SS_LOCAL || sym->storage() == SS_TEMP)
                isrc2 = 1;
        }
    }
    else if (src2().kind() == QOK_TEMP)
    {
        isrc2 = 1;
    }

    if (isrc1 == -1)
    {
        isrc1 = isrc2;
        isrc2 = -1;
    }
    if (isrc2 == -1)
    {
        isrc2 = isrc3;
        isrc3 = -1;
    }

    it.index_[0] = isrc1;
    it.index_[1] = isrc2;
    //it.index_[2] = isrc3;

    return it;

}
*/
void trill::Quad::init_uses()
{
    Iterator& it = use_iterator_;

    int isrc1 = -1, isrc2 = -1, isrc3 = -1;

    /*if (src1().kind() == QOK_SYMBOL)
    {
        if (src1().symbol()->kind() == SK_VARIABLE)
        {
            VariableSymbol *sym = (VariableSymbol*)src1().symbol();
            if (sym->storage() == SS_LOCAL || sym->storage() == SS_TEMP)
                isrc1 = 0;
        }
    }*/
    if (src1().isLocal())
    {
        isrc1 = 0;
    }

    /* else if (src1().kind() == QOK_TEMP)
     {
         isrc1 = 0;
     }*/

    /*if (src2().kind() == QOK_SYMBOL)
    {
        if (src2().symbol()->kind() == SK_VARIABLE)
        {
            VariableSymbol *sym = (VariableSymbol*)src2().symbol();
            if (sym->storage() == SS_LOCAL || sym->storage() == SS_TEMP)
                isrc2 = 1;
        }
    }*/
    if (src2().isLocal())
    {
        isrc2 = 1;
    }
    if (kind() == QK_SET && target().isLocal())
    {
        isrc3 = 2;
    }
    /*else if (src2().kind() == QOK_TEMP)
    {
        isrc2 = 1;
    }*/

    if (isrc1 == -1)
    {
        isrc1 = isrc2;
        isrc2 = -1;
    }
    if (isrc2 == -1)
    {
        isrc2 = isrc3;
        isrc3 = -1;
    }

    it.index_[0] = isrc1;
    it.index_[1] = isrc2;
    it.index_[2] = isrc3;

}

void trill::Quad::init_defs()
{
    Iterator& it = defs_iterator_;

    int isrc1 = -1;

    if (target().isLocal() && kind() != QK_SET)
    {
        isrc1 = 2;
    }
    /*if (target().kind() == QOK_SYMBOL)
    {
        if (target().symbol()->kind() == SK_VARIABLE)
        {
            VariableSymbol *sym = (VariableSymbol*)target().symbol();
            if (sym->storage() == SS_LOCAL || sym->storage() == SS_TEMP)
                isrc1 = 2;
        }
    }
    else if (target().kind() == QOK_TEMP)
    {
        isrc1 = 2;
    }*/

    it.index_[0] = isrc1;
}

void trill::Quad::Accept(Quad& q, QuadVisitor* visitor)
{
    switch (q.kind())
    {
    case trill::QK_MOVE:
        visitor->OnMove(q);
        break;
    case trill::QK_LOAD:
        visitor->OnLoad(q);
        break;
    case trill::QK_ADD:
        visitor->OnAdd(q);
        break;
    case trill::QK_SUB:
        visitor->OnSub(q);
        break;
    case trill::QK_MUL:
        visitor->OnMul(q);
        break;
    case trill::QK_DIV:
        visitor->OnDiv(q);
        break;
    case trill::QK_MOD:
        visitor->OnMod(q);
        break;
    case trill::QK_NEG:
        visitor->OnNeg(q);
        break;
    case trill::QK_NOT:
        visitor->OnNot(q);
        break;
    case trill::QK_INC:
        visitor->OnInc(q);
        break;
    case trill::QK_DEC:
        visitor->OnDec(q);
        break;
    case trill::QK_JUMP:
        visitor->OnJump(q);
        break;
    case trill::QK_JL:
        visitor->OnJL(q);
        break;
    case trill::QK_JLE:
        visitor->OnJLE(q);
        break;
    case trill::QK_JE:
        visitor->OnJE(q);
        break;
    case trill::QK_JNE:
        visitor->OnJNE(q);
        break;
    case trill::QK_JG:
        visitor->OnJG(q);
        break;
    case trill::QK_JGE:
        visitor->OnJGE(q);
        break;
    case trill::QK_CALL:
        visitor->OnCall(q);
        break;
    case trill::QK_PUSH:
        visitor->OnPush(q);
        break;
    case trill::QK_RET:
        visitor->OnRet(q);
        break;
    case QK_GET:
        visitor->OnGet(q);
        break;
    case QK_SET:
        visitor->OnSet(q);
        break;
    default:
        INTERNAL_ERROR("Unknown quad kind");
    }
}


std::ostream& trill::operator<<(std::ostream& o, Quad& q)
{
    switch (q.kind())
    {
    case trill::QK_UNKNOWN:
        break;
    case trill::QK_MOVE:
        o << "move";
        break;
    case trill::QK_LOAD:
        o << "load";
        break;
    case trill::QK_ADD:
        o << "add ";
        break;
    case trill::QK_SUB:
        o << "sub ";
        break;
    case trill::QK_MUL:
        o << "mul ";
        break;
    case trill::QK_DIV:
        o << "div ";
        break;
    case trill::QK_NEG:
        o << "neg ";
        break;
    case trill::QK_NOT:
        o << "not ";
        break;
    case trill::QK_INC:
        o << "inc ";
        break;
    case trill::QK_DEC:
        o << "dec ";
        break;
    case trill::QK_JUMP:
        o << "jmp ";
        break;
    case trill::QK_JL:
        o << "jl  ";
        break;
    case trill::QK_JLE:
        o << "jle ";
        break;
    case trill::QK_JE:
        o << "je  ";
        break;
    case trill::QK_JNE:
        o << "jne ";
        break;
    case trill::QK_JG:
        o << "jg  ";
        break;
    case trill::QK_JGE:
        o << "jge ";
        break;
    case trill::QK_CALL:
        o << "call";
        break;
    case trill::QK_PUSH:
        o << "push";
        break;
    case trill::QK_RET:
        o << "ret ";
        break;
    /*case trill::QK_RETN:
        o << "retn";
        break;*/
    default:
        o << "unknown";
        break;
    }
    o << "  ";
    for (unsigned i = 0; i < 3; i++)
    {
        QuadOperand& op = q.Operands_[i];
        if (op.kind() == QOK_EMPTY)
            continue;

        if (i)
            o << ", ";

        switch (op.kind())
        {
        case trill::QOK_ICONST:
            o << op.Int();
            break;
        case trill::QOK_FCONST:
            o << op.Float();
            break;
        case trill::QOK_SCONST:
            o << op.str();
            break;
        /*case trill::QOK_TEMP:
            o << "t" << op.index();
            break;*/
        case trill::QOK_SYMBOL:
            if (op.symbol()->name())
                o << op.symbol()->name();
            else
                o << "t" << op.index();
            break;
        case trill::QOK_LABEL:
            o << op.label()->name();
            break;
        default:
            o << " unknown operand";
            break;
        }

    }
    return o;

}

