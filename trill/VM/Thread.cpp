//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Thread.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../config.h"
#include "../internal.h"

#include "../Context.h"
#include "Script.h"
#include "Thread.h"
#include "../Runtime.h"

#include <math.h> //fmodf

using namespace trill;

Thread::Thread(Context* ctx)
    : _context(ctx)
{
    _runtime = _context->getRuntime();
    _stack = (RuntimeValue*) _runtime->newAlignedBuffer(sizeof(RuntimeValue) * 1024 * 16, 64);
    LMemSet(_stack, 0, 1024 * 16 * sizeof(RuntimeValue));
    _sp = _base =  _ip = 0;
    //unpack all
    _insbuffer = (UInstruction*) _runtime->newAlignedBuffer(_context->_byteCode.size() * sizeof(UInstruction), 64);
    _ubytecode.Init(_insbuffer, _context->_byteCode.size());
    _ubytecode.Unpack(_context->_byteCode);
}


Thread::~Thread()
{
    //delete[] _stack;
    _runtime->deleteAlignedBuffer(_stack);
    _runtime->deleteAlignedBuffer(_insbuffer);

}

void trill::Thread::Execute(FunctionInfo& fnInfo)
{
    pushFrame(fnInfo);
}


void trill::Thread::pushFrame(FunctionInfo& fnInfo, int nargs /* narg= 0*/)
{
    RuntimeValue retVal;
    _ip = fnInfo.entry;
    _sp = fnInfo.nregs;
    _base = 0;

    //push sp
    _stack[_sp++].i = fnInfo.nregs;
    //push base
    _stack[_sp++].p = &_stack[0];
    //push ip
    _stack[_sp++].i = _ubytecode.size() - 1; //halt instruction;
    //ret value address
    _stack[_sp++].p = &retVal;

    run();
    _retValue = retVal;

}

void trill::Thread::popFrame()
{

}

void trill::Thread::run()
{
    bool bHalted = false;
    unsigned ip = _ip;
    unsigned base = _base;
    unsigned sp = _sp;
    int icmp = 0;
    float fcmp = 0;
    bool zflag = false, lflag = false;
    RuntimeValue* stack = base + _stack;

    while (!bHalted)
    {
        //_bytecode->getInstruction(ip++, ins);
        /*ByteCodeEntry& bc = _bytecode->getAt(ip++);
        Instruction& ins = bc.ins;
        RMInstruction& rmins = bc.rm_ins;
        DispInstruction& ins = bc.disp_ins;*/
        UInstruction& ins = _ubytecode.getInstruction(ip++);

        switch (ins.opcode)
        {
        case VM::VM_CALL:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_ADD:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_SUB:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_MUL:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_DIV:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_NEG:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_INC:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_DEC:
            NOT_IMPLEMENTED();
            break;
        case VM::VM_CMP:
            NOT_IMPLEMENTED();
            break;
        case VM_GET: //get a property r0 obj, r1 key, r2 target
        {
            Object* obj = (Object*)stack[ins.r0].p;
            stack[ins.r2].p = obj->get(stack[ins.r1].i);
        }
        break;
        case VM_SET:
            NOT_IMPLEMENTED();
            break;
        //integer operators
        case VM::VM_IADD:
            stack[ins.r2].i = stack[ins.r0].i + stack[ins.r1].i;
            break;
        case VM::VM_ISUB:
            stack[ins.r2].i = stack[ins.r0].i - stack[ins.r1].i;
            break;
        case VM::VM_IMUL:
            stack[ins.r2].i = stack[ins.r0].i * stack[ins.r1].i;
            break;
        case VM::VM_IDIV:
            //TODO: check for div by zero
            stack[ins.r2].i = stack[ins.r0].i / stack[ins.r1].i;
            break;
        case VM::VM_IMOD:
            //TODO: check for mod by zero
            stack[ins.r2].i = stack[ins.r0].i % stack[ins.r1].i;
            break;
        case VM::VM_INEG:
            stack[ins.r0].i = -stack[ins.r0].i;
            break;
        case VM::VM_IINC:
            ++stack[ins.r0].i;
            break;
        case VM::VM_IDEC:
            --stack[ins.r0].i;
            break;
        case VM::VM_IGET:
        {
            IntArrayObject *obj = (IntArrayObject*)stack[ins.r0].p;
            stack[ins.r2].i = obj->getAt(stack[ins.r1].i);
        }
        break;
        case VM::VM_ISET:
        {
            IntArrayObject *obj = (IntArrayObject*)stack[ins.r0].p;
            obj->setAt(stack[ins.r1].i, stack[ins.r2].i);
        }
        break;
        case VM::VM_FADD:
            stack[ins.r2].f = stack[ins.r0].f + stack[ins.r1].f;
            break;
        case VM::VM_FSUB:
            stack[ins.r2].f = stack[ins.r0].f - stack[ins.r1].f;
            break;
        case VM::VM_FMUL:
            stack[ins.r2].f = stack[ins.r0].f * stack[ins.r1].f;
            break;
        case VM::VM_FDIV:
            stack[ins.r2].f = stack[ins.r0].f / stack[ins.r1].f;
            break;
        case VM::VM_FMOD:
            stack[ins.r2].f = fmodf(stack[ins.r0].f, stack[ins.r1].f);
            break;
        case VM::VM_FNEG:
            stack[ins.r0].f = -stack[ins.r0].f;
            break;
        case VM::VM_FINC:
            ++stack[ins.r0].f;
            break;
        case VM::VM_FDEC:
            --stack[ins.r0].f;
            break;

        case VM::VM_MOV:
            _context->setGlobal(ins.m, stack[ins.r0]);
            break;
        case VM::VM_MOVG:
            stack[ins.r0] = _context->getGlobal(ins.m);
            break;
        case VM::VM_MOVR:
            stack[ins.r1] = stack[ins.r0];
            break;
        case VM::VM_LOADI:
            stack[ins.r0].i = ins.m;
            break;
        case VM::VM_LOADIx:
            stack[ins.r0].i = _ubytecode.getInt(ip++);
            break;
        case VM::VM_LOADF:
            stack[ins.r0].f = _ubytecode.getFloat(ip++);
            break;
        case VM::VM_LOADS:
            stack[ins.r0].s = _context->getString(_ubytecode.getIndex(ip++));
            break;
        case VM::VM_ICMP:
            icmp = stack[ins.r0].i - stack[ins.r1].i;
            zflag = icmp == 0 ? true : false;
            lflag = icmp < 0 ? true : false;
            break;
        case VM::VM_FCMP:
            fcmp = stack[ins.r0].f - stack[ins.r1].f;
            zflag = fcmp == 0.0 ? true : false;
            lflag = fcmp < 0.0 ? true : false;
            break;
        case VM::VM_JL:
            if (lflag) ip = ins.disp;
            break;
        case VM::VM_JLE:
            if (lflag || zflag) ip = ins.disp;
            break;
        case VM::VM_JG:
            if (!lflag && !zflag) ip = ins.disp;
            break;
        case VM::VM_JGE:
            if (!lflag || zflag) ip = ins.disp;
            break;
        case VM::VM_JE:
            if (zflag) ip = ins.disp;
            break;
        case VM::VM_JNE:
            if (!zflag) ip = ins.disp;
            break;
        case VM::VM_JMP:
            ip = ins.disp;
            break;
        case VM::VM_PUSH:
            _stack[sp++] = stack[ins.r0];
            break;
        case VM::VM_PUSHI:
            _stack[sp++].i = ins.disp;
            break;
        case VM::VM_PUSHIx:
            _stack[sp++].i = _ubytecode.getInt(ip++);
            break;
        case VM::VM_PUSHF:
            _stack[sp++].f = _ubytecode.getFloat(ip++);
            break;
        case VM::VM_PUSHS:
            _stack[sp++].s = _context->getString(_ubytecode.getIndex(ip++));
            break;
        case VM::VM_CALLx:
        {
            FunctionInfo fn = _context->getFunction(_ubytecode.getIndex(ip++));
            //check if native
            //TODO: this needs to be moved to push frame
            if (fn.native != nullptr)
            {
                //TODO: here trap exception/ check the return value
                Variant* params = (Variant*) (_stack + (sp - ins.r1 * 2));
                //TODO:  check stack top
                sp += 2; //reserve a return value
                //call it
                bool retVal = fn.native(_context, params , ins.r1);
                //top of the stack is the return value save it and move it to return register
                Variant v = *(Variant *)(_stack + sp - 2);
                //adjust sp
                sp -= (ins.r1 * 2) - 2;
                _stack[ins.r2].p = v.Ptr();

            }
            else
            {
                //script function
                RuntimeValue *fnbase = _stack + (sp - ins.r1);
                int saved_sp = (sp - ins.r1);
                //reserve regs
                sp += (fn.nregs - ins.r1);
                //push sp
                _stack[sp++].i = saved_sp;
                //push base
                _stack[sp++].p = stack;
                //push ip
                _stack[sp++].i = ip;
                //ret value address
                _stack[sp++].p = &stack[ins.r2];
                //update ip and base
                ip = fn.entry;
                stack = fnbase;
            }

        }
        break;
        case VM::VM_RET:
        {
            //TODO: move this to pop frame
            //set return value from r0 address
            RuntimeValue* rval = (RuntimeValue*)_stack[--sp].p;
            (*rval) = stack[ins.r0];
            //get ip
            ip = _stack[--sp].i;
            //get base
            RuntimeValue* fnbase = (RuntimeValue*)_stack[--sp].p;
            //get sp
            int saved_sp = _stack[--sp].i;
            //update sp/base
            stack = fnbase;
            sp = saved_sp;
        }
        break;
        case VM::VM_RETI:
        {
            //int in disp
            RuntimeValue* rval = (RuntimeValue*)_stack[--sp].p;
            (*rval).i = ins.disp;
            //get ip
            ip = _stack[--sp].i;
            //get base
            RuntimeValue* fnbase = (RuntimeValue*)_stack[--sp].p;
            //get sp
            int saved_sp = _stack[--sp].i;
            //update sp/base
            stack = fnbase;
            sp = saved_sp;

        }
        break;
        case VM::VM_RETIx:
        {
            //int in instruction
            RuntimeValue* rval = (RuntimeValue*)_stack[--sp].p;
            (*rval).i = _ubytecode.getInt(ip);
            //get ip
            ip = _stack[--sp].i;
            //get base
            RuntimeValue* fnbase = (RuntimeValue*)_stack[--sp].p;
            //get sp
            int saved_sp = _stack[--sp].i;
            //update sp/base
            stack = fnbase;
            sp = saved_sp;
        }
        break;
        case VM::VM_RETF:
        {
            //float in instruction
            RuntimeValue* rval = (RuntimeValue*)_stack[--sp].p;
            (*rval).f = _ubytecode.getFloat(ip);
            //get ip
            ip = _stack[--sp].i;
            //get base
            RuntimeValue* fnbase = (RuntimeValue*)_stack[--sp].p;
            //get sp
            int saved_sp = _stack[--sp].i;
            //update sp/base
            stack = fnbase;
            sp = saved_sp;
        }
        break;
        case VM::VM_RETN:
        {
            //return null
            RuntimeValue* rval = (RuntimeValue*)_stack[--sp].p;
            //TODO: return a null object here
            (*rval).p = nullptr;
            //get ip
            ip = _stack[--sp].i;
            //get base
            RuntimeValue* fnbase = (RuntimeValue*)_stack[--sp].p;
            //get sp
            int saved_sp = _stack[--sp].i;
            //update sp/base
            stack = fnbase;
            sp = saved_sp;
        }
        break;
        case VM::VM_NOP:
            break;
        case VM::VM_BP:
            break;
        case VM::VM_HLT:
            bHalted = true;
            break;
        default:
            INTERNAL_ERROR("unknown VM opcode");
        }

    }

}
