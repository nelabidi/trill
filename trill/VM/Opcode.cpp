//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Opcode.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "../config.h"
#include "Opcode.h"


using namespace VM;

void ByteCode::getInstruction(unsigned index, UInstruction& inst)
{
    ASSERT(index < size());
    //unpack instruction
    ByteCodeEntry entry = _bytes[index];

    inst.opcode = entry.ins.opcode;
    inst.r0 = entry.ins.r0;
    inst.r1 = entry.ins.r1;
    inst.r2 = entry.ins.r2;
    inst.m = entry.rm_ins.m;
    inst.disp = entry.disp_ins.disp;
    inst.v.ui = entry.index; //this hold float, string index
}

void VM::UByteCode::Unpack(ByteCode& bc)
{
    ASSERT(bc.size() == size_);
    ASSERT(_bytes != nullptr);
    for (unsigned i = 0; i < bc.size(); i++)
    {
        bc.getInstruction(i, _bytes[i]);
    }
}



//Opcode info
/*

//reserve first opcode for object operators
VM_ADD,
VM_SUB,


VM_CMP,

//int arithmetics r0,r1,r2
VM_IADD,
VM_ISUB,

//float arithmetic
VM_FADD,
VM_FSUB,

//MOVE
VM_MOV,  //register to global
VM_MOVG,// global to register

//load constant to register
VM_LOADI, //integer in instruction
VM_LOADF, //float
VM_LOADS, //string index/pointer in instruction


VM_ICMP,
VM_FCMP,


//control r0,r1, target = ip+1, instruction offset
VM_JL,
VM_JLE,
VM_JG,
VM_JGE,
VM_JE,
VM_JNE,
//direct jump, ip+1 == instruction offset
VM_JMP,

//push param
VM_PUSH,  //r0
VM_PUSHDISP, // constant in disp
VM_PUSHI, //int constant in ip+1
VM_PUSHF, //float in ip+1

//call
VM_CALL, //method in r0, number or params in r1, return in r2
VM_CALLI, // offset in ip+1, r1 =nparams, r2 return


//return
VM_RET,  // register in r0
VM_RETI, // int constant in ip+1
VM_RETF, // float in ip+1
VM_RETN, // return null

*/
OpcodeInfo gOpcodeInfo[] =
{


    /*VM_NOP, VM_3R, VM_NOTYPE, "reti",
    VM_, VM_3R, VM_NOTYPE, "retf",
    VM_RETN, VM_3R, VM_NOTYPE, "retn",*/

    VM_NOP, VM_3R, VM_NOTYPE,   "nop",
    VM_BP,  VM_3R, VM_NOTYPE,   "bp",
    VM_HLT, VM_3R, VM_NOTYPE,   "hlt",
};


