//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  FGBuilder.h
// PURPOSE:
// DATE: 2015/08/25
// NOTES:
//
//////////////////////////////////////////////////////////////////////////
#ifndef FGBuilder_h__
#define FGBuilder_h__

#pragma once

#include "../Compiler/Symbol.h"
#include "../Compiler/Expr.h"
#include "Quad.h"
#include "../Utils/BitSet.h"
#include "CFG.h"

namespace trill {

    //struct Symbol;
    struct Context;

    struct CFGBuilder : ExprVisitor
    {
    public:
        CFGBuilder(Context* ctx);
        ~CFGBuilder();
        void buildCFG(FunctionSymbol& function, CFG& cfg);
        unsigned int nlocals() const
        {
            return nextTempId_;
        }

        virtual void OnBinray(BinaryExpr& e);

        virtual void OnAssign(AssignExpr& e);

        virtual void OnValue(ValueExpr& e);

        virtual void OnUnary(UnaryExpr& e);

        virtual void OnArray(ArrayExpr& e);

        virtual void OnSymbol(SymbolExpr& e);

        virtual void OnJump(JumpExpr& e);

        virtual void OnJTrue(JTrueExpr& e);

        virtual void OnCall(CallExpr& e);

        virtual void OnList(ExprList& e);

        virtual void OnLabel(LabelExpr& e);

        virtual void OnReturn(ReturnExpr& e);

        virtual void OnArg(ArgExpr& e);



    protected:
        Context*        context_;
        CFG*            cfg_;
        BBlock*         currentBlock_;
        FunctionSymbol* function_;
        unsigned        nextTempId_;

        std::vector<VariableSymbol*>            temps_;
        std::unordered_map < char*, BBlock* >   labelsBBlockMap_;

        std::vector<QuadOperand>                operandsStack_;

        //generation flags
        bool needRHS_;
        bool pushTypes_;


        //finds a basic block for the given label or create new one if not found
        inline BBlock* getBBlockForLabel(LabelSymbol* label)
        {
            if (labelsBBlockMap_.find(label->name()) != labelsBBlockMap_.end())
            {
                return labelsBBlockMap_.at(label->name());
            }
            BBlock*  bb = cfg_->newBBlock();
            bb->setLabel(label);
            //add it to the map
            labelsBBlockMap_[label->name()] = bb;
            return bb;
        }

        inline void chainBlocks(BBlock* first, BBlock*second)
        {
            first->setNext(second);
            second->setPrev(first);
        }
        inline void chainPredSucc(BBlock* pred, BBlock* succ)
        {
            pred->addSucc(succ);
            succ->addPred(pred);
        }
        inline void pushOperand(QuadOperand& operand)
        {
            operandsStack_.push_back(operand);
        }
        inline QuadOperand popOperand()
        {
            ASSERT(operandsStack_.size() > 0);
            QuadOperand operand = operandsStack_.back();
            operandsStack_.pop_back();
            return operand;
        }
        inline QuadOperand& topOperand()
        {
            ASSERT(operandsStack_.size() > 0);
            return operandsStack_.back();
        }

        QuadOperand makeTemp(TypeSpec type);
        /*inline QuadOperand makeTemp(TypeSpec type)
        {
            QuadOperand operand;
            VariableSymbol* temp = new VariableSymbol(type, Location::empty());
            temp->setName(context_->getRuntime()->newName())
            temp->setIndex(nextTempId_++);
            temp->setStorage(SS_TEMP);
            operand.setTemp(temp);
            return operand;
        }*/



        inline void insertQuad(QuadKind kind)
        {
            Quad q(kind);
            currentBlock_->addQuad(q);
        }

        inline void insertQuad(QuadKind kind, QuadOperand& src)
        {
            Quad q(kind, src);

            currentBlock_->addQuad(q);
        }

        inline void insertQuad(QuadKind kind, QuadOperand& src, QuadOperand& target)
        {
            Quad q(kind, src, target);

            currentBlock_->addQuad(q);
        }

        inline void insertQuad(QuadKind kind, QuadOperand& src1, QuadOperand& src2, QuadOperand& target)
        {
            Quad q(kind, src1, src2, target);

            currentBlock_->addQuad(q);
        }

        inline void insertQuad(QuadKind kind, QuadOperand& src, int n, QuadOperand& target)
        {
            QuadOperand op;
            op.setInt(n);

            Quad q(kind, src, op, target);
            currentBlock_->addQuad(q);
        }

        void putInTemp(QuadOperand& operand, bool bforce = false);
    };


}

#endif // FGBuilder_h__