//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  CFG.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CFG.h"


void trill::CFG::dump()
{
    //dump quads
    for (BBlock* bb = start(); bb; bb = bb->next())
    {
        if (bb->getLabel())
        {
            std::cout << "BB" << bb->id() << ":" << std::endl << bb->getLabel()->name() << std::endl;
        }
        else
            std::cout << "BB" << bb->id() << std::endl;
        for (unsigned i = 0; i < bb->quadCount(); i++)
        {
            std::cout << "\t";
            std::cout << bb->getQuad(i) << std::endl;
        }
    }
}

void trill::BBlock::Accept(BBlock* bb, QuadVisitor *visitor)
{

    for (unsigned i = 0; i < bb->quadCount(); i++)
    {
        Quad::Accept(bb->getQuad(i), visitor);
    }
}
