//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  CFGBuilder.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "../internal.h"
#include "../Context.h"
#include "CFGBuilder.h"
#include "DataFlow.h"
#include "RegAlloc.h"


using namespace trill;

CFGBuilder::CFGBuilder(Context* context)
    : context_(context)
{
}


CFGBuilder::~CFGBuilder()
{
    for (auto t : temps_)
    {
        delete t;
    }
}

void trill::CFGBuilder::OnBinray(BinaryExpr& e)
{
    QuadKind qk;
    switch (e.subKind())
    {
    case trill::ES_UNKNOWN:
        INTERNAL_ERROR("BinaryExpr with UNKNOWN SubKind");
        break;
    case trill::ES_ADD:
        qk = QK_ADD;
        break;
    case trill::ES_SUB:
        qk = QK_SUB;
        break;
    case trill::ES_MUL:
        qk = QK_MUL;
        break;
    case trill::ES_DIV:
        qk = QK_DIV;
        break;
    case trill::ES_MOD:
        qk = QK_MOD;
        break;
    case trill::ES_SHL:
        qk = QK_SHL;
        break;
    case trill::ES_SHR:
        qk = QK_SHR;
        break;
    case trill::ES_AND:
        qk = QK_AND;
        break;
    case trill::ES_XOR:
        qk = QK_XOR;
        break;
    case trill::ES_OR:
        qk = QK_OR;
        break;
    /*case trill::ES_DOT:
        break;*/
    case trill::ES_INDEX:
        qk = QK_GET;
        break;
    /* case trill::ES_LOR:
         break;
     case trill::ES_LAND:
         break;
     case trill::ES_UADD:
         break;
     case trill::ES_USUB:
         break;
     case trill::ES_NOT:
         break;
     case trill::ES_LNOT:
         break;
     case trill::ES_FLOAT:
         break;
     case trill::ES_INT:
         break;
     case trill::ES_STRING:
         break;
     case trill::ES_NULL:
         break;
     case trill::ES_THIS:
         break;
     case trill::ES_BASE:
         break;
     case trill::ES_TRUE:
         break;
     case trill::ES_FALSE:
         break;
     case trill::ES_LT:
         break;
     case trill::ES_LTE:
         break;
     case trill::ES_GT:
         break;
     case trill::ES_GTE:
         break;
     case trill::ES_EQ:
         break;
     case trill::ES_NEQ:
         break;
     case trill::ES_IN:
         break;*/
    default:
        NOT_IMPLEMENTED();
        break;
    }

    Expr::Accept(e.left(), this);
    Expr::Accept(e.right(), this);
    QuadOperand right = popOperand();
    QuadOperand left = popOperand();
    QuadOperand target = makeTemp(e.typeId());
    pushOperand(target);

    putInTemp(left);
    putInTemp(right);
    insertQuad(qk, left, right, target);

}

void trill::CFGBuilder::OnAssign(AssignExpr& e)
{
    bool oldRHS = needRHS_;

    if (e.right()->kind() == E_ASSIGN)
    {
        needRHS_ = true;
    }
    else
    {
        needRHS_ = false;
    }

    //generate right
    Expr::Accept(e.right(), this);
    QuadOperand src = popOperand();
    QuadOperand target;
    //depends on the left expression kind
    if (e.left()->is<SymbolExpr>())
    {
        target.setSymbol(e.left()->as<SymbolExpr>().symbol());
        putInTemp(src);
        insertQuad(QK_MOVE, src, target);
    }
    else if (e.left()->is<BinaryExpr>())
    {
        //[]/dot
        BinaryExpr& bin_expr = e.left()->as<BinaryExpr>();
        if (bin_expr.subKind() == ES_INDEX)
        {
            Expr::Accept(bin_expr.left(), this);
            QuadOperand obj = popOperand();
            putInTemp(obj);

            Expr::Accept(bin_expr.right(), this);
            QuadOperand key = popOperand();
            putInTemp(key);
            putInTemp(src);
            insertQuad(QK_SET, obj, key, src);

        }
        else
            NOT_IMPLEMENTED();

    }
    else
        NOT_IMPLEMENTED();

    //Expr::Accept(e.left(), this);
    //QuadOperand target = popOperand();

    if (oldRHS)
        pushOperand(src);

    needRHS_ = oldRHS;

}

void trill::CFGBuilder::OnValue(ValueExpr& e)
{
    QuadOperand op;
    switch (e.subKind())
    {
    case trill::ES_UNKNOWN:
        INTERNAL_ERROR("ValueExpr with UNKNOWN SubKind");
        break;
    case ES_INT:
        op.setInt(e.Int());
        break;
    case ES_STRING:
        op.setStr(e.str(), e.len());
        break;
    case ES_FLOAT:
        op.setFloat(e.Float());
        break;
    default:
        NOT_IMPLEMENTED();
    }

    pushOperand(op);
}

void trill::CFGBuilder::OnUnary(UnaryExpr& e)
{
    Expr::Accept(e.expr(), this);

    QuadOperand top = popOperand();
    QuadOperand temp = top;

    QuadKind qk;
    switch (e.subKind())
    {
    case trill::ES_UADD:
        return;
    case trill::ES_USUB:
        qk = QK_NEG;
        break;
    case trill::ES_NOT:
        qk = QK_NOT;
        break;
    case trill::ES_LNOT:
        qk = QK_LNOT;
        break;
    case trill::ES_INC:
        qk = QK_INC;
        break;
    case trill::ES_DEC:
        qk = QK_DEC;
        break;
    case trill::ES_PINC:
    {
        putInTemp(temp, true);
        qk = QK_INC;
    }
    break;
    case trill::ES_PDEC:
    {
        //QuadOperand temp = popOperand();
        putInTemp(temp, true);
        qk = QK_DEC;
    }
    break;
    default:
        break;
    }
    //check if global
    if (top.isGlobal())
    {
        QuadOperand temp = top;
        putInTemp(temp);
        //insertQuad(QK_MOVE, top, temp);
        insertQuad(qk, temp);
        insertQuad(QK_MOVE, temp, top);
    }
    else
        insertQuad(qk, top);

    //if (e.subKind() == ES_PDEC || e.subKind() == ES_PINC)

    pushOperand(temp);

    //else
    //pushOperand(top);

}

void trill::CFGBuilder::OnArray(ArrayExpr& e)
{
    throw std::exception("The method or operation is not implemented.");
}

void trill::CFGBuilder::OnSymbol(SymbolExpr& e)
{

    switch (e.symbol()->kind())
    {
    case SK_VARIABLE:
    {
        VariableSymbol &var = e.symbol()->as<VariableSymbol>();
        QuadOperand symOp(e.symbol());
        switch (var.storage())
        {
        case SS_GLOBAL:
        case SS_LOCAL:
            pushOperand(symOp);
            break;

            //if global load it in a temp
            /*{

                QuadOperand temp = makeTemp(var.type());
                insertQuad(QK_MOVE, symOp, temp);
                pushOperand(temp);
            }*/

            break;
        default:
            NOT_IMPLEMENTED();
        }
    }
    break;
    default:
        NOT_IMPLEMENTED();
    }

}

void trill::CFGBuilder::OnJump(JumpExpr& e)
{
    //emit jump in current block
    insertQuad(QK_JUMP, QuadOperand(e.label()));
    //chain preds/succ
    //create new basic block /chain it to current and set current
    BBlock* target = getBBlockForLabel(e.label());
    chainPredSucc(currentBlock_, target);

    /*BBlock*  bb = CFG::newBBlock();
    chainBlocks(currentBlock_, bb);
    currentBlock_ = bb;*/

}

void trill::CFGBuilder::OnJTrue(JTrueExpr& e)
{
    //first generate the quad
    QuadKind qk;
    switch (e.subKind())
    {
    case ES_LT:
        qk = QK_JL;
        break;
    case ES_LTE:
        qk = QK_JLE;
        break;
    case ES_GT:
        qk = QK_JG;
        break;
    case ES_GTE:
        qk = QK_JGE;
        break;
    case ES_EQ:
        qk = QK_JE;
        break;
    case ES_NEQ:
        qk = QK_JNE;
        break;
    default:
        NOT_IMPLEMENTED();
        break;
    }
    Expr::Accept(e.left(), this);
    Expr::Accept(e.right(), this);
    QuadOperand right = popOperand();
    QuadOperand left = popOperand();
    QuadOperand target(e.label());

    putInTemp(left);
    putInTemp(right);
    insertQuad(qk, left, right, target);
    //now chain blocks
    BBlock* target_bblock = getBBlockForLabel(e.label());
    chainPredSucc(currentBlock_, target_bblock);
    //create next block
    BBlock* newbb = cfg_->newBBlock();
    chainBlocks(currentBlock_, newbb);
    chainPredSucc(currentBlock_, newbb);
    currentBlock_ = newbb;

}

void trill::CFGBuilder::OnArg(ArgExpr& e)
{
    Expr::Accept(e.arg(), this);
    QuadOperand op = popOperand();
    if (pushTypes_)
    {
        QuadOperand optype(op.type());
        insertQuad(QK_PUSH, optype);
    }

    if (op.isGlobal())
        putInTemp(op);

    insertQuad(QK_PUSH, op);
}


void trill::CFGBuilder::OnCall(CallExpr& e)
{
    if (e.function()->as<SymbolExpr>().symbol()->is<NativeSymbol>())
    {
        //native we push args type
        pushTypes_ = true;
    }
    //generate args
    ExprList::Accept(e.args(), this);
    pushTypes_ = false;

    int args_count = 0;
    //calculate args count
    for (Expr* arg = e.args(); arg != nullptr; arg = arg->next())
        args_count++;

    QuadOperand opCall, opRet;
    opCall.setSymbol(e.function()->as<SymbolExpr>().symbol());
    opRet = makeTemp(e.typeId());
    insertQuad(QK_CALL, opCall, args_count, opRet);
    pushOperand(opRet);

}

void trill::CFGBuilder::OnList(ExprList& e)
{
    //throw std::exception("The method or operation is not implemented.");
    INTERNAL_ERROR("ExprList in statement list!");
}

void trill::CFGBuilder::OnLabel(LabelExpr& e)
{
    //on a label we create a new bblock if it's not already created by a jump instruction
    //and set it to current bblock

    BBlock *bb = getBBlockForLabel(e.label());

    chainBlocks(currentBlock_, bb);

    //chain successor and predecessor if last instruction is different than jump
    //imply basic block have no successor
    if (currentBlock_->succCount() == 0)
        chainPredSucc(currentBlock_, bb);

    currentBlock_ = bb;

}

void trill::CFGBuilder::OnReturn(ReturnExpr& e)
{
    if (e.expr())
    {
        Expr::Accept(e.expr(), this);
        QuadOperand ret = popOperand();
        insertQuad(QK_RET, ret);
    }
    else
    {
        insertQuad(QK_RET, QuadOperand::empty());
    }

    //chain successor/predecessor
    chainPredSucc(currentBlock_, cfg_->end());

}




void trill::CFGBuilder::buildCFG(FunctionSymbol& function, CFG& cfg)
{
    cfg_ = &cfg;
    function_ = &function;
    currentBlock_ = cfg_->start();
    nextTempId_ = function.nlocals();
    needRHS_ = false;
    pushTypes_ = false;

    //walk Expression sequence and generate
    for (Expr* seq = function_->body(); seq != nullptr; seq = seq->next())
    {
        Expr::Accept(seq, this);
        operandsStack_.clear();
    }

    //chain current and end block
    chainBlocks(currentBlock_, cfg_->end());
    //chain successor/predecessor
    chainPredSucc(currentBlock_, cfg_->end());



}

//put constant or global in register using MOVE/LOAD
void trill::CFGBuilder::putInTemp(QuadOperand& operand, bool force /*false*/)
{
    QuadKind qk = QK_LOAD;
    //if constant or global put it in a register/temp
    TypeSpec ts = TS_UNKNOWN;
    switch (operand.kind())
    {
    case trill::QOK_ICONST:
        ts = TS_INT;
        break;
    case trill::QOK_FCONST:
        ts = TS_FLOAT;
        break;
    case trill::QOK_SCONST:
        ts = TS_STRING;
        break;
    case QOK_SYMBOL:
        if (force || operand.isGlobal())
        {
            qk = QK_MOVE;
            //use promoted type
            ts = operand.type();
        }
        else
            return;
        break;
    default:
        return;
    }
    QuadOperand temp = makeTemp(ts);
    insertQuad(qk, operand, temp);
    operand = temp;
}

trill::QuadOperand trill::CFGBuilder::makeTemp(TypeSpec ts)
{
    QuadOperand operand;
    Type* type = GetBaseType(ts);
    //promot types
    type = PromoteType(type);
    VariableSymbol* temp = new VariableSymbol(type, Location::empty());
    //char name[80];
    //sprintf_s(name, sizeof(name), "t%d", nextTempId_);
    //temp->setName(context_->getRuntime()->newName(name, strlen(name)));
    temp->setName(nullptr);
    temp->setIndex(nextTempId_++);
    temp->setStorage(SS_LOCAL);
    operand.setSymbol(temp);
    temps_.push_back(temp);
    return operand;
}

