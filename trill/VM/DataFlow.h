//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  DataFlow.h
// PURPOSE:
// DATE: 2015/08/26
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#ifndef DataFlow_h__
#define DataFlow_h__
#pragma once

#include "../Utils/BitSet.h"
#include "CFG.h"

namespace trill {


    struct BBlockLiveness
    {
        inline BBlock* block() const
        {
            return bb_;
        }
        inline void setBlock(BBlock *bb)
        {
            bb_ = bb;
        }
        inline BitSet& liveIn()
        {
            return live_in_;
        }
        inline BitSet& liveOut()
        {
            return live_out_;
        }

        inline void setLiveIn(BitSet& bitset)
        {
            live_in_ = bitset;
        }
        inline void setLiveOut(BitSet& bitset)
        {
            live_out_ = bitset;
        }

        inline BitSet& gen()
        {
            return gen_;
        }
        inline BitSet& kill()
        {
            return kill_;
        }

        inline void setGen(BitSet& bitset)
        {
            gen_ = bitset;
        }
        inline void setKill(BitSet& bitset)
        {
            kill_ = bitset;
        }


    protected:
        BBlock *bb_;
        BitSet  live_in_, gen_;
        BitSet  live_out_, kill_;
    };


    struct DataFlow
    {
        //DataFlow();
        //~DataFlow();

        inline std::vector<BitSet>& quadLiveness()
        {
            return quadLivness_;
        }
        inline std::vector<BBlockLiveness>& basicBlocksLiveness()
        {
            return bbliveness_;
        }
        inline unsigned nlocals() const
        {
            return nlocals_;
        }
        inline unsigned nquads() const
        {
            return nquads_;
        }

        void calculateLiveness(CFG* cfg, unsigned int nlocals);

        //for debug purpose
        void dumpBBLiveness();

    protected:
        std::vector<BBlockLiveness>  bbliveness_;
        std::vector<BitSet>    quadLivness_;
        CFG*                   cfg_;
        unsigned               nlocals_;
        unsigned               nbblocks_;
        unsigned               nquads_;


        std::vector<BBlock*>   dfs_order_;

        void resetCFG();
        void buildDFS(bool backward = true);
        void calculateBBGenKill();
    };


}




#endif // DataFlow_h__


