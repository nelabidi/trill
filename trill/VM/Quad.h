//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Quad.h
// PURPOSE:
// DATE: 2015/08/25
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef Quad_h__
#define Quad_h__


namespace trill {

    enum QuadOperandKind
    {
        QOK_EMPTY,
        QOK_ICONST,
        QOK_FCONST,
        QOK_SCONST,
        // QOK_TEMP,
        QOK_SYMBOL,
        QOK_LABEL,
    };

    struct QuadOperand
    {
        QuadOperand()
            : kind_(QOK_EMPTY)
        {
            v_.ui = 0;
            u_.sym = nullptr;
            type_ = TS_UNKNOWN;
        }

        QuadOperand(LabelSymbol *sym)
        {
            setLabel(sym);
        }

        QuadOperand(Symbol *sym)
        {
            setSymbol(sym);
        }
        QuadOperand(int i)
        {
            setInt(i);
        }

        inline QuadOperandKind kind() const
        {
            return kind_;
        }

        inline Symbol* symbol() const
        {
            ASSERT(kind_ == QOK_SYMBOL );
            return u_.sym;
        }

        inline LabelSymbol* label() const
        {
            ASSERT(kind_ == QOK_LABEL);
            return (LabelSymbol*)u_.sym;
        }

        inline void setSymbol(Symbol *sym)
        {
            kind_ = QOK_SYMBOL;
            u_.sym = sym;
            type_ = sym ? ((SourceSymbol*)sym)->typeId() : TS_VAR;
        }

        inline void setLabel(LabelSymbol* label)
        {
            kind_ = QOK_LABEL;
            u_.sym = label;
            type_ = TS_UNKNOWN;
        }
        inline TypeSpec type() const
        {
            return type_;
        }

        inline char* str() const
        {
            ASSERT(kind_ == QOK_SCONST);
            return v_.str;
        }
        inline unsigned len() const
        {
            ASSERT(kind_ == QOK_SCONST);
            return u_.len;
        }

        inline void setStr(char *s, unsigned len)
        {
            kind_ = QOK_SCONST;
            u_.len = len;
            v_.str = s;
            type_ = TS_STRING;
        }

        inline void setInt(int i)
        {
            type_ = TS_INT;
            kind_ = QOK_ICONST;
            v_.i = i;
        }
        inline void setFloat(float f)
        {
            type_ = TS_FLOAT;
            kind_ = QOK_FCONST;
            v_.f = f;
        }

        inline int Int() const
        {
            ASSERT(kind_ == QOK_ICONST);
            return v_.i;
        }

        inline float Float() const
        {
            ASSERT(kind_ == QOK_FCONST);
            return v_.f;
        }

        inline unsigned index() const
        {
            ASSERT(isLocal());
            return ((VariableSymbol*)symbol())->index();
        }


        inline bool isSymbol() const
        {
            return kind_ == QOK_SYMBOL;
        }

        inline bool isLocal() const
        {
            return kind_ == QOK_SYMBOL && IsLocal(symbol());
        }

        inline bool isGlobal() const
        {
            return kind_ == QOK_SYMBOL && IsGlobal(symbol());
        }

        inline bool isConstant() const
        {
            return kind_ == QOK_ICONST || kind_ == QOK_FCONST || kind_ == QOK_SCONST;
        }
        inline bool isEmpty() const
        {
            return kind_ == QOK_EMPTY;
        }
        static  QuadOperand empty()
        {
            static QuadOperand q;

            return q;
        }

    protected:

        QuadOperandKind kind_;
        union
        {
            int         i;  //int const
            unsigned    ui; //uint const/ temp index /register index
            float       f;
            char       *str;
        } v_;
        union
        {
            Symbol*     sym; //symbol,label, or symbol which temp is referring to
            unsigned    len; //string len
        } u_;
        TypeSpec    type_; //temp type

    };


    enum QuadKind
    {
        QK_UNKNOWN,

        QK_MOVE, //move global in register, (src,target)
        QK_LOAD, //load constant (src, target)
        QK_SET,  //set object, obj,key,target==src
        QK_GET,

        //binary (src1,src2,target
        QK_ADD,
        QK_SUB,
        QK_MUL,
        QK_DIV,
        QK_MOD,
        QK_SHL,
        QK_SHR,
        QK_AND,
        QK_XOR,
        QK_OR,

        //unary src1/src
        QK_NEG,
        QK_NOT, //binary
        QK_LNOT,
        QK_INC,
        QK_DEC,

        //control src == label
        QK_JUMP,
        QK_JL, //src1,src2, target(label)
        QK_JLE,
        QK_JE,
        QK_JNE,
        QK_JG,
        QK_JGE,

        //calls
        QK_CALL, //src1,int,target
        QK_PUSH, //param in src
        QK_RET, // reg/constant/symbol in in src or empty

    };

    struct QuadVisitor;


    struct Quad
    {

        Quad(QuadKind kind = QK_UNKNOWN)
            : kind_(kind)
        {
        }

        Quad(QuadKind kind, QuadOperand& src)
            : kind_(kind)
        {
            Operands_[0] = src;
            init_defs();
            init_uses();
        }

        Quad(QuadKind kind, QuadOperand& src, QuadOperand& target)
            : kind_(kind)
        {
            Operands_[0] = src;
            Operands_[2] = target;
            init_defs();
            init_uses();

        }

        Quad(QuadKind kind, QuadOperand& src1, QuadOperand& src2, QuadOperand& target)
            : kind_(kind)
        {
            Operands_[0] = src1;
            Operands_[1] = src2;
            Operands_[2] = target;
            init_defs();
            init_uses();

        }

        inline QuadKind kind() const
        {
            return kind_;
        }

        QuadOperand& src()
        {
            return Operands_[0];
        }

        QuadOperand& src1()
        {
            return Operands_[0];
        }

        QuadOperand& src2()
        {
            return Operands_[1];
        }
        QuadOperand& target()
        {
            return Operands_[2];
        }

        //iterator
        struct Iterator
        {
            Iterator(Quad* quad = nullptr) :
                quad_(quad), current_(0)
            {
                index_[0] = index_[1] = index_[2] = index_[3] = -1;
            }
            inline void operator++()
            {
                current_++;
            }
            inline void operator++(int)
            {
                current_++;
            }
            inline operator bool(void)
            {
                return isValid();
            }
            inline operator bool(void) const
            {
                return isValid();
            }
            inline QuadOperand& operator*() const
            {
                return operand();
            }
            inline QuadOperand& operand() const
            {
                ASSERT(current_ < 3  && index_[current_] != -1);
                return  quad_->Operands_[index_[current_]];
            }
        protected:
            Quad*       quad_;
            unsigned    current_;
            unsigned    index_[4];
            inline void reset()
            {
                current_ = 0;
            }
            inline bool isValid() const
            {
                return index_[current_] != -1;
            }
            friend struct Quad;

        };

        inline Iterator& uses()
        {
            use_iterator_.reset();
            use_iterator_.quad_ = this;
            return use_iterator_;
        }
        inline Iterator& defs()
        {
            defs_iterator_.reset();
            defs_iterator_.quad_ = this;
            return defs_iterator_;
        }

        inline unsigned id() const
        {
            return id_;
        }
        inline void setId(unsigned id)
        {
            id_ = id;
        }

        static void Accept(Quad& q, QuadVisitor* visitor);

    protected:

        QuadKind kind_;
        QuadOperand Operands_[3];
        Iterator  use_iterator_, defs_iterator_;
        unsigned    id_;

        void init_uses();
        void init_defs();

        friend std::ostream& operator<<(std::ostream&, Quad&);
    };

    std::ostream& operator<<(std::ostream&, Quad&);

    struct QuadVisitor
    {
        virtual void OnMove(Quad& q) = 0;
        virtual void OnLoad(Quad& q) = 0;
        virtual void OnGet(Quad& q) = 0;
        virtual void OnSet(Quad& q) = 0;

        virtual void OnAdd(Quad& q) = 0;
        virtual void OnSub(Quad& q) = 0;
        virtual void OnMul(Quad& q) = 0;
        virtual void OnDiv(Quad& q) = 0;
        virtual void OnMod(Quad& q) = 0;


        virtual void OnNeg(Quad& q) = 0;
        virtual void OnNot(Quad& q) = 0;
        virtual void OnInc(Quad& q) = 0;
        virtual void OnDec(Quad& q) = 0;
        //virtual void On(Quad& q) = 0;

        virtual void OnJump(Quad& q) = 0;
        virtual void OnJL(Quad& q) = 0;
        virtual void OnJLE(Quad& q) = 0;
        virtual void OnJE(Quad& q) = 0;
        virtual void OnJNE(Quad& q) = 0;
        virtual void OnJG(Quad& q) = 0;
        virtual void OnJGE(Quad& q) = 0;
        virtual void OnCall(Quad& q) = 0;
        virtual void OnPush(Quad& q) = 0;
        virtual void OnRet(Quad& q) = 0;

    };




}









#endif // Quad_h__