//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  CFG.h
// PURPOSE:
// DATE: 2015/08/26
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef CFG_h__
#define CFG_h__

#include "../config.h"
#include "../Compiler/Symbol.h"
#include "Quad.h"

namespace trill {

    struct CFG;

    struct BBlock
    {
        inline unsigned id() const
        {
            return id_;
        };
        inline void setId(unsigned id)
        {
            id_ = id;
        }

        inline LabelSymbol* getLabel() const
        {
            return label_;
        }
        inline void setLabel(LabelSymbol* label)
        {
            label_ = label;
        }
        inline BBlock *prev() const
        {
            return prev_;
        }
        inline BBlock *next() const
        {
            return next_;
        }
        inline void setPrev(BBlock *bb)
        {
            prev_ = bb;
        }
        inline void setNext(BBlock *bb)
        {
            next_ = bb;
        }

        inline unsigned quadCount() const
        {
            return quads_.size();
        }
        inline Quad& getQuad(unsigned index)
        {
            ASSERT(index < quadCount());
            return quads_[index];
        }
        inline void addQuad(Quad& quad)
        {
            quads_.push_back(quad);
        }
        inline unsigned succCount() const
        {
            return succ_.size();
        }
        inline BBlock* getSucc(unsigned index)
        {
            ASSERT(index < succCount());
            return succ_[index];
        }

        bool hasPred(BBlock* bb)
        {
            for (auto b : preds_)
            {
                if (b == bb) return true;
            }
            return false;
        }

        bool hasSucc(BBlock* bb)
        {
            for (auto b : succ_)
            {
                if (b == bb) return true;
            }
            return false;
        }


        inline void addSucc(BBlock *bb)
        {
            if (!hasSucc(bb))
                succ_.push_back(bb);
        }
        inline void addPred(BBlock *bb)
        {
            if (!hasPred(bb))
                preds_.push_back(bb);
        }


        inline unsigned predCount() const
        {
            return preds_.size();
        }
        inline BBlock* getPred(unsigned index)
        {
            ASSERT(index < predCount());
            return preds_[index];
        }
        inline bool visited() const
        {
            return visited_;
        }
        inline void setVisited(bool flag)
        {
            visited_ = flag;
        }

        static void Accept(BBlock* bb, QuadVisitor *visitor);

    protected:
        BBlock()
            : next_(nullptr),
              prev_(nullptr),
              label_(nullptr)
        {
            quads_.reserve(64);
        }
        friend struct CFG;
        BBlock* next_;
        BBlock* prev_;
        LabelSymbol* label_;
        std::vector<Quad> quads_;
        std::vector<BBlock*> succ_;
        std::vector<BBlock*> preds_;
        bool                 visited_;
        unsigned            id_;
    };


    struct CFG
    {

        CFG()
            : start_(nullptr),
              end_(nullptr)
        {
            start_ = new BBlock();
            end_ = new BBlock();
            blocks_.reserve(64);
        }
        ~CFG()
        {
            if (start_) delete start_;
            if (end_) delete end_;
            for (auto bb : blocks_)
            {
                delete bb;
            }
        }

        inline BBlock* start() const
        {
            return start_;
        }
        inline BBlock* end() const
        {
            return end_;
        }

        inline bool isEndBlock(BBlock *bb) const
        {
            return bb == end_;
        }
        inline bool isStartBlock(BBlock *bb) const
        {
            return bb == start_;
        }

        inline BBlock* newBBlock()
        {
            BBlock *bb = new BBlock();
            blocks_.push_back(bb);
            return bb;
        }
        inline void reset()
        {
            if (start_) delete start_;
            if (end_) delete end_;
            for (auto bb : blocks_)
            {
                delete bb;
            }
            blocks_.clear();
            start_ = new BBlock();
            end_ = new BBlock();
            blocks_.reserve(64);

        }
        //for debug
        void dump();
    protected:
        BBlock* start_;
        BBlock* end_;
        std::vector<BBlock*> blocks_;
    };
}


#endif // CFG_h__

