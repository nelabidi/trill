#include "stdafx.h"
#include "RegAlloc.h"

#include <algorithm>  //sort
#include <list>

using namespace trill;

//allocate registers using linear scan /bin packing algorithm
void trill::RegAlloc::allocateRegs(DataFlow& df)
{
    reset();

    liveness_ = df.quadLiveness();
    nlocals_ = df.nlocals();
    nquads_ = df.nquads();

    buildIntervals();

    //clear regs
    regs_.resize(255);
    used_regs_ = 0;
    std::vector<Interval> sorted_intervals;
    sorted_intervals = intervals_;

    std::sort(sorted_intervals.begin(), sorted_intervals.end(), Interval::sort_start);
    std::list<Interval*> active;

    //for each interval i in order of increasing start point
    for (unsigned i = 0; i < nlocals_; i++)
    {
        Interval& current = sorted_intervals[i];
        Interval *pactive = active.empty() ? nullptr : active.front();

        //expire intervals in active
        while (pactive && pactive->end < current.start)
        {
            //free reg
            regs_[pactive->reg] = 0;
            active.pop_front();
            pactive = active.empty() ? nullptr : active.front();
        }

        //allocate register for this interval
        unsigned reg = 0;
        while (reg < regs_.size() && regs_[reg] != 0)
            reg++;
        if (reg > used_regs_)
            used_regs_ = reg;

        current.reg = reg;
        regs_[reg] = 1;

        //add current and sort
        active.push_back(&sorted_intervals[i]);
        active.sort(Interval::sort_end);

    }
    //map regs
    for (auto interval : sorted_intervals)
    {
        //TODO: intervals with no start are dead locals, instructions that defines them needs to be removed
        if (interval.start == -1)
            //assign unsed reg
            intervals_[interval.local].reg = used_regs_++;
        else
            intervals_[interval.local].reg = interval.reg;
    }


}



void trill::RegAlloc::buildIntervals()
{
    intervals_.resize(nlocals_);
    //assign locals index
    for (unsigned i = 0; i < nlocals_; i++)
    {
        intervals_[i].local = i;
    }
    //for each quad/local
    for (unsigned i = 0; i < liveness_.size(); i++)
    {
        BitSet&  bs = liveness_[i];
        //for each local
        for (unsigned j = 0; j < bs.size(); j++)
        {
            //check if live
            if (bs[j])
            {
                Interval& interval = intervals_[j];
                //has a start
                if (interval.start == -1)
                {
                    interval.start = i;
                }
                //update end
                interval.end = i;
            }
        }
    }


}

void trill::RegAlloc::reset()
{
    liveness_.clear();
    nlocals_ = 0;
    nquads_ = 0;
    used_regs_ = 0;
    intervals_.clear();
    regs_.clear();
}
