//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  RegAlloc.h
// PURPOSE:
// DATE: 2015/08/26
// NOTES:
//
//////////////////////////////////////////////////////////////////////////
#ifndef RegAlloc_h__
#define RegAlloc_h__
#pragma once


#include "DataFlow.h"
#include "VM.h"


namespace trill {


    struct RegAlloc
    {


        void allocateRegs(DataFlow& df);
        inline VM::RegIndex getReg(unsigned int local) const
        {
            ASSERT(local < nlocals_);
            return intervals_[local].reg;
        }

        inline unsigned usedRegs() const
        {
            return used_regs_ + 1;
        }
        void reset();

    protected:
        std::vector<BitSet> liveness_;
        unsigned            nlocals_;
        unsigned            nquads_;
        unsigned            used_regs_;

        //linear scan structs
        struct Interval
        {
            int     start;
            int     end;
            int     reg;
            int    local;
            Interval()
            {
                local = reg = start = end = -1;

            }
            //sort by increasing end point
            static bool sort_end(Interval* left, Interval* right)
            {
                return left->end < right->end;
            }
            static bool sort_start(const Interval& left, const Interval& right)
            {
                return left.start < right.start;
            }
        };
        std::vector<Interval>  intervals_;
        std::vector<char>       regs_;

        void buildIntervals();

    };

}


#endif // RegAlloc_h__

