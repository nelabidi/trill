//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  SourceInfo.h
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma  once

namespace trill {

    struct SourceInfo
    {
        char   *source;
        unsigned bsize; //size in byte of m_source includes null character
        char *   filename;
        unsigned id;
    };

}