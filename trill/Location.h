//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Location.h
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma  once

namespace trill {

    struct Location
    {
        unsigned  lineno;
        unsigned  column;
        static const Location& empty()
        {
            static Location loc;
            return loc;
        }
    };


}//end namespace trill
