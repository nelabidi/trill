//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Runtime.h
// PURPOSE:
// DATE: 2015/08/20
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#include "SourceInfo.h"
#include "Types.h"
#include "ErrorReporter.h"
#include "Variant.h"
#include "Objects/Object.h"


namespace trill {

    //forward declaration
    struct Context;

    struct Runtime
    {
        Runtime();
        ~Runtime();
        //Error reporting
        inline void SetErrorReporter(ErrorReporter * reporter)
        {
            _errorReporter = reporter;
        }
        inline ErrorReporter* GetErrorReporter()
        {
            return _errorReporter;
        }

        //API
        Context* NewContext();
        void     DestroyContext(Context*);

        //Objects API
        SourceInfo* NewSourceInfo(const char* script);
        SourceInfo* NewSourceInfo(const char*sourceName, FILE* file);
        void        DestroySourceInfo(SourceInfo *);

        //Factories
        char *      newName(const char* ident, int len);

        //objects factory
        Object*    newArrayObject(Context*ctx, Type* t);

        template<typename T>
        ArrayObject<T>*    newSubArray(ArrayObject<T>* parent);


    private:
        ErrorReporter*      _errorReporter;
        ConsoleErrorReporter _consoleErrorReporter;


        std::unordered_map<unsigned, std::string> _strings;

        friend struct Context;
        friend struct Thread;

        void * newAlignedBuffer(unsigned size, unsigned alignement);
        void   deleteAlignedBuffer(void* p);

    };

    //TODO: this needs GC
    template<typename T>
    ArrayObject<T>
    * trill::Runtime::newSubArray(ArrayObject<T>* parent)
    {
        ASSERT(parent && parent->type_->isDerived());

        Type* dt = parent->type_->derivedType();
        ASSERT(dt->is<ArrayType>());

        return new  ArrayObject<T>(parent->context(), &dt->as<ArrayType>());
    }


}