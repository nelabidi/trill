//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill, scripting language
// FILE:  config.h
// PURPOSE: Common project configuration goes here
// DATE: 2015/08/20
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////


#ifndef config_h__
#define config_h__


#define UNNAMED_SOURCE "noname"

//conflicts in grammar.h and windows sdk
#ifdef IN
#undef IN
#endif

#ifdef CONST
#undef CONST
#endif


#define LStrlen strlen
#define LMemSet memset
#define ASSERT assert




#endif // config_h__