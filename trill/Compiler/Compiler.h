//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill scripting language
// FILE:  Compiler.h
// PURPOSE: Language front end, the compiler parse the source and generate
//          a symbol table in the passed global scope to compileSource()
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#include "Expr.h"
#include "Scope.h"
#include "Semantics.h"
#include "FoldExpr.h"

namespace trill {

    struct  ErrorReporter;
    struct  Runtime;
    struct  Context;
    struct  Symbol;
    struct  SourceInfo;
    struct  Scope;


    struct Compiler
    {
        Compiler(Context* );
        ~Compiler();
        //getters
        const std::vector<Symbol*>& getScriptSymbols() const
        {
            return _scriptScope.symbols();
        }
        inline Context* getContext() const
        {
            return _context;
        }
        Runtime* getRuntime() const;

        //entry
        bool compileSource(SourceInfo* srcInfo, Scope* global);

        //Error reporting
        void  ReportError(char *message);
        void  ReportError(Location& location, char *message, ...);
        void  ReportSyntaxError(Location& location, char *message);
        void  ReportWarning(Location& location, char *message, ...);

        //objects allocation
        char* newString(const char *str, int len);
        char* newName(const char* name, int len);

        //types
        ArrayType*  newArrayType(Type *derivedType, unsigned nelement = 0);
        TypeList*   newTypeList(Type* t = nullptr);

        //expressions
        ValueExpr * newValueExpr(ValueExprSubKind subkind, Value& value, Location& loc);
        SymbolExpr* newSymbolExpr(Symbol *symbol, Location& loc);
        BinaryExpr* newBinaryExpr(BinExprSubKind subkind, Expr* left, Expr*right, Location&loc);
        CallExpr*   newCallExpr(Expr* func, Expr* args, Location& loc);
        ExprList*   newExprList(Expr* head, Expr*tail);
        UnaryExpr*  newUnaryExpr(UnaryExprSubKind subkind, Expr* expr, Location& loc);
        AssignExpr* newAssignExpr(Expr* left, Expr*right, Location& loc);
        ArrayExpr*  newArrayExpr(Expr* fields, Location& loc);
        LabelExpr*  newLabelExpr(LabelSymbol*);
        LabelExpr*  newLabel(char *name);
        JumpExpr*   newJumpExpr(Expr* label);
        JTrueExpr*  newJTrueExpr(BinExprSubKind subkind, Expr* label);
        ReturnExpr* newReturnExpr(Expr*, Location& loc);
        ArgExpr*    newArgExpr(Expr*);

        //symbols
        VariableSymbol* newVariableSymbol(char* name, Type* type, Location& loc);
        FunctionSymbol* newFunctionSymbol(char* name, Type* type, Location& loc);
        LabelSymbol*    newLabelSymbol(char *name);
        ConstSymbol*    newConstSymbol(ValueExpr*, Type* type, Location& loc);

        void declareSymbol(SymbolExpr& expr, Type* type);
        FunctionSymbol* declareFunction(char *name, Type* type, Location& loc);
        void declareConst(char* name, ValueExpr* value, Location& loc);

        Symbol* findSymbol(char *name, bool bnestedScopes = true);
        //scopes
        void enterScope();
        void leaveScope();
        void pushFunction(FunctionSymbol* fn);
        FunctionSymbol* popFunction();
        FunctionSymbol* currentFunction();

        //semantics
        void checkExpression(Expr* e, ExprInfo& info);
        void checkExpression(Expr* e);
        void onParsingFinished(Expr *body);
        Expr* foldExpression(Expr* e);

        //debug purpose
        void DumpSymbols(const char* symbolFile);

    private:
        ErrorReporter* errorReporter();

        Context* _context;
        SourceInfo* _source;
        int  _nErrors, _nWarnings;
        std::unordered_map<unsigned, std::string> _strings;
        unsigned _nextSymbolId;
        unsigned _nextLabelId;
        LocalScope _scriptScope;
        Scope*   _currentScope;

        std::vector<FunctionSymbol *> _functionsStack;
        FunctionSymbol _mainFunction;

        ExprChecker  _exprChecker;
        FoldExpr    _exprFold;
    };

}

