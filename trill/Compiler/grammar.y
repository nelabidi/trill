%include{
//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill, Scritping language
// FILE:  grammar.y /grammar.cpp / grammar.h
// PURPOSE: define Trill langauge in Lemon parser syntax
// DATE: 2015/08/28
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common.h"
#include "Symbol.h"
#include "Expr.h"
#include "Compiler.h"
#include "Lexer.h"
#include "Parser.h"

#pragma warning(disable:4065; disable:4996)
using namespace trill;

}

%syntax_error {
	char *message = (char *) _alloca(TOKEN.len + 50);
    sprintf(message,"Parser : unexpected `%s`",TOKEN.str,TOKEN.len);
	_compiler->ReportSyntaxError(TOKEN.location,message);
}

//unused derivtives
//%extra_argument { Parser *parser }
//%token_destructor{ %token_destructor here }
//%destructor program { { free($$); } }

%stack_size 300

%token_type {Token }

%type statmentList{ Expr* }
%type declOrStmtList{ Expr* }
%type declOrStmt{ Expr* }
//declarations
%type declatation{ Expr* }
%type varDecl{ Expr* }
%type varList{ Expr* }
//statement
%type compound{ Expr* }
%type statment{ Expr* }
//expression
%type funcExpr{ Expr* }
%type argExprList{ ExprList* }
%type arrayExpr{ Expr* }
%type arrayFiledsList{ ExprList* }
%type arrayFiled{ Expr* }
%type primaryExpr{ Expr* }
%type constExpr{ Expr* }
%type postExpr{ Expr* }
%type unaryExpr{ Expr* }
%type mulExpr{ Expr* }
%type addExpr{ Expr* }
%type shiftExpr{ Expr* }
%type relExpr{ Expr* }
%type eqExpr{ Expr* }
%type inExpr{ Expr* }
%type binExpr{ Expr* }
%type logicalExpr{ Expr* }
%type assignExpr{ Expr* }
%type expr{ Expr* }
%type typeSpec{ TypeSpec }
%type argDeclList{ ExprList* }
%type argDecl { Expr* }

%type rhAssign{ Expr* }

%type arrayDecl{ int }
%type initDecl{ Expr* }
%type arrayDeclList{ int }

%type rightHandAssign{ Expr* }
%type leftHandAssign{ Expr* }
%type memberExpr{ Expr* }
%type callExpr{ Expr* }

//pririty and assiciativity
%left IF.
%left ELSE.
%right ASSIGN.
%left LOR.
%left LAND.
%nonassoc  EQ NEQ.
%nonassoc  GT GTE  LT LTE INSTANCEOF.

%start_symbol program

program::= declOrStmtList(A). 
{ 
	if (A != nullptr && A->is<ExprList>())
		OnProgramEnd( A->as<ExprList>().head());
	else
		OnProgramEnd(nullptr);
} 

declOrStmtList(A)::= declOrStmtList(B) declOrStmt(C).
{
	A = mergeExpr(B,C); 
}
declOrStmtList(A)::= . { A = nullptr; } 

declOrStmt(A)::= declatation(B). { A = B;}
declOrStmt(A)::= statment(B). { A = B;}

/*
 * Declaration 
 ***********************************************/
declatation(A)::= varDecl(B).  {A = B;}
declatation(A)::= funcDecl.    {A = nullptr;}
declatation(A)::= classDecl.   {A = nullptr; }


/*Type specifier*/
typeSpec::= VAR.		{ OnVarDeclarationBegin(TS_VAR); }
typeSpec::= INT_TS.	    { OnVarDeclarationBegin(TS_INT); }
typeSpec::= UINT_TS.	{ OnVarDeclarationBegin(TS_INT); }
typeSpec::= BOOL_TS.	{ OnVarDeclarationBegin(TS_INT); }
typeSpec::= CHAR_TS.	{ OnVarDeclarationBegin(TS_CHAR);  }
typeSpec::= UCHAR_TS.	{ OnVarDeclarationBegin(TS_CHAR);  }
typeSpec::= FLOAT_TS.	{ OnVarDeclarationBegin(TS_FLOAT); }
typeSpec::= STRING_TS.  { OnVarDeclarationBegin(TS_STRING); }


/*
 * Var Declaration
 **********************************************/
arrayDecl(A)::= LBRACKET ICONST(B) RBRACKET. 
{
	//std::cout << "arrayDecl: [ ICONST ]\n" << B.val.Int() << "\n";
	 A = B.val.Int();
}
arrayDecl(A)::= LBRACKET RBRACKET.
{
	//std::cout << "arrayDecl: [  ]\n";
	A = 0;
}

arrayDeclList::= arrayDecl(B) arrayDeclList.
{
	//std::cout << "arrayDeclList: arrayDeclList arrayDecl\n" << B << "\n";
	OnArrayDeclaration(B);
}
arrayDeclList::= arrayDecl(B).
{
	//std::cout << "arrayDeclList:  arrayDecl\n" << B << "\n" ;
	OnArrayDeclaration(B);
}

varDeclBegin::=typeSpec.
{
	//std::cout << "varDeclBegin: typeSpec:\n";
}

varDeclBegin::= typeSpec arrayDeclList.
{
	//std::cout << "varDeclBegin: typeSpec arrayDeclList\n";
}

initDecl(A)::= IDENT(B).
{ 
	A = OnIdentifier(B);
}
initDecl(A)::= IDENT(B) ASSIGN assignExpr(C).
{
	A = OnAssign(OnIdentifier(B), C);
}

//varList(A)::= assignExpr(B).
varList(A)::= initDecl(B).// assignExpr(B)
{
	A = OnVarDeclaration(B);
}

varList(A)::= varList(B) COMMA initDecl(C).
{
	C = OnVarDeclaration(C);
	A = mergeExpr(B, C);
}


varDecl(A)::= varDeclBegin varList(B) SEMI.
{ 
	A = B; 
}


/*
 * Function Declaration
 **********************************/
argDecl(A)::= typeSpec IDENT(B). 
{
	A = OnArgDecl(B);
}
argDecl(A)::= IDENT(B). {
	OnVarDeclarationBegin(TS_VAR);
	A = OnArgDecl(B);
}

argDeclList(A)::= argDeclList(B) COMMA argDecl(C).
{
	//Args are pushed left to right
	B->tail()->setNext(C);
	B->setTail(C);
	A = B;
}
argDeclList(A)::= argDecl(B). {	A = _compiler->newExprList(B, B); }
argDeclList(A)::= .  { A = _compiler->newExprList(nullptr,nullptr); }

funcProto::= FUNCTION IDENT(B) LPAREN argDeclList(C) RPAREN typeSpec.
{
	OnFunctionProto(B,C->head());
}

funcProto::= FUNCTION IDENT(B) LPAREN argDeclList(C) RPAREN.
{
	//just to update the current type
	OnVarDeclarationBegin(TS_VAR);
	OnFunctionProto(B,C->head());
}
funcDecl::= funcProto compound(A). 
{ 
	OnFunctionEnd(A);
}


/*
 * Class Declaration
 ******************************************************/
identList::= IDENT.
identList::= identList COMMA IDENT.
classDecl::= CLASS IDENT LPAREN identList RPAREN compound.
classDecl::= CLASS IDENT compound.


/****************************************************** 
 * Statements
 ******************************************************/

blockBegin::= LBRACE.
{
	OnBlockBegin();
}
compound(A)::= blockBegin declOrStmtList(B) RBRACE.
{ 
	A = OnBlockEnd(B); 
}
/*Expression and compound*/
statment(A)::= SEMI.  {  A = nullptr; }
statment(A)::= compound(B). {A = B;}
statment(A)::= assignExpr(B) SEMI. {  A = OnExprStatement(B); }

/*Branch*/
statment(A)::= IF LPAREN assignExpr(B) RPAREN statment(C). { 
	A = OnIfStatement(B, C);
}
statment(A)::= IF LPAREN assignExpr(B) RPAREN statment(C) ELSE statment(D). {
	A = OnIfStatement(B,C,D);
}
/*Jump expression*/
statment(A)::= BREAK(B) SEMI.  { A = OnBreak(B); }
statment(A)::= CONTINUE(B) SEMI. { A = OnContinue(B); }
statment(A)::= RETURN(B) assignExpr(C) SEMI. {A = OnReturnStatement(C, B.location); }
statment(A)::= RETURN(B) SEMI. {A = OnReturnStatement(nullptr, B.location); }
/*Loops*/
whileBegin::= WHILE(A) LPAREN. { OnWhileLoopBegin(A); }
statment(A)::= whileBegin assignExpr(B) RPAREN statment(C). { A = OnWhileLoop(B, C); }
statment::= DO statment WHILE LPAREN assignExpr RPAREN .
forLoopBegin::= FOR LPAREN. { OnForLoopBegin(); }
//for loop
statment(A)::= forLoopBegin varDecl(B) logicalExpr(C) SEMI assignExpr(D)  RPAREN statment(E). {A = OnForLoop(B, C, D, E); }
statment(A)::= forLoopBegin assignExpr(B) SEMI logicalExpr(C) SEMI assignExpr(D)  RPAREN statment(E). {A = OnForLoop(B, C, D, E); }
//for in statement
statment::= forLoopBegin inExpr RPAREN statment.
statment::= forLoopBegin IDENT COMMA inExpr RPAREN statment.
//try catch throw
statment::= TRY compound CATCH LPAREN IDENT RPAREN compound.
statment::= THROW primaryExpr SEMI.
//const expression
statment(A)::= CONST IDENT(B) ASSIGN logicalExpr(C) SEMI. 
{
	A = OnConstExpr(B, C);
}

/**********************************/
/* Expression                     */
/**********************************/
expr(A)::= assignExpr(B). {A = B;}


//unamed function/lambda: function expression 
//ex: var f = function(){};
funcExprProto::= FUNCTION LPAREN argDeclList RPAREN.
funcExpr::= funcExprProto compound.

//Array initializer
arrayFiled(A)::= assignExpr(B). { A = B; }
arrayExpr(A)::= LBRACKET(B) arrayFiledsList(C) RBRACKET.
{
	A; B; C;
	//A = OnArrayExpr(C->head(), B.location);
}
arrayFiledsList(A)::= arrayFiledsList(B) COMMA arrayFiled(C).
{
	//fields are pushed left to right
	B->tail()->setNext(C);
	B->setTail(C);
	A = B;
}
arrayFiledsList(A)::= arrayFiled(B). {   A = _compiler->newExprList(B, B); }
arrayFiledsList(A)::= .  { A = _compiler->newExprList(nullptr, nullptr); }

//
// Assign expression begins here
////////////////////////////////////////////////
assignExpr(A)::= postExpr(B)  rightHandAssign(C).
{
	A = OnAssign(B, C);
}
//simple assignement
rightHandAssign(A)::= ASSIGN assignExpr(B). {A = B; }
/*composed assignement*/
assignExpr(A)::= postExpr(B) OR_ASSIGN shiftExpr(C).  {A = OnAssign(B, OnBinary(ES_OR, B, C)); }
assignExpr(A)::= postExpr(B) XOR_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_XOR, B, C)); }
assignExpr(A)::= postExpr(B) AND_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_AND, B, C)); }
assignExpr(A)::= postExpr(B) SHL_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_SHL, B, C)); }
assignExpr(A)::= postExpr(B) SHR_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_SHR, B, C)); }
assignExpr(A)::= postExpr(B) ADD_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_ADD, B, C)); }
assignExpr(A)::= postExpr(B) SUB_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_SUB, B, C)); }
assignExpr(A)::= postExpr(B) MUL_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_MUL, B, C)); }
assignExpr(A)::= postExpr(B) DIV_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_DIV, B, C)); }
assignExpr(A)::= postExpr(B) MOD_ASSIGN shiftExpr(C). {A = OnAssign(B, OnBinary(ES_MOD, B, C)); }

assignExpr(A)::= logicalExpr(B).  { A = B; }
assignExpr(A)::= arrayExpr(B). { A = B; }
assignExpr::=funcExpr.

//trinary
assignExpr::= logicalExpr QUEST assignExpr COLON assignExpr.

/*expressions start from logical expression */

logicalExpr(A)::= binExpr(B). {A = B;}
logicalExpr(A)::= logicalExpr(B) LOR binExpr(C). { A = OnBinary(ES_LOR, B, C);}
logicalExpr(A)::= logicalExpr(B) LAND binExpr(C).{ A = OnBinary(ES_LAND, B, C);}

binExpr(A)::= inExpr(B).{A = B;}
binExpr(A)::= eqExpr(B).{A = B; }
binExpr(A)::= binExpr(B)  OR  eqExpr(C). { A = OnBinary(ES_OR, B, C);}
binExpr(A)::= binExpr(B)  XOR eqExpr(C). { A = OnBinary(ES_XOR, B, C);}
binExpr(A)::= binExpr(B)  AND eqExpr(C). { A = OnBinary(ES_AND, B, C);}

inExpr(A)::= inExpr(B) IN eqExpr(C).{ A = OnBinary(ES_IN, B, C);}
//inExpr(A)::= eqExpr(B).{A = B;}

eqExpr(A)::= relExpr(B).{A = B;}
eqExpr(A)::= eqExpr(B) EQ eqExpr(C). { A = OnBinary(ES_EQ, B, C);}
eqExpr(A)::= eqExpr(B) NEQ eqExpr(C).{ A = OnBinary(ES_NEQ, B, C);}

relExpr(A)::= shiftExpr(B). {A = B;}
relExpr(A)::= relExpr(B)  LT  relExpr(C).{ A = OnBinary(ES_LT, B, C);}
relExpr(A)::= relExpr(B)  LTE relExpr(C).{ A = OnBinary(ES_LTE, B, C);}
relExpr(A)::= relExpr(B)  GT  relExpr(C). { A = OnBinary(ES_GT, B, C);}
relExpr(A)::= relExpr(B)  GTE relExpr(C). { A = OnBinary(ES_GTE, B, C);}
//TODO: implement instanceof
relExpr::= relExpr  INSTANCEOF relExpr. {}

shiftExpr(A)::= addExpr(B).{A = B;}
shiftExpr(A)::= shiftExpr(B) SHL addExpr(C). { A = OnBinary(ES_SHL, B, C);}
shiftExpr(A)::= shiftExpr(B) SHR addExpr(C).{ A = OnBinary(ES_SHR, B, C);}

addExpr(A)::= addExpr(B) ADD mulExpr(C). { A = OnBinary(ES_ADD, B, C);}
addExpr(A)::= addExpr(B) SUB mulExpr(C). { A = OnBinary(ES_SUB, B, C);}
addExpr(A)::= mulExpr(B).{A = B;}

mulExpr(A)::=  mulExpr(B) MUL unaryExpr(C). { A = OnBinary(ES_MUL, B, C);}
mulExpr(A)::=  mulExpr(B) DIV unaryExpr(C). { A = OnBinary(ES_DIV, B, C);}
mulExpr(A)::=  mulExpr(B) MOD unaryExpr(C). { A = OnBinary(ES_MOD, B, C);}
mulExpr(A)::=  unaryExpr(B).{A = B;}

unaryExpr(A)::=  postExpr(B).{A = B;}
unaryExpr(A)::=  SUB unaryExpr(B).  { A = OnUnary(ES_USUB,B);}
unaryExpr(A)::=  ADD unaryExpr(B).  { A = OnUnary(ES_UADD,B);}
unaryExpr(A)::=  NOT unaryExpr(B).  { A = OnUnary(ES_NOT,B);}
unaryExpr(A)::=  LNOT unaryExpr(B). { A = OnUnary(ES_LNOT,B);}

unaryExpr(A)::= INC unaryExpr(B). { A = OnUnary(ES_INC, B); }
unaryExpr(A)::= DEC unaryExpr(B). { A = OnUnary(ES_DEC, B); }
unaryExpr::= TYPEOF unaryExpr.

postExpr(A)::= primaryExpr(B).    {	A = B;}
postExpr(A)::= memberExpr(B). {	A = B; }
postExpr(A)::= memberExpr(B) LPAREN argExprList(C) RPAREN. {A = OnCall(B, C->head()); }

postExpr(A)::= postExpr(B) INC. { A = OnUnary(ES_PINC, B); }
postExpr(A)::= postExpr(B) DEC. { A = OnUnary(ES_PDEC, B); }

memberExpr(A)::= IDENT(B). { A = OnIdentifier(B); }
memberExpr::= ATHIS. { /*A = OnPrimary(B); */}
memberExpr::= ABASE. { /*A = OnPrimary(B);*/ }
memberExpr(A)::= memberExpr(B) DOT IDENT(C).  {A = OnDotExpr(B, C); }
memberExpr(A)::= memberExpr(B) LBRACKET addExpr(C) RBRACKET.
{
	A = OnIndex(B, C);
}

argExprList(A)::= argExprList(B) COMMA assignExpr(C) .
{
	//Args are pushed left to right
	A = OnArgList(B, C);
}

argExprList(A)::= assignExpr(B). {	A = OnArg(B);  }
argExprList(A)::= .				 {  A = OnArg(nullptr); }

primaryExpr(A)::= constExpr(B). {A = B; }
primaryExpr(A)::= LPAREN expr(B) RPAREN. {A = B; }

constExpr(A)::= ICONST(B). { A = OnPrimary(B); }
constExpr(A)::= FCONST(B). { A = OnPrimary(B); }
constExpr(A)::= SCONST(B). { A = OnPrimary(B); }
constExpr(A)::= CSCONST(B). { A = OnPrimary(B); }
constExpr(A)::= ATRUE(B).  { A = OnPrimary(B); }
constExpr(A)::= AFALSE(B). { A = OnPrimary(B); }
constExpr(A)::= ANULL(B).  { A = OnPrimary(B); }
