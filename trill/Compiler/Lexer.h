//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Lexer.h
// PURPOSE:
// DATE: 2015/02/10
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common.h"

#define EOI 0
#define LINE_INDEX_BASE 1

namespace trill {

    struct Compiler;

    struct Lexer
    {
    public:
        Lexer(Compiler *a_compiler, char * a_source, unsigned a_bsize = 0)
            : _source(a_source),
              _compiler(a_compiler),
              _bsize(a_bsize)
        {
            _bfree = false;
            Initialize();
        }
        Lexer(const char * a_source, unsigned a_bsize = 0)
            :  _compiler(nullptr),
               _bsize(a_bsize)
        {
            _bfree = true;
            _source = _strdup(a_source);
            Initialize();
        }
        ~Lexer()
        {
            if (_bfree)
                free(_source);
        }

        inline int Next(Token &token)
        {
            token.id = Scan(token);
            token.len = _tokenLen;
            token.str = _tokenStr;
            token.location.lineno = _lineno;
            token.location.column = _tokenStr - _lineStr;
            return token.id;
        }

    private:
        void Initialize();

        int Scan(Token &token);
        int ScanIdentifier(Token &token);
        int ScanString(Token &token);
        int ScanVerbatim(Token &token);
        int ScanInteger(Token &token, int radix);
        int ScanFloat(Token &token);
        int ScanCommentLine();
        int ScanCommentBlock();

        void ReportError(const char *msg);
        void ReportSyntaxError(const char *msg);

        char* CharToString(char c );

        bool        _bfree;
        char        *_source;
        unsigned     _bsize;
        Compiler    *_compiler;

        char* _cursor;  //store re2c current position
        char* _end;     //re2c limit
        char* _marker;  //re2c marker
        int   _lineno;  //line number start at 0
        char* _lineStr; //line start pointer
        char* _tokenStr;//token string
        int   _tokenLen;//token length
        char  _oldChar;

    };

}