//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Semantics.cpp
// PURPOSE:
// DATE: 2015/08/25
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../Types.h"
#include "../internal.h"
#include "Expr.h"
#include "Semantics.h"
#include "Compiler.h"


//binary operators:
// 1 - left, right types must be equal after promotion
// 2 - <<, >>, & , |  must be used with integer type
// 3 - dot, require a type with dot operator
// 4 - [], require a type with [] operator.
//      - if it's an array expression, type must be equal to 'derived type' and index must be integer
// 5 - LOR(logical or), LAND(logical and), require a boolean expression
// 6 - 'in' operator must be used with reference type
// 7 - 'in' left operand (ex: k in Array) 'k' must be compatible with array derived type
// 8 - dereferenced array is a lvalue

void trill::ExprChecker::OnBinray(BinaryExpr& e)
{
    Expr::Accept(e.left(), this);
    Expr::Accept(e.right(), this);

    ExprInfo left_info = popInfo();
    ExprInfo right_info = popInfo();

    Type* etype = nullptr;

    Type* left_type = e.left()->type();
    Type* right_type = e.right()->type();
    bool lvalue = false;

    //special case of in operator
    if (e.subKind() == ES_IN)
    {
        //set the expression type as boolean
        etype = GetBaseType(TS_BOOL);
        if (!right_type->isReference())
        {
            _compiler->ReportError(e.location(), "'in' needs reference type.  got '%s' type",
                                   right_type->name());
        }
        //case of array expression apply rule 4
        else if (right_type->is<ArrayType>())
        {
            Type* derived_Type = right_type->as<ArrayType>().derivedType();
            derived_Type = PromoteType(derived_Type);
            if (!IsCompatibleType(derived_Type, left_type))
            {
                _compiler->ReportError(e.location(), "'in' left operand '%s' incompatible with '%s' type",
                                       left_type->name(),  derived_Type->name());
            }
        }
    }
    else
    {
        //other binary operators
        switch (e.subKind())
        {
        //objects and value operators
        case trill::ES_ADD:
        case trill::ES_SUB:
        case trill::ES_MUL:
        case trill::ES_DIV:
        case trill::ES_MOD:
            //do a type promotion first
            left_type = PromoteType(left_type);
            right_type = PromoteType(right_type);
            etype = left_type;
            if (!IsEqualType(left_type, right_type))
            {
                _compiler->ReportError(e.location(), "operator '%s' Type mismatch",
                                       BinaryExpr::operatorName(e));
                etype = VarType::instance();
            }
            break;
        case trill::ES_SHL:
        case trill::ES_SHR:
        case trill::ES_AND:
        case trill::ES_XOR:
        case trill::ES_OR:
            left_type = PromoteType(left_type);
            right_type = PromoteType(right_type);
            if (!IsEqualType(left_type, right_type))
            {
                _compiler->ReportError(e.location(), "operator '%s' Type mismatch",
                                       BinaryExpr::operatorName(e));
                etype = VarType::instance();
            }
            else
            {
                etype = left_type;
                if (!IsIntType(etype->id()))
                {
                    _compiler->ReportError(e.location(), "operator '%s' requires integer type",
                                           BinaryExpr::operatorName(e));
                }
            }
            break;
        case trill::ES_DOT:
            if (!hasDotOperator(left_type->id()))
            {
                _compiler->ReportError(e.location(), "invalid use of dot operator with '%s' type",
                                       etype->name());
            }
            etype = VarType::instance();
            break;
        case trill::ES_INDEX:
            etype = VarType::instance();
            if (!hasSubscriptOperator(left_type->id()))
            {
                _compiler->ReportError(e.location(), "invalid use of subscript with '%s' type",
                                       etype->name());
            }//dereference the expression if it's an array
            else
            {
                lvalue = true;
                if (left_type->is<ArrayType>())
                {
                    if (!IsIntType(right_type->id()))
                        _compiler->ReportError(e.location(), "array index must be integer type");

                    etype = left_type->as<ArrayType>().derivedType();
                }
                else
                {
                    //check index type must be int or string
                    right_type = PromoteType(right_type);
                    if (!IsIntType(right_type->id()) || right_type->id() != TS_STRING)
                    {
                        _compiler->ReportError(e.location(), "property id must be integer or string");
                    }
                }
            }
            break;
        case trill::ES_LOR:
        case trill::ES_LAND:
            //after promotion etype must be int type if it's bool type
            if (!etype->is<IntType>())
            {
                _compiler->ReportError(e.location(), "invalid use of '%s' with '%s' type",
                                       BinaryExpr::operatorName(e), etype->name());
            }
            break;
        case trill::ES_LT:
        case trill::ES_LTE:
        case trill::ES_GT:
        case trill::ES_GTE:
        case trill::ES_EQ:
        case trill::ES_NEQ:
            left_type = PromoteType(left_type);
            right_type = PromoteType(right_type);
            //set expression type to bool type
            etype = GetBaseType(TS_BOOL);
            if (!IsEqualType(left_type, right_type))
            {
                _compiler->ReportError(e.location(), "operator '%s' Type mismatch",
                                       BinaryExpr::operatorName(e));
            }
            break;
        default:
            INTERNAL_ERROR("Unknown Binray expression");
        }

        //}
    }

    ExprInfo info;
    info.setlvalue(lvalue);
    info.setSideEffect(left_info.hasSideEffect() || right_info.hasSideEffect());
    pushInfo(info);

    e.setType(etype);

}
// Assign: '='
// 1 - left operand must be l-value
// 2 - right operand type must be compatible with left operand type
// 3 - the expression it's not l-value
// 4 - side-effect: yes
// 5 - expression type is the left type
// 6 - if left is array, right size must be less or equal to right size
void trill::ExprChecker::OnAssign(AssignExpr& e)
{
    //'=' :
    Expr::Accept(e.right(), this);
    Expr::Accept(e.left(), this);

    Type* lefttype = e.left()->type();
    Type* righttype = e.right()->type();

    ExprInfo left_info = popInfo();
    ExprInfo right_info = popInfo();

    //l-value check
    if (!left_info.islvalue())
    {
        _compiler->ReportError(e.location(), "'=' left operand must be l-value");
    }

    if (!IsCompatibleType(righttype, lefttype))
    {
        _compiler->ReportError(e.location(), "'=' type mismatch, cannot convert '%s' to '%s'",
                               righttype->name(), lefttype->name());
        //TODO:
        //insert a cast expression to the left type
    }
    else
    {
        if (lefttype->is<ArrayType>())
        {
            ASSERT(righttype->is< ArrayType > ());
            if (righttype->as<ArrayType>().length() > lefttype->as<ArrayType>().length())
            {
                _compiler->ReportError(e.location(), "right array size is too big '%d' or less expected",
                                       righttype->as<ArrayType>().length());
            }
        }
    }


    ExprInfo info;
    info.setlvalue(false);
    info.setSideEffect(true);
    pushInfo(info);

    e.setType(lefttype);
}

void trill::ExprChecker::OnArray(ArrayExpr& e)
{
    NOT_IMPLEMENTED();
    //TODO: match every field type to field type
    ExprList::Accept(e.fields(), this);

    ExprInfo info;
    info.setlvalue(true);
    info.setSideEffect(false);
    pushInfo(info);
}

void trill::ExprChecker::OnSymbol(SymbolExpr& e)
{
    SourceSymbol * sym = (SourceSymbol *)e.symbol();

    ASSERT(sym != nullptr);

    ExprInfo info;
    info.setlvalue(true);
    info.setSideEffect(false);

    //check symbol definition
    if ( sym->scope() == nullptr )
    {
        _compiler->ReportError(e.location(), "undeclared identifier '%s'", sym->name());
        //just to keep other function from not crashing
        e.setType(VarType::instance());
    }
    else
    {
        ASSERT(sym->type() != nullptr);
        e.setType(sym->type());
        info.setlvalue(sym->kind() != SK_CONST);
    }

    pushInfo(info);

}

void trill::ExprChecker::OnCall(CallExpr& e)
{
    //make sure the function symbol have a type
    Expr::Accept(e.function(), this);

    ASSERT(e.function()->type());
    ASSERT(e.function()->is<SymbolExpr>());

    //save stack size needed to check the call against function arguments
    unsigned size = _exprInfoStack.size();

    //check every argument
    ExprList::Accept(e.args(), this);
    Symbol* sym = e.function()->as<SymbolExpr>().symbol();
    //check number of parameters if it's script function
    if (sym->is<FunctionSymbol>())
    {
        FunctionSymbol& fnsym = sym->as<FunctionSymbol>();
        unsigned ncalls_args = _exprInfoStack.size() - size;

        if (ncalls_args != fnsym.nargs())
        {
            const char* msg = ncalls_args < fnsym.nargs() ?  "few arguments" : "too many arguments";
            _compiler->ReportError(e.location(), "%s to call '%s'", msg, fnsym.name());
            Location l;
            l.column = fnsym.column();
            l.lineno = fnsym.line();
            //TODO: this may change if declared in another file
            _compiler->ReportError(l, "see declaration of '%s'", fnsym.name());

        }
        //equal arguments check types
        else
        {
            Expr* arg = e.args();
            TypeList* proto = fnsym.prototype();

            for (unsigned i = 0; i < ncalls_args; i++)
            {
                Type* argType = arg->type();
                arg = arg->next();
                Type* paramType = proto->type();
                proto = proto->next();

                argType = PromoteType(argType);
                paramType = PromoteType(paramType);
                if (!IsCompatibleType(argType, paramType))
                {
                    Location l;
                    l.column = fnsym.column();
                    l.lineno = fnsym.line();

                    _compiler->ReportError(e.location(), "argument %d type '%s' incompatible with '%s'", i,
                                           argType->name(), paramType->name());
                    _compiler->ReportError(l, "see declaration of '%s'", fnsym.name());
                }
            }
        }
    }
    else
    {
        //here must be a native function
        ASSERT(sym->is<NativeSymbol>());
        //check if the number of argument is more or equal to the native function expected parameters
        unsigned ncalls_args = _exprInfoStack.size() - size;
        NativeSymbol& fnsym = sym->as<NativeSymbol>();
        if (ncalls_args < fnsym.nargs())
        {
            _compiler->ReportError(e.location(), "few argument to call native function '%s', at least '%d' expected",
                                   fnsym.name(),
                                   fnsym.nargs());
        }
    }

    //clean up the stack
    while (_exprInfoStack.size() > size)
        _exprInfoStack.pop_back();

    //pop function info
    _exprInfoStack.pop_back();
    //set expression type to the function return type
    e.setType(e.function()->type());

    ExprInfo info;
    info.setlvalue(false);
    info.setSideEffect(true);
    pushInfo(info);

}

void trill::ExprChecker::OnArg(ArgExpr& e)
{
    Expr::Accept(e.arg(), this);
    e.setType(e.arg()->type());
}

//return:
// if a function return type is value type, it must return a value
// type: must match the current function return type

void trill::ExprChecker::OnReturn(ReturnExpr& e)
{
    Type* returnType = _compiler->currentFunction()->type();
    ASSERT(returnType != nullptr);
    //is value type
    if (!returnType->isReference() && e.expr() == nullptr)
    {
        _compiler->ReportError(e.location(), "function '%s' must return a value", _compiler->currentFunction()->name());
    }
    else if (e.expr() != nullptr)
    {
        Expr::Accept(e.expr(), this);
        ASSERT(e.expr()->type());

        Type* etype = e.expr()->type();
        ExprInfo  eInfo = popInfo();

        //match it to current function type
        if (!IsCompatibleType(etype, returnType))
        {
            _compiler->ReportError(e.location(), "'return' type mismatch, '%s' function returning '%s'",
                                   returnType->name(), etype->name());
        }
        //TODO: insert cast here or alert and provide a cast function
    }
    //put something in the stack
    ExprInfo info;
    info.setlvalue(false);
    info.setSideEffect(false);
    pushInfo(info);

}


// value: constant value
// l-value = false
// side-effect false
// type is the same type as the constant type, must be set during parsing
void trill::ExprChecker::OnValue(ValueExpr& e)
{
    ExprInfo info;
    info.setlvalue(false);
    info.setSideEffect(false);
    pushInfo(info);

    ASSERT(e.type() != nullptr);

}

// unary operators:
//  1 - inc/dec/post inc/post dec, require l-value in target expression
//  2 - neg,+,not,!logical not, no l-value required in target expression
//  3 - the expression itself is not l-value
//  4 - expression type is the same as the target expression
//  5 - side effect: yes
//  6 - ~ not must be used with int type after promotion
void trill::ExprChecker::OnUnary(UnaryExpr& e)
{
    //generate expression info
    Expr::Accept(e.expr(), this);

    ExprInfo  eInfo = popInfo();
    Type* etype = e.expr()->type();

    etype = PromoteType(etype);
    e.setType(etype);

    if (!eInfo.islvalue() )
    {
        char *name = nullptr;
        switch (e.subKind())
        {
        case ES_INC:
        case ES_PINC:
            name = "++";
            break;
        case ES_PDEC:
        case ES_DEC:
            name = "--";
            break;
        default:
            break;
        }
        if (name)
            _compiler->ReportError(e.location(), "'%s' needs l-value", name);
    }
    if (e.subKind() == ES_NOT && !IsIntType(etype->id()))
    {
        _compiler->ReportError(e.location(), "'~' requires integer type");
    }

    ExprInfo info;
    info.setlvalue(false);
    info.setSideEffect(true);
    pushInfo(info);




}

void trill::ExprChecker::OnJTrue(JTrueExpr& e)
{
    e.setType(GetBaseType(TS_BOOL));
}
/*
//do type check and update expression type`
void trill::ExprChecker::checkExprType(Expr* expr)
{
    //_bdoTypeCheck = true;
    Expr::Accept(expr, this);
}

//fill ExprInfo
void trill::ExprChecker::getExprInfo(Expr* expr, ExprInfo& info)
{
    ASSERT(expr != nullptr);
    _exprInfoStack.clear();
    _exprInfoStack.reserve(64);
   // _bdoTypeCheck = false;

    Expr::Accept(expr, this);

    ASSERT(_exprInfoStack.size() == 1);
    info = popInfo();

}
*/
void trill::ExprChecker::checkExpression(Expr* expr, ExprInfo& info)
{
    ASSERT(expr != nullptr);

    _exprInfoStack.clear();
    _exprInfoStack.reserve(64);

    Expr::Accept(expr, this);
    ASSERT(_exprInfoStack.size() == 1);
    info = popInfo();
}

trill::ExprChecker::ExprChecker(Compiler* compiler)
    : _compiler(compiler)
{
}
