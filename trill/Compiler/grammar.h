#ifndef _GRAMMAR_Y_H
#define _GRAMMAR_Y_H
#define IF                               1
#define ELSE                             2
#define ASSIGN                           3
#define LOR                              4
#define LAND                             5
#define EQ                               6
#define NEQ                              7
#define GT                               8
#define GTE                              9
#define LT                              10
#define LTE                             11
#define INSTANCEOF                      12
#define VAR                             13
#define INT_TS                          14
#define UINT_TS                         15
#define BOOL_TS                         16
#define CHAR_TS                         17
#define UCHAR_TS                        18
#define FLOAT_TS                        19
#define STRING_TS                       20
#define LBRACKET                        21
#define ICONST                          22
#define RBRACKET                        23
#define IDENT                           24
#define COMMA                           25
#define SEMI                            26
#define FUNCTION                        27
#define LPAREN                          28
#define RPAREN                          29
#define CLASS                           30
#define LBRACE                          31
#define RBRACE                          32
#define BREAK                           33
#define CONTINUE                        34
#define RETURN                          35
#define WHILE                           36
#define DO                              37
#define FOR                             38
#define TRY                             39
#define CATCH                           40
#define THROW                           41
#define CONST                           42
#define OR_ASSIGN                       43
#define XOR_ASSIGN                      44
#define AND_ASSIGN                      45
#define SHL_ASSIGN                      46
#define SHR_ASSIGN                      47
#define ADD_ASSIGN                      48
#define SUB_ASSIGN                      49
#define MUL_ASSIGN                      50
#define DIV_ASSIGN                      51
#define MOD_ASSIGN                      52
#define QUEST                           53
#define COLON                           54
#define OR                              55
#define XOR                             56
#define AND                             57
#define IN                              58
#define SHL                             59
#define SHR                             60
#define ADD                             61
#define SUB                             62
#define MUL                             63
#define DIV                             64
#define MOD                             65
#define NOT                             66
#define LNOT                            67
#define INC                             68
#define DEC                             69
#define TYPEOF                          70
#define ATHIS                           71
#define ABASE                           72
#define DOT                             73
#define FCONST                          74
#define SCONST                          75
#define CSCONST                         76
#define ATRUE                           77
#define AFALSE                          78
#define ANULL                           79
#endif
