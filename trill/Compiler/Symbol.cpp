//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Symbol.cpp
// PURPOSE:
// DATE: 2015/11/27
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Symbol.h"
#include "Scope.h"


trill::Symbol* trill::LocalScope::findSymbol(char* name)
{
    for (unsigned i = 0; i < _symbols.size(); i++)
    {
        Symbol *sym = _symbols[i];
        if (sym->name() == name)
            return sym;
    }
    return nullptr;
}

trill::Symbol* trill::GlobalScope::findSymbol(char* name)
{
    if (_symbols.find(name) != _symbols.end())
    {
        return _symbols[name];
    }
    return nullptr;
}

void trill::GlobalScope::addSymbol(trill::Symbol *sym)
{
    _symbols[sym->name()] = sym;
}
