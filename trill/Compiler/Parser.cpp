//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Parser.cpp
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Symbol.h"
#include "Expr.h"
#include "Lexer.h"
#include "grammar.h"
#include "../internal.h"
#include "../Types.h"
#include "Parser.h"

#include "Semantics.h"
#include "Compiler.h"

using namespace trill;

trill::Parser::Parser(Compiler* compiler)
    : _compiler(compiler)
{

    InitLemon();
    _breakLabels.reserve(64);
    _continueLabels.reserve(64);
    _nforLoopScope = 0;
    //runtime is used for type creation
    // _runtime = _compiler->getContext()->getRuntime();
}

trill::Parser::~Parser()
{
    FreeLemon();
}
//ParseSource: Parser loop.
//////////////////////////////////////////////////////////////////////////
void trill::Parser::ParseSource(char *src, unsigned bsize /* = 0*/)
{

    Lexer lex(_compiler, src, bsize);
    Token tk;
    do
    {
        int id = lex.Next(tk);
        Parse(id, tk);
    }
    while (tk.id != EOI);

}

//////////////////////////////////////////////////////////////////////////
//   Parsing callbacks
//////////////////////////////////////////////////////////////////////////

Expr* trill::Parser::OnPrimary(Token &tk)
{
    ValueExprSubKind subkind;
    TypeSpec ts = TS_VAR;

    switch (tk.id)
    {
    case ANULL:
        subkind = ES_NULL;
        tk.val.setInt(0);
        break;
    case ATHIS:
        subkind = ES_THIS;
        break;
    case ABASE:
        subkind = ES_BASE;
        break;
    case ATRUE:
        ts = TS_BOOL;
        subkind = ES_TRUE;
        tk.val.setInt(1);
        break;
    case AFALSE:
        ts = TS_BOOL;
        subkind = ES_FALSE;
        tk.val.setInt(0);
        break;
    case ICONST:
        ts = TS_INT;
        subkind = ES_INT;
        break;
    case FCONST:
        ts = TS_FLOAT;
        subkind = ES_FLOAT;
        break;
    case SCONST:
        ts = TS_VAR;
        subkind = ES_STRING;
        break;
    case CSCONST:
        //this is hardcoded, for sizeof int == 4, we try to gess if we can convert a vebratim constant
        // to char or integer so we can have something like char c = 'a';
        //if len = 1 -> char, 2> len <5 -> int  , 0 or > 4 string
        if (tk.val.len() == 1)
        {
            //try to convert to a char
            char c = *tk.val.str();
            tk.val.setInt(c);
            ts = TS_CHAR;
            subkind = ES_INT;
        }
        else if (tk.val.len() > 1 && tk.val.len() < 5)
        {
            int i = 0, l = tk.val.len();
            char *c = tk.val.str();
            while (l)
            {
                i = (i << 8) | *c;
                c++;
                l--;
            }
            tk.val.setInt(i);
            ts = TS_INT;
            subkind = ES_INT;
        }
        else
        {
            ts = TS_STRING;
            subkind = ES_STRING;
        }
        break;
    default:
        INTERNAL_ERROR("Shouldn't be here");
        break;
    }
    //create a valueExpression and set the base type
    Expr * expr = _compiler->newValueExpr(subkind, tk.val, tk.location);
    expr->setType(GetBaseType(ts));
    return expr;

}

Expr* trill::Parser::OnIdentifier(Token &tk)
{
    Symbol* sym = _compiler->findSymbol(tk.val.str());
    if (sym == nullptr)
    {
        sym = _compiler->newVariableSymbol(tk.val.str(), nullptr, tk.location);
    }
    return _compiler->newSymbolExpr(sym, tk.location) ;
}

Expr* trill::Parser::OnDotExpr(Expr *expr, Token& ident)
{
    VariableSymbol* sym = _compiler->newVariableSymbol(ident.val.str(), nullptr, ident.location);
    sym->setStorage(SS_MEMBER);
    SymbolExpr* right = _compiler->newSymbolExpr(sym, ident.location);
    return _compiler->newBinaryExpr (ES_DOT, expr, right, ident.location);

}

Expr* trill::Parser::OnCall(Expr *fn, Expr* args)
{
    CallExpr* e = _compiler->newCallExpr(fn, args, fn->location());
    return e;
}

ExprList* trill::Parser::OnArg(Expr* arg)
{
    if (arg)
    {
        arg = _compiler->newArgExpr(arg);
    }
    return _compiler->newExprList(arg, arg);
}

ExprList* trill::Parser::OnArgList(ExprList* argList, Expr* arg)
{
    //Arguments are pushed left to right
    if (arg)
        arg = _compiler->newArgExpr(arg);

    argList->tail()->setNext(arg);
    argList->setTail(arg);
    return argList;
}

Expr* trill::Parser::OnIndex(Expr *arry, Expr *index)
{
    return _compiler->newBinaryExpr(ES_INDEX, arry, index, arry->location());
}

Expr* trill::Parser::OnUnary(UnaryExprSubKind subKind, Expr *expr)
{
    return _compiler->newUnaryExpr(subKind, expr, expr->location());
}

Expr* trill::Parser::OnBinary(BinExprSubKind subKind, Expr *left, Expr* right)
{
    //TODO: fold constant
    Expr* retVal = _compiler->newBinaryExpr(subKind, left, right, left->location());
    if (retVal)
        _compiler->checkExpression(retVal);

    return retVal;
}

Expr* trill::Parser::OnAssign(Expr *left, Expr* right)
{
    return _compiler->newAssignExpr(left, right, left->location());
}
//merge 2 expression and return expression list
Expr* trill::Parser::mergeExpr(Expr *first, Expr* last)
{

    if (last != nullptr)
    {
        if (first == nullptr)
        {
            if (last->is<ExprList>())
                return  last;
            return  _compiler->newExprList (last, last);
        }
        else if (!first->is<ExprList>())
        {
            //here make a new expression list or use last if it's a list
            if (last->is<ExprList>())
            {
                //update head/tail
                first->setNext(last->as<ExprList>().head());
                last->as<ExprList>().setHead(first);
                return last;
            }
            return  _compiler->newExprList(first, last);
        }
        else
        {
            ASSERT(first->is<ExprList>());
            if (last->is<ExprList>())
            {
                //both are list connect first tail to last head and update first tail
                first->as<ExprList>().tail()->setNext(last->as<ExprList>().head());
                first->as<ExprList>().setTail(last->as<ExprList>().tail());
            }
            else
            {

                first->as<ExprList>().tail()->setNext(last);
                first->as<ExprList>().setTail(last);
            }
        }
    }
    return first;

}

Expr* trill::Parser::OnArrayExpr(Expr *fileds, Location &location)
{
    return _compiler->newArrayExpr(fileds, location);
}

void trill::Parser::OnVarDeclarationBegin(TypeSpec type)
{
    switch (type)
    {
    case trill::TS_CHAR:
    case trill::TS_INT:
    case trill::TS_FLOAT:
    case trill::TS_STRING:
    case trill::TS_VAR:
        _currentType = GetBaseType(type);
        break;
    default:
        NOT_IMPLEMENTED();
    }
}

//handle single declaration
Expr* trill::Parser::OnVarDeclaration(Expr *var)
{
    ASSERT(var != nullptr);

    Expr *retVal = nullptr;
    Expr *ident = var;
    // Expr* right = nullptr;

    if (var->is<AssignExpr>())
    {
        // right = var->as<AssignExpr>().right();
        ident = var->as<AssignExpr>().left();
        retVal = var;
    }

    //check if it's an identifier
    if (ident->is<SymbolExpr>())
    {
        //declare symbol here
        _compiler->declareSymbol(ident->as<SymbolExpr>(), _currentType);
    }
    else
    {
        _compiler->ReportError(var->location(), "Identifier expected");
    }

    if (retVal)
        _compiler->checkExpression(retVal);

    return retVal;
}

void trill::Parser::OnProgramEnd(Expr *compound)
{
    _compiler->onParsingFinished(compound);
}

Expr* trill::Parser::OnExprStatement(Expr *expr)
{
    ExprInfo info;
    _compiler->checkExpression(expr, info);

    //don't generate an expression with no side-effect
    if (!info.hasSideEffect())
    {
        return nullptr;
    }
    return _compiler->foldExpression(expr);
}

Expr* trill::Parser::OnIfStatement(Expr* cond, Expr* trueBlock)
{
    //TODO: check for constant condition, dead code elemenation

    Expr * trueLabel = _compiler->newLabel("iftrue");
    Expr * endIfLabel = _compiler->newLabel("endif");

    //empty true block?
    if (trueBlock)
        trueLabel->setLocation(trueBlock->location());

    Expr * cjump = makeCJump(cond, trueLabel, endIfLabel);

    Expr* list = mergeExpr(nullptr, cjump);
    mergeExpr(list, trueLabel);
    mergeExpr(list, trueBlock);
    mergeExpr(list, endIfLabel);
    return list;
}

Expr* trill::Parser::OnIfStatement(Expr* cond, Expr* trueBlock, Expr* falseBlock)
{

    Expr * trueLabel = _compiler->newLabel("iftrue");
    Expr * elseLabel = _compiler->newLabel("else");
    Expr * endIfLabel = _compiler->newLabel("endif");

    if (trueBlock)
        trueLabel->setLocation(trueBlock->location());

    if (falseBlock)
        elseLabel->setLocation(falseBlock->location());


    Expr * cjump = makeCJump(cond, trueLabel, elseLabel); // MakeExpr(E_CJUMP,cond->location,cond,elseLabel);

    cjump->setLocation(cond->location());
    Expr * jump =  _compiler->newJumpExpr(endIfLabel);

    Expr* list = mergeExpr(nullptr, cjump);
    mergeExpr(list, trueLabel);
    mergeExpr(list, trueBlock);
    mergeExpr(list, jump);
    mergeExpr(list, elseLabel);
    mergeExpr(list, falseBlock);
    mergeExpr(list, endIfLabel);
    return list;

}

Expr* trill::Parser::makeCJump(Expr *cond, Expr *trueLabel, Expr* falseLabel)
{
    Expr *retVal = nullptr;
    Expr *label = nullptr;
    //TODO: make cond relational
    if (cond->is<BinaryExpr>())
    {
        BinaryExpr& binCond = cond->as<BinaryExpr>();
        switch (binCond.subKind())
        {
        case ES_LAND:
        {
            label = _compiler->newLabel("and");
            retVal = mergeExpr(nullptr, makeCJump(binCond.left(), label, falseLabel));
            mergeExpr(retVal, label);
            mergeExpr(retVal, makeCJump(binCond.right(), trueLabel, falseLabel));
        }
        break;
        case ES_LOR:
        {
            label = _compiler->newLabel("or");
            retVal = mergeExpr(nullptr, makeCJump(binCond.left(), trueLabel, label));
            mergeExpr(retVal, label);
            mergeExpr(retVal, makeCJump(binCond.right(), trueLabel, falseLabel));
        }
        break;
        default:
            retVal = mergeExpr(nullptr, makeJTrue(cond, trueLabel));
            mergeExpr(retVal, _compiler->newJumpExpr(falseLabel));
            // retVal = MergeExpr(NULL,MakeJTrue(cond,falseLabel));
        }
    }
    else if(cond->is<SymbolExpr>())
    {
        // case of if(symbol) -> if(symbol!=0)
        Value v;
        v.setInt(0);
        JTrueExpr * jtrueExpr = _compiler->newJTrueExpr(ES_NEQ, trueLabel);
        jtrueExpr->setLeft(cond);
        jtrueExpr->setRight(_compiler->newValueExpr(ES_INT, v, cond->location()));
        jtrueExpr->setLocation(cond->location());
        retVal = mergeExpr(nullptr, jtrueExpr);
        mergeExpr(retVal, _compiler->newJumpExpr(falseLabel));
    }
    else
    {
        NOT_IMPLEMENTED();
    }

    return retVal;

}

Expr* trill::Parser::makeJTrue(Expr *cond, Expr *label)
{
    JTrueExpr * retVal = nullptr;
    //TODO: make sure it's a relational operation
    if (cond->is<BinaryExpr>())
    {
        BinaryExpr& bincond = cond->as<BinaryExpr>();
        switch (bincond.subKind())
        {
        case ES_LT:
        case ES_LTE:
        case ES_GT:
        case ES_GTE:
        case ES_EQ:
        case ES_NEQ:

            retVal = _compiler->newJTrueExpr(bincond.subKind(), label);
            retVal->setLeft(bincond.left());
            retVal->setRight(bincond.right());
            break;
        default:
            NOT_IMPLEMENTED();
        }
    }
    else
    {
        NOT_IMPLEMENTED();
    }

    return retVal;
}

void trill::Parser::OnBlockBegin()
{
    //if we are not in for loop scope enter a new scope
    if (_nforLoopScope <= 0)
        _compiler->enterScope();
    //reset the flag
    if (_nforLoopScope > 0)
        --_nforLoopScope;
}

Expr* trill::Parser::OnBlockEnd(Expr *list)
{
    _compiler->leaveScope();
    return list;
}

void trill::Parser::OnFunctionProto(Token &ident, Expr* args)
{
    FunctionSymbol *fn = _compiler->declareFunction(ident.val.str(), _currentType, ident.location);
    if (fn)
    {
        //build the prototype
        TypeList *proto = nullptr, *head = nullptr;
        //enter function scope here
        _compiler->pushFunction(fn);
        _compiler->enterScope();
        //loop over args and declare them
        int nargs = 0;
        for (Expr* argExpr = args; argExpr != nullptr; argExpr = argExpr->next(), nargs++)
        {
            ASSERT(argExpr->is<SymbolExpr>());
            SymbolExpr& symExpr = argExpr->as<SymbolExpr>();

            //declare formal
            _compiler->declareSymbol(symExpr, argExpr->type());
            //add it to function locals
            ASSERT(symExpr.symbol()->is<VariableSymbol>());
            VariableSymbol* arg = (VariableSymbol*)symExpr.symbol();
            //add type to prototype list
            if (!proto)
            {
                proto = _compiler->newTypeList(argExpr->type());
                head = proto;
            }
            else
            {
                head->setNext(_compiler->newTypeList(argExpr->type()));
                head = head->next();
            }
            //TODO: we may define 'this' symbol here
            unsigned index = fn->addLocal(arg);
            arg->setIndex(index);

        }
        fn->setPrototype(proto);
        fn->setnArgs(nargs);
    }

}

Expr* trill::Parser::OnArgDecl(Token& ident)
{
    VariableSymbol *arg = _compiler->newVariableSymbol(ident.val.str(), _currentType, ident.location);
    SymbolExpr *expr = _compiler->newSymbolExpr(arg, ident.location);
    //expression type same as symbol type
    expr->setType(_currentType);
    return expr;
}

void trill::Parser::OnFunctionEnd(Expr* body)
{
    FunctionSymbol *fn = _compiler->popFunction();

    if (body && body->is<ExprList>())
        body = body->as<ExprList>().head();
    //set body and leave function scope
    fn->setBody(body);
    _compiler->leaveScope();
}

Expr* trill::Parser::OnReturnStatement(Expr* expr, Location& loc)
{
    Expr *retExpr = _compiler->newReturnExpr(expr, loc);
    _compiler->checkExpression(retExpr);
    return retExpr;
}

Expr* trill::Parser::OnBreak(Token& tk)
{
    //check if we are inside a loop
    if (_breakLabels.size() == 0)
    {
        _compiler->ReportError(tk.location, "illegal break outside of a loop");
        return nullptr;
    }
    //get break label from the top of the stack
    LabelExpr *label_expr = _breakLabels.back();
    Expr * jump = _compiler->newJumpExpr(label_expr);
    jump->setLocation(tk.location);
    return jump;
}

Expr* trill::Parser::OnContinue(Token& tk)
{
    if (_continueLabels.size() == 0 )
    {
        _compiler->ReportError(tk.location, "illegal continue outside of a loop");
        return nullptr;
    }
    //get continue label from the top of the stack
    LabelExpr *label_expr = _continueLabels.back();
    Expr * jump = _compiler->newJumpExpr(label_expr);
    jump->setLocation(tk.location);
    return jump;
}

void trill::Parser::OnWhileLoopBegin(Token& tk)
{
    //create the continue/break labels here and push them in the stack
    LabelExpr*  continue_expr = _compiler->newLabel("while_continue");
    LabelExpr* break_expr = _compiler->newLabel("while_break");

    _breakLabels.push_back(break_expr);
    _continueLabels.push_back(continue_expr);
    //set the location as well
    continue_expr->setLocation(tk.location);

}

Expr* trill::Parser::OnWhileLoop(Expr* cond, Expr* block)
{
    LabelExpr*  continue_label = _continueLabels.back();
    LabelExpr* break_label = _breakLabels.back();

    //remove labels from the stack
    _breakLabels.pop_back();
    _continueLabels.pop_back();

    //while block begin label
    Expr * trueLabel = _compiler->newLabel("while_true");
    //generate the condition
    Expr * cjump = makeCJump(cond, trueLabel, break_label);
    //end while jump
    Expr * jump  = _compiler->newJumpExpr(continue_label);
    //continue_label:
    //    ... condition
    //    ... jtrue while_true
    //    ... jump while_break
    //while_true:
    //    ....block
    //    ....jump continue_expr
    //while_break:

    //create and return a list
    Expr* list = mergeExpr(nullptr, continue_label);
    mergeExpr(list, cjump);
    mergeExpr(list, trueLabel);
    mergeExpr(list, block);
    mergeExpr(list, jump);
    mergeExpr(list, break_label);
    return list;//list->as<ExprList>().head();

}

void trill::Parser::OnForLoopBegin()
{
    //create the continue/break labels
    LabelExpr*  continue_expr = _compiler->newLabel("for_continue");
    LabelExpr* break_expr = _compiler->newLabel("for_break");
    _breakLabels.push_back(break_expr);
    _continueLabels.push_back(continue_expr);
    //enter if we are not in for loop scope
    if (_nforLoopScope == 0)
    {
        _compiler->enterScope();
        ++_nforLoopScope;
    }
}

Expr* trill::Parser::OnForLoop(Expr* initBlock, Expr* condition, Expr* continue_block, Expr* block)
{
    //    ...  initBlock
    //condition_label:
    //    ... condition
    //    ... jtrue for_true
    //    ... jump for_break
    //for_true:
    //    ... block
    //continue_label:
    //    ... continue_block
    //    ... jump condition_label
    //for_break:

    //get and remove break,continue labels from the stack
    LabelExpr*  continue_label = _continueLabels.back();
    LabelExpr* break_label = _breakLabels.back();
    _breakLabels.pop_back();
    _continueLabels.pop_back();

    //for_true block begin label
    Expr * trueLabel = _compiler->newLabel("for_true");
    //generate the condition
    Expr * cjump = makeCJump(condition, trueLabel, break_label);
    //end for loop jump
    Expr * condition_label = _compiler->newLabel("for_test");
    Expr * jump = _compiler->newJumpExpr(condition_label);

    //build statements list
    Expr* list = mergeExpr(nullptr, initBlock);
    list = mergeExpr(list, condition_label);
    mergeExpr(list, cjump);
    mergeExpr(list, trueLabel);
    mergeExpr(list, block);
    mergeExpr(list, continue_label);
    mergeExpr(list, continue_block);
    mergeExpr(list, jump);
    mergeExpr(list, break_label);
    //in case we didn't enter a compound block
    if (_nforLoopScope > 0)
    {
        _compiler->leaveScope();
        --_nforLoopScope;
    }
    return list;
}

void trill::Parser::OnArrayDeclaration(int n)
{
    _currentType = _compiler->newArrayType(_currentType, n);
}

Expr* trill::Parser::OnConstExpr(Token& ident, Expr* val)
{
    _compiler->checkExpression(val);
    val = _compiler->foldExpression(val);
    if (!val->is<ValueExpr>())
        _compiler->ReportError(val->location(), "Constant expression expected");
    else
    {
        _compiler->declareConst(ident.val.str(), (ValueExpr*) val, ident.location);
    }
    return nullptr;
}

