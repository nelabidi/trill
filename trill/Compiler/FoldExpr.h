//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  FoldExpr.h
// PURPOSE:
// DATE: 2015/12/02
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////
///
#pragma once
namespace trill {

    struct FoldExpr : ExprSimpleVisitor
    {
        FoldExpr(Compiler* compiler);
        ~FoldExpr();

        Expr* Fold(Expr* e);

        virtual void Visit(Expr * e);

    private:
        Compiler* _compiler;
        std::vector<Expr*> _stack;

        Expr *Fold(BinExprSubKind subKind, Expr *left, Expr* right);
        bool FoldValue(BinExprSubKind op, int v1, int v2, int &retVal);
        bool FoldValue(BinExprSubKind op, float v1, float v2, float &retVal);
    };
}


