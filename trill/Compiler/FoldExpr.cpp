//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  FoldExpr.cpp
// PURPOSE:
// DATE: 2015/12/02
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../internal.h"
#include "Expr.h"
#include "Compiler.h"
#include "FoldExpr.h"

using namespace trill;

trill::FoldExpr::FoldExpr(Compiler* compiler)
    : _compiler(compiler)
{

}

trill::FoldExpr::~FoldExpr()
{

}

void trill::FoldExpr::Visit(Expr * e)
{
    switch (e->kind())
    {

    case trill::E_BINARY:
    {
        Expr::Accept(e->as<BinaryExpr>().left(), this);
        Expr::Accept(e->as<BinaryExpr>().right(), this);
        if(_stack.size() >= 2 )
        {
            Expr*  er = _stack.back();
            _stack.pop_back();
            Expr*  el = _stack.back();
            _stack.pop_back();
            e->as<BinaryExpr>().setLeft(el);
            e->as<BinaryExpr>().setRight(er);

            Expr*  ex = Fold(e->as<BinaryExpr>().subKind(), el, er);
            ex = ex != nullptr ? ex : e;
            _stack.push_back(ex);
        }
    }
    break;
    case trill::E_UNARY:
    {
        _stack.push_back(e);
        /*Expr::Accept(e->as<UnaryExpr>().expr(), this);
        if (_stack.size() >= 1)
        {
            //TODO: unary expression
            /*Expr* ex = _stack.back();
            _stack.pop_back();*/
        /*}*/
    }
    break;
    case trill::E_VALUE:
    {
        _stack.push_back(e);
    }
    break;
    case trill::E_SYMBOL:
    {
        Expr * ex = e;
        if (e->as<SymbolExpr>().symbol()->kind() == SK_CONST)
            ex = e->as<SymbolExpr>().symbol()->as<ConstSymbol>().value();

        _stack.push_back(ex);
    }
    break;

    case trill::E_CALL:
    {
        //walk arguments
        Expr::Accept(e->as<CallExpr>().args(), this);
        _stack.push_back(e);
    }
    break;
    case trill::E_ARG:
    {
        Expr::Accept(e->as<ArgExpr>().arg(), this);
        Expr*  arg = _stack.back();
        _stack.pop_back();
        e->as<ArgExpr>().setArg(arg);
    }
    break;
    case trill::E_ARRAY:
    case trill::E_LABEL:
    case trill::E_JUMP:
    case trill::E_JTRUE:
    case trill::E_RETURN:
    case trill::E_LIST:
    case trill::E_UNKNOWN:
    case trill::E_ASSIGN:
        _stack.push_back(e);
        break;
    default:
        NOT_IMPLEMENTED();
        break;
    }
}

Expr * trill::FoldExpr::Fold(BinExprSubKind subKind, Expr *left, Expr* right)
{
    if (!left || !right)
        return nullptr;

    if (!left->is<ValueExpr>() || !right->is<ValueExpr>())
        return nullptr;

    ValueExpr& lvalue = left->as<ValueExpr>();
    ValueExpr& rvalue = right->as<ValueExpr>();
    bool  success = false;
    int   iretVal;
    float fretVal;
    ValueExprSubKind   newsubKind;

    switch (lvalue.subKind())
    {
    case ES_INT:
        if (rvalue.subKind() == ES_FLOAT)
        {
            newsubKind = ES_FLOAT;
            success = FoldValue(subKind, (float)lvalue.Int(), rvalue.Float(), fretVal);
        }
        else
        {
            newsubKind = ES_INT;
            success = FoldValue(subKind, lvalue.Int(), rvalue.Int(), iretVal);
        }
        break;
    case ES_FLOAT:
        newsubKind = ES_FLOAT;
        if (rvalue.subKind() == ES_FLOAT)
        {
            success = FoldValue(subKind, lvalue.Float(), rvalue.Float(), fretVal);
        }
        else
        {
            success = FoldValue(subKind, lvalue.Float(), (float)rvalue.Int(), fretVal);
        }
        break;
    }

    if (!success)
        return nullptr;

    Value v;
    newsubKind == ES_FLOAT ? v.setFloat(fretVal) : v.setInt(iretVal);
    Expr* e = _compiler->newValueExpr(newsubKind, v, lvalue.location());
    e->setType(GetBaseType(newsubKind == ES_FLOAT ? TS_FLOAT : TS_INT));
    return e;
}

Expr* trill::FoldExpr::Fold(Expr* e)
{
    _stack.clear();
    _stack.reserve(64);
    Expr::Accept(e, this);
    ASSERT(_stack.size() == 1);
    return _stack.back();
}

bool trill::FoldExpr::FoldValue(BinExprSubKind op, int v1, int v2, int &retVal)
{
    switch (op)
    {
    case ES_ADD:
        retVal = v1 + v2;
        break;
    case ES_SUB:
        retVal = v1 - v2;
        break;
    case ES_MUL:
        retVal = v1 * v2;
        break;
    case ES_DIV:
        retVal = v1 / v2;
        break;
    default:
        return false;
    }
    return true;
}

bool trill::FoldExpr::FoldValue(BinExprSubKind op, float v1, float v2, float &retVal)
{
    switch (op)
    {
    case ES_ADD:
        retVal = v1 + v2;
        break;
    case ES_SUB:
        retVal = v1 - v2;
        break;
    case ES_MUL:
        retVal = v1 * v2;
        break;
    case ES_DIV:
        retVal = v1 / v2;
        break;
    default:
        return false;
    }
    return true;
}

