//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  common.h
// PURPOSE:
// DATE: 2015/03/05
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#include "../config.h"
#include "../Location.h"

namespace trill {


    struct Value
    {
        union
        {
            char    *sval;
            int     ival;
            float   fval;
        } u;
        unsigned len_; //for string constant

        inline int Int() const
        {
            return u.ival;
        }
        inline float Float() const
        {
            return u.fval;
        }
        inline char* str() const
        {
            return u.sval;
        }
        inline unsigned len() const
        {
            return len_;
        }
        inline void setInt(int i)
        {
            u.ival = i;
        }
        inline void setFloat(float f)
        {
            u.fval = f;
        }
        inline void setStr(char *s, int l)
        {
            u.sval = s;
            len_ = l;
        }

        inline bool operator==(const Value & other)
        {
            return this->u.ival == other.u.ival;
        }
        inline bool operator!=(const Value & other)
        {
            return this->u.ival != other.u.ival;
        }

    };


    struct Token
    {
        int id;     //Token ID
        Value val; //Token value
        char *str;  //unparsed, pointer in the source buffer
        int len;    //length of unparsed source
        Location location;
    };



}








