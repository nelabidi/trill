//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill, scripting language
// FILE:  Semantics.h
// PURPOSE: implement Expression type checker class
// DATE: 2015/08/25
// LICENSE:    Copyright 2015 El Abidi Naoufel
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//////////////////////////////////////////////////////////////////////////

#ifndef Semantics_h__
#define Semantics_h__

namespace trill {

    struct Compiler;

    //updates expressions type
    /// <summary>
    /// ExprChecker implement the expression visitor interface, walks a given
    /// expression, updates types and throw errors.
    /// </summary>
    struct ExprChecker : ExprVisitor
    {
        ExprChecker(Compiler* compiler);
        void checkExpression(Expr* expr, ExprInfo& info);

    private:
        Compiler* _compiler;

        std::vector<ExprInfo> _exprInfoStack;

        void pushInfo(ExprInfo& info)
        {
            _exprInfoStack.push_back(info);
        }
        ExprInfo popInfo()
        {
            ASSERT(_exprInfoStack.size() > 0);
            ExprInfo top = _exprInfoStack.back();
            _exprInfoStack.pop_back();
            return top;
        }
        ExprInfo& topInfo()
        {
            ASSERT(_exprInfoStack.size() > 0);
            return _exprInfoStack.back();
        }

        //Expression visitor interface
        virtual void OnBinray(BinaryExpr& e);
        virtual void OnAssign(AssignExpr& e);
        virtual void OnArray(ArrayExpr& e);
        virtual void OnSymbol(SymbolExpr& e);
        virtual void OnCall(CallExpr& e);
        virtual void OnArg(ArgExpr& e);
        virtual void OnReturn(ReturnExpr& e);
        virtual void OnValue(ValueExpr& e);
        virtual void OnUnary(UnaryExpr& e);
        virtual void OnJTrue(JTrueExpr& e);

    };

}

#endif // Semantics_h__
