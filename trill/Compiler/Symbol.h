//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Symbol.h
// PURPOSE:
// DATE: 2015/08/21
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#pragma once

#include "Common.h"
#include "../Variant.h" //native function pointer


namespace trill {

    //forward declaration
    struct Expr;
    struct Scope;
    struct VariableSymbol;
    struct FunctionSymbol;
    struct ClassSymbol;

    //symbol kind
    enum SymbolKind
    {
        SK_UNDEFINED = 0,
        SK_VARIABLE,  // local/temp/global/member/outer
        SK_FUNCTION,  // function defined the script scope/ class method
        SK_CLASS,     // class object*/
        SK_CONST,
        SK_LABEL,     // label for jumps
        SK_NATIVE,
        /*SK_EXTERN,  //not defined in the script.
        SK_NULL,    //NULL
        SK_THIS,    //this
        SK_BASE,    //base*/


    };

    enum SymbolStorage
    {
        SS_UNKNOWN,
        SS_GLOBAL,  // global defined in script scope,context scope
        SS_LOCAL,   // parameter /local
        // SS_TEMP,    // local temporary  /register
        SS_MEMBER,  // property / key
        SS_OUTER,   // defined in another stack frame

    };

    struct Symbol
    {
        Symbol(SymbolKind kind) :
            kind_(kind),
            scope_(nullptr)
        {}
        inline SymbolKind  kind() const
        {
            return kind_;
        }
        inline char *name() const
        {
            return name_;
        }
        inline void setName(char *n)
        {
            name_ = n;
        }
        inline void setId(unsigned id)
        {
            id_ = id;
        }
        inline unsigned id() const
        {
            return id_;
        }
        inline Symbol* parent() const
        {
            return parent_;
        }
        inline void setParent(Symbol* parent)
        {
            parent_ = parent;
        }
        inline Scope* scope() const
        {
            return scope_;
        }
        inline void setScope(Scope* scope)
        {
            scope_ = scope;
        }
        //casting helpers
        template<typename T>
        inline bool is() const
        {
            return kind_ == T::kind;
        }

        template <class T>
        T& as()
        {
            ASSERT(this->is<T>());
            return *static_cast<T*>(this);
        }

        template <class T>
        const T& as() const
        {
            ASSERT(this->is<T>());
            return *static_cast<const T*>(this);
        }

    protected:
        SymbolKind kind_;
        char*    name_;
        unsigned id_;
        Symbol*  parent_;
        Scope*   scope_;
    };

    //base for symbols with location and type
    struct SourceSymbol : Symbol
    {
        SourceSymbol(SymbolKind kind, Type* type = nullptr, const Location& loc = Location::empty()) :
            Symbol(kind),
            loc_(loc),
            type_(type)
        {}
        inline int column() const
        {
            return loc_.column;
        }
        inline int line() const
        {
            return loc_.lineno;
        }
        inline TypeSpec typeId() const
        {
            ASSERT(type_ != nullptr);
            return type_->id();
        }
        inline Type* type() const
        {
            return type_;
        }
        inline void setType(Type* type)
        {
            type_ = type;
        }
    protected:
        Type*     type_;
        Location  loc_;
    };


    struct VariableSymbol : SourceSymbol
    {
        static const SymbolKind kind = SK_VARIABLE;
        VariableSymbol(Type* type, const Location& loc) :
            SourceSymbol(SK_VARIABLE, type, loc),
            _expr(nullptr)
        {
        }
        inline SymbolStorage storage() const
        {
            return storage_;
        }

        inline void setStorage(SymbolStorage storage)
        {
            storage_ = storage;
        }

        inline unsigned index() const
        {
            return index_;
        }

        inline void setIndex(unsigned index)
        {
            index_ = index;
        }
        inline Expr* value() const
        {
            return _expr;
        }
        inline void setValue(Expr* val)
        {
            _expr = val;
        }

    protected:
        SymbolStorage storage_;
        unsigned int  index_;
        Expr*       _expr;

    };

    struct ConstSymbol : SourceSymbol
    {
        static const SymbolKind kind = SK_CONST;
        ConstSymbol(Type* type, const Location& loc) :
            SourceSymbol(SK_CONST, type, loc),
            _expr(nullptr)
        {
        }
        inline Expr* value() const
        {
            return _expr;
        }
        inline void setValue(Expr* val)
        {
            _expr = val;
        }
    protected:
        Expr*       _expr;
    };



    struct FunctionSymbol : SourceSymbol
    {
        static const SymbolKind kind = SK_FUNCTION;
        FunctionSymbol(Type* type, const Location& loc) :
            SourceSymbol(SK_FUNCTION, type, loc),
            private_(false), main_(false)
        {
        }
        inline unsigned nlocals() const
        {
            return locals_.size();
        }
        inline unsigned nargs() const
        {
            return nargs_;
        }
        inline void setnArgs(unsigned nargs)
        {
            ASSERT(nargs <= nlocals());
            nargs_ = nargs;
        }
        inline unsigned addLocal(Symbol* local)
        {
            locals_.push_back(local);
            return locals_.size() - 1;
        }
        inline Symbol*  getLocal(unsigned index)
        {
            ASSERT(index < locals_.size());
            return locals_[index];
        }
        inline bool isPrivate() const
        {
            return private_;
        }
        inline void setPrivate(bool b)
        {
            private_ = b;
        }
        inline Expr* body() const
        {
            return body_;
        }
        inline void setBody(Expr*body)
        {
            body_ = body;
        }
        inline bool isMain() const
        {
            return main_;
        }
        inline void setMain(bool b)
        {
            main_ = b;
        }
        inline TypeList* prototype() const
        {
            return proto_;
        }
        inline void setPrototype(TypeList* p)
        {
            proto_ = p;
        }
    protected:
        TypeList*            proto_;
        std::vector<Symbol*> locals_;
        unsigned             nargs_;
        Expr*                body_;
        bool                 private_;
        bool                 main_;

    };


    /*struct ClassSymbol : SourceSymbol
    {
        static const SymbolKind kind = SK_CLASS;
        ClassSymbol(TypeSpec type, const Location& loc) :
            SourceSymbol(SK_CLASS, type, loc)
        {
        }
    };*/

    struct LabelSymbol : Symbol
    {
        static const SymbolKind kind = SK_LABEL;
        LabelSymbol() : Symbol(SK_LABEL), defined_(false) {}

        inline unsigned address()const
        {
            return address_;
        }
        inline bool defined() const
        {
            return defined_;
        }
        inline void setAddress(unsigned address)
        {
            address_ = address;
            defined_ = true;
        }
    private:
        unsigned address_;
        bool     defined_;
    };


    struct NativeSymbol : SourceSymbol
    {
        static const SymbolKind kind = SK_NATIVE;

        NativeSymbol() : SourceSymbol(SK_NATIVE) {}

        inline NativeFunctionPtr  fnptr() const
        {
            return fnptr_;
        }
        inline void setFunctionPtr(NativeFunctionPtr ptr)
        {
            fnptr_ = ptr;
        }

        inline unsigned nargs() const
        {
            return nargs_;
        }
        inline void setnArgs(unsigned nargs)
        {
            nargs_ = nargs;
        }
        /*inline TypeSpec type() const
        {
            return type_;
        }
        inline void setType(TypeSpec type)
        {
            type_ = type;
        }*/
    protected:
        NativeFunctionPtr  fnptr_;
        unsigned nargs_;
        //TypeSpec type_;
    };


    struct SymbolFactory
    {
        virtual VariableSymbol* newVariableSymbol(char* name, TypeSpec type, Location& loc) = 0;
        virtual FunctionSymbol* newFunctionSymbol(char* name, TypeSpec type, Location& loc) = 0;
        virtual LabelSymbol*    newLabelSymbol(char *name) = 0;
    };




    inline bool IsLocal(Symbol *sym)
    {
        ASSERT(sym != nullptr);
        return sym->is<VariableSymbol>() && sym->as<VariableSymbol>().storage() == SS_LOCAL;
    }
    inline bool IsGlobal(Symbol *sym)
    {
        ASSERT(sym != nullptr);
        return sym->is<VariableSymbol>() && sym->as<VariableSymbol>().storage() == SS_GLOBAL;
    }

    //bool IsLocalSymbol(Symbol *sym);


}