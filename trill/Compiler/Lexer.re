// 
// Lexer based on re2c
//
/////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Common.h"
#include "grammar.h"
#include "Lexer.h"
#include "Compiler.h"

using namespace trill;

#pragma warning(disable : 4390)

//re2c defines
#define YYCTYPE     char
#define YYCURSOR    cursor
#define YYLIMIT     _end
#define YYMARKER    _marker

// re2c, global configuration
/*!re2c
re2c:yyfill:enable = 0;
any = [\000-\377];
D   = [0-9];
L   = [a-zA-Z_];
H   = [a-fA-F0-9];
E   = [Ee] [+-]? D+;
ESC = [\\] ([abfnrtv?'"\\] | "x" H+);
*/

//helpers
#define RET(i)  { _cursor = cursor; _oldChar = *cursor; *cursor = 0; _tokenLen = cursor - _tokenStr; return i;}
#define MAKE_STRING _cursor = cursor; _oldChar = *cursor; *cursor = 0; _tokenLen = cursor - _tokenStr


void Lexer::Initialize()
{
    _cursor =  _source;
    if( _bsize == 0)
        _bsize = LStrlen(_source) + 1;
    _end =  _source +  _bsize;
    _marker = NULL;
    _lineno = LINE_INDEX_BASE;
    _lineStr = _cursor;
    _oldChar = *_cursor;
}

char * Lexer::CharToString( char c )
{
    static char str[4];
    switch(c)
    {
    case '\n':
    case '\r':
        return "EOL";
    default:
        sprintf_s(str, 4, "%c", c);
    }
    return str;
}

void Lexer::ReportError(const char *msg)
{
    static char message[260];
    sprintf_s(message, 256, "%s", msg);
    Location l;
    l.column = _tokenStr - _lineStr;
    l.lineno = _lineno;
    _compiler->ReportSyntaxError(l, message);
}


void Lexer::ReportSyntaxError(const char *msg)
{
    static char message[260];
    sprintf_s(message, 256, "%s `%s`", msg, CharToString(*_tokenStr));

    Location l;
    l.column = _tokenStr - _lineStr;
    l.lineno = _lineno;
    _compiler->ReportSyntaxError(l, message);

}

int Lexer::ScanInteger(Token &token, int radix)
{
    char *tmp;
    token.val.setInt((int)strtol(_tokenStr, &tmp, radix));
    return ICONST;
}
int Lexer::ScanFloat(Token &token)
{
    char *tmp;
    token.val.setFloat((float)strtod(_tokenStr, &tmp));
    return FCONST;
}

int Lexer::ScanVerbatim(Token &token)
{
    char *buffer = (char *) _alloca(_tokenLen + 1);
    char *cursor = _tokenStr, *output = buffer;
    ++cursor;
    while(*cursor)
    {
        char *start = cursor;
        /*!re2c
            "\\'" { *output++ ='\''; }
            "'"   { *output = 0; break; }
            [\r\n]         { ReportError("unterminated string literal"); continue; }
            [^\r\n\\']+   { memcpy(output,start,cursor-start); output+= (cursor-start); continue;   }
        */
    }
	token.val.setStr(_compiler->newString(buffer, output - buffer), output - buffer);
    return CSCONST;

}
int Lexer::ScanIdentifier(Token &token)
{
    token.val.setStr(_compiler->newName(_tokenStr, _tokenLen),_tokenLen);
    return IDENT;

}

int Lexer::ScanString(Token &token)
{
    char *buffer = (char *) _alloca(_tokenLen + 1);

    //scan for escape chars and replace with thier values
    //strip double quote
    char *cursor = _tokenStr, *output = buffer;
    ++cursor;
    for(; *cursor;)
    {
        char *start = cursor;
        char *tmp;

        /*!re2c
            "\\" [0-7]{1,3}           {
                int v = (int)strtol(start+1,&tmp,8);
                *output++ = (char)v;
            }
            "\\" [xX][0-9A-Fa-f]{1,2} {
                int v = (int)strtol(start+2,&tmp,16);
                *output++ = (char)v;
            }
            "\\\\"         { *output++ ='\\'; continue; }
            "\\\""         { *output++ ='"'; continue; }
            "\\?"          { *output++ ='?'; continue; }
            "\\a"          { *output++ ='\a'; continue; }
            "\\b"          { *output++ ='\b'; continue; }
            "\\f"          { *output++ ='\f'; continue; }
            "\\n"          { *output++ ='\n'; continue; }
            "\\r"          { *output++ ='\r'; continue; }
            "\\t"          { *output++ ='\t'; continue; }
            "\\v"          { *output++ ='\v'; continue; }
            "\""           { *output = 0; break; }
            "\\"           { ReportSyntaxError("Invalid escape sequence"); continue; }
            [\r\n]         { ReportError("unterminated string literal"); continue;}
            [^\r\n\\\"]+   { memcpy(output,start,cursor-start); output+= (cursor-start); continue;  }
        */
    }
   	token.val.setStr(_compiler->newString(buffer, output - buffer), output - buffer);
    return SCONST;
}


int Lexer::ScanCommentLine()
{
    char *cursor = _cursor;
    for(;;)
    {
        /*!re2c
            "\n"
            {
                if(*cursor == 0)
                {
                    return EOI;
                }
                _lineStr = cursor;
                _cursor = cursor;
                _lineno++;
                return 1;
            }
            "\000"  { return EOI;}
            any   { continue; }
        */
    }
}

//return 1 if sucess otherwise 0
int Lexer::ScanCommentBlock()
{
    char *cursor = _cursor;
    for(;;)
    {
        /*!re2c

            "*/"	{ _cursor = cursor; return 1; }
        "\n"
    {
        //TODO: raise an error
        if(*cursor == 0)
    {
        ReportError("unterminated comment block");
        _cursor = cursor;
        return EOI;
    }
        _lineno++;
        continue;
    }
        "\000"
    {
        ReportError("unterminated comment block");
        _cursor = cursor;
        return EOI;
    }
        any		{ continue; }
        */
    }
}


int Lexer::Scan(Token &token)
{
    char *cursor = _cursor;
    *(cursor) = _oldChar;

    while(true)
    {
        _tokenStr = cursor;
        /*!re2c
        "/*"            { _cursor = cursor; if(ScanCommentBlock()){cursor = _cursor;  continue;} return EOI; }
        "//"            { _cursor = cursor; if(ScanCommentLine()) {cursor = _cursor;  continue;} return EOI; }
        "#"             { _cursor = cursor; if(ScanCommentLine()) {cursor = _cursor;  continue;} return EOI; }

        "base"          { MAKE_STRING; ScanIdentifier(token); return ABASE;  }
        "break"         { RET(BREAK); }
        "catch"         { RET(CATCH);}
        "class"         { RET(CLASS); }
        "continue"      { RET(CONTINUE); }
		"const"         { RET(CONST);}
        "do"            { RET(DO); }
        "else"          { RET(ELSE); }
        "false"         { RET(AFALSE); }
        "for"           { RET(FOR); }
        "function"      { RET(FUNCTION);}
        "if"            { RET(IF); }
        "in"            { RET(IN); }
        "null"          { MAKE_STRING; ScanIdentifier(token); return ANULL;  }
        "return"        { RET(RETURN); }
        "throw"         { RET(THROW);}
        "try"           { RET(TRY);}
        "true"          { RET(ATRUE); }
        "this"          { MAKE_STRING; ScanIdentifier(token); return ATHIS; }
        "var"           { RET(VAR); }
		"int"			{ RET(INT_TS); }
		"uint"			{ RET(UINT_TS); }
		"char"			{ RET(CHAR_TS); }
		"uchar"			{ RET(UCHAR_TS); }
		"float"			{ RET(FLOAT_TS); }
		"string"        { RET(STRING_TS); }
		"bool"			{ RET(BOOL_TS); }
        "while"         { RET(WHILE); }

        L (L | D)*        { MAKE_STRING; return ScanIdentifier(token); }

        "0" [xX] H +     { MAKE_STRING; return ScanInteger(token, 16); }
        "0" D +          { MAKE_STRING; return ScanInteger(token, 8);}
        D +              { MAKE_STRING; return ScanInteger(token, 10); }

        (D + E) | (D* "." D + E ? ) | (D + "." D * E ? )
        {
            MAKE_STRING;
            return ScanFloat(token);
        }

        (["] (ESC|any\[\n\\"])* ["])  { MAKE_STRING; return ScanString(token); }

        (['] (any\[\n\\'])* [']) { MAKE_STRING; return ScanVerbatim(token); }

        [ \t\v\f\r]+		{ continue; }

        ">="		{ RET(GTE);}
        ">>"		{ RET(SHR);}
		"<<"		{ RET(SHL);}

        "||"		{ RET(LOR);}
        "&&"		{ RET(LAND);}
        "!"			{ RET(LNOT);}
        "=="		{ RET(EQ);}
        "!="		{ RET(NEQ);}
        "<="		{ RET(LTE);}

		"++"		{ RET(INC); }
		"--"		{ RET(DEC); }
		"+="        { RET(ADD_ASSIGN); }
		"-="        { RET(SUB_ASSIGN); }
		"*="        { RET(MUL_ASSIGN); }
		"/="        { RET(DIV_ASSIGN); }
		"%="        { RET(MOD_ASSIGN); }
		"|="        { RET(OR_ASSIGN); }
		"^="        { RET(XOR_ASSIGN); }
		"&="        { RET(AND_ASSIGN); }
		"<<="       { RET(SHL_ASSIGN); }
		">>="       { RET(SHR_ASSIGN); }

        "["			{ RET(LBRACKET);}
        "]"			{ RET(RBRACKET);}
        "}"			{ RET(RBRACE);}
        "{"			{ RET(LBRACE);}
        "<"			{ RET(LT);}
        ">"			{ RET(GT);}
        "~"			{ RET(NOT);}
        "*"			{ RET(MUL);}
        "/"			{ RET(DIV);}
        "%"			{ RET(MOD);}
        "+"			{ RET(ADD);}
        "-"			{ RET(SUB);}
        "*"			{ RET(MUL);}
        "."			{ RET(DOT);}
        "|"			{ RET(OR);}
        "^"			{ RET(XOR);}
        "&"			{ RET(AND);}
        ";"			{ RET(SEMI); }
        ","			{ RET(COMMA); }

        "("			{ RET(LPAREN); }
        ")"			{ RET(RPAREN); }
        "="			{ RET(ASSIGN);}
        "\n"
        {
			if(*cursor == 0) return 0;
			_lineStr = cursor;
			_lineno++;
			continue;
		}
        "\000"  { return EOI;}

        any
        {
			ReportSyntaxError("unexpected character");
			continue;
		}
		*/
    } //end while(true)

    return EOI;

}

