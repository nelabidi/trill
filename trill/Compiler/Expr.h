//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Expr.h
// PURPOSE:
// DATE: 2015/03/08
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#pragma once

#include "Common.h"
#include "Symbol.h"

namespace trill {

    //Main expression kind
    enum ExprKind
    {
        E_UNKNOWN = 0,
        E_ASSIGN,  // ASSIGN(left,right) source,destination: symbol Temp/local/global
        E_BINARY,  // BINOP(left,right,subkind) symbol = temp.
        E_UNARY,   // UNARY(left,subkind)
        E_VALUE,   // Value Expression
        E_SYMBOL,  // Symbol Expression
        E_CALL,    // CALL(left,right), left:function, right: args: list
        E_ARG,     // Expression holding an argument in Call Expr
        E_ARRAY,   // ARRAY(fields) fields are list of expressions

        E_LABEL,    //Label Expression
        E_JUMP,     //unconditional jump, JUMP(symbol)
        E_JTRUE,    //JTRUE(subType,left,right,symbol/label)
        E_RETURN,   //Return expression

        E_LIST,  //left: head, right: tail, we reuse the left/right fields as head and tail

    };

    enum
    {
        ES_UNKNOWN = 0,
    };
    //binary operators
    enum BinExprSubKind
    {
        //BinaryExpr subkind
        ES_ADD = ES_UNKNOWN + 1,
        ES_SUB,
        ES_MUL,
        ES_DIV,
        ES_MOD,
        ES_SHL,
        ES_SHR,
        ES_AND, //binary and
        ES_XOR,
        ES_OR,
        ES_DOT,
        ES_INDEX,   // INDEX(left,right), same as member ES_DOT, right can be a constant
        ES_LOR,
        ES_LAND,

        //condition JTrueExpr /operators
        ES_LT,
        ES_LTE,
        ES_GT,
        ES_GTE,
        ES_EQ,
        ES_NEQ,
        ES_IN,

    };
    //unary expression subkind
    enum UnaryExprSubKind
    {

        // ES_UNKNOWN = 0,
        //UnaryExpr
        ES_UADD = ES_UNKNOWN + 1,
        ES_USUB,
        ES_NOT, //binary not
        ES_LNOT, //logical not
        ES_INC,
        ES_DEC,
        ES_PINC,
        ES_PDEC,

    };

    //Value expression kind
    enum ValueExprSubKind
    {
        //ValueExpr constant / Value
        ES_FLOAT = ES_UNKNOWN + 1,
        ES_INT,
        ES_STRING,
        ES_NULL,
        ES_THIS,
        ES_BASE,
        ES_TRUE,
        ES_FALSE,
    };

    struct ExprVisitor;
    struct ExprSimpleVisitor;

    //Expr define intermediate language
    struct Expr
    {
        Expr(ExprKind kind = E_UNKNOWN):
            kind_(kind),
            type_(nullptr), //type is the static type, this is set during type check
            next_(nullptr)  //used to build statement list
        {}

        inline ExprKind kind()  const
        {
            return kind_;
        }
        inline Expr* next()  const
        {
            return next_;
        }
        inline void setNext(Expr* next)
        {
            next_ = next;
        }

        inline TypeSpec typeId() const
        {
            ASSERT(type_ != nullptr);
            return type_->id();
        }
        inline Type* type() const
        {
            return type_;
        }
        inline void setType(Type* type)
        {
            type_ = type;
        }

        inline Location location() const
        {
            return location_;
        }
        inline void setLocation(const Location& location)
        {
            location_ = location;
        }

        //casting helper, return true is this instance is kind of T
        template<typename T>
        inline bool is() const
        {
            return kind_ == T::kind;
        }
        //return a reference to the subclass
        template <class T>
        T& as()
        {
            ASSERT(this->is<T>());
            return *static_cast<T*>(this);
        }
        //cast down to a const reference of type T
        template <class T>
        const T& as() const
        {
            ASSERT(this->is<T>());
            return *static_cast<const T*>(this);
        }
        //Walkers /visitors
        static void Accept(Expr *expr, ExprVisitor* visitor);
        //
        static void Accept(Expr *expr, ExprSimpleVisitor* visitor);

        //for debug purpose
        static void Dump(Expr*, FILE* out = stderr);

    protected:
        Expr *next_;
        Type*    type_;
        ExprKind kind_;
        Location location_;
    };
    //Binary expression, for binary operators
    struct BinaryExpr : Expr
    {
        static const ExprKind kind = E_BINARY;
        BinaryExpr(BinExprSubKind subkind, Expr* left = nullptr, Expr* right = nullptr) :
            Expr(E_BINARY),
            subkind_(subkind),
            left_(left),
            right_(right)
        {
        }

        inline BinExprSubKind subKind() const
        {
            return subkind_;
        }
        inline Expr* left() const
        {
            return left_;
        }
        inline Expr* right() const
        {
            return right_;
        }
        inline void setLeft(Expr *left)
        {
            left_ = left;
        }
        inline void setRight(Expr* right)
        {
            right_ = right;
        }
        static const char* operatorName(BinaryExpr& e);
    protected:
        Expr*           left_;
        Expr*           right_;
        BinExprSubKind     subkind_;
    };
    //Assignment Expression
    struct AssignExpr : Expr
    {
        static const ExprKind kind = E_ASSIGN;
        AssignExpr(Expr* left = nullptr, Expr* right = nullptr)
            : Expr(E_ASSIGN),
              left_(left),
              right_(right)
        {
        }

        inline Expr* left() const
        {
            return left_;
        }
        inline Expr* right() const
        {
            return right_;
        }
        inline void setLeft(Expr *left)
        {
            left_ = left;
        }
        inline void setRight(Expr* right)
        {
            right_ = right;
        }
    protected:
        Expr*           left_;
        Expr*           right_;
    };
    //Value expression is any value/constant found in the source code
    struct ValueExpr : Expr
    {
        static const ExprKind kind = E_VALUE;
        ValueExpr(ValueExprSubKind subkind, Value value) :
            Expr(E_VALUE),
            subkind_(subkind),
            value_(value)
        {
        }
        inline ValueExprSubKind subKind() const
        {
            return subkind_;
        }
        inline const Value& value() const
        {
            return value_;
        }
        inline bool isFloat() const
        {
            return subkind_ == ES_FLOAT;
        }
        inline bool isInt() const
        {
            return  subkind_ == ES_INT;
        }
        inline bool isBoolean() const
        {
            return subkind_ == ES_TRUE || subkind_ == ES_FALSE;
        }
        inline bool isString() const
        {
            return subkind_ == ES_STRING;
        }

        inline float Float() const
        {
            ASSERT(subkind_ == ES_FLOAT);
            return value_.Float();
        }

        inline int Int() const
        {
            ASSERT(subkind_ == ES_INT);
            return value_.Int();
        }
        inline char * str() const
        {
            return value_.str();
        }
        inline unsigned len() const
        {
            return value_.len();
        }

    protected:
        ValueExprSubKind subkind_;
        Value       value_;

    };
    //constant expression
    /*struct ConstExpr : Expr
    {
        static const ExprKind kind = E_CONST;
        ConstExpr(Expr* sym, Expr* val):
            Expr(E_CONST),
            _symExpr(sym),
            _value(val)
        {
        }
        inline Symbol* symbol()
        {
            ASSERT(_symExpr != nullptr && _symExpr->is<SymbolExpr>());
            return _symExpr->as<SymbolExpr>->symbol();
        }
        inline ValueExpr* value()
        {
            ASSERT(_value != nullptr && _value->is<ValueExpr>());
            return (ValueExpr*)_value;
        }

    protected:
        Expr* _symExpr;
        Expr*   _value;

    };*/
    //unary expression for unary operators
    struct UnaryExpr : Expr
    {
        static const ExprKind kind = E_UNARY;
        UnaryExpr(UnaryExprSubKind subkind, Expr* expr = nullptr) :
            Expr(E_UNARY),
            subkind_(subkind),
            expr_(expr)
        {
        }
        inline UnaryExprSubKind subKind() const
        {
            return subkind_;
        }
        inline Expr* expr() const
        {
            return expr_;
        }
        inline void setExpr(Expr* expr)
        {
            expr_ = expr;
        }

    protected:
        Expr   *expr_;
        UnaryExprSubKind subkind_;

    };

    //Array expression, store the Array declaration or initializer
    struct ArrayExpr : Expr
    {
        static const ExprKind kind = E_ARRAY;
        ArrayExpr(Expr* fields) :
            Expr(E_ARRAY),
            fields_(fields)
        {
        }
        inline Expr* fields() const
        {
            return fields_;
        }
        inline void setFields(Expr* fields)
        {
            fields_ = fields;
        }

    protected:
        Expr   *fields_;

    };
    //Symbol expression store a symbol, this can be Function, identifier..etc
    struct SymbolExpr : Expr
    {
        static const ExprKind kind = E_SYMBOL;
        SymbolExpr(Symbol *sym)
            : Expr(E_SYMBOL),
              symbol_(sym)
        {}

        inline Symbol* symbol() const
        {
            return symbol_;
        }
        inline void setSymbol(Symbol*sym)
        {
            symbol_ = sym;
        }

    protected:
        Symbol * symbol_;
    };
    //label expression store the label symbol, used by jump expressions
    struct LabelExpr : Expr
    {
        static const ExprKind kind = E_LABEL;
        LabelExpr(LabelSymbol *label) : Expr(E_LABEL), label_(label)
        {}

        inline LabelSymbol* label() const
        {
            return label_;
        }
        inline void setLabel(LabelSymbol*label)
        {
            label_ = label;
        }

    protected:
        LabelSymbol *label_;
    };
    //direct jump expression, used in conjunction with JTrue expression
    //or for goto, break, continue statement
    struct JumpExpr : Expr
    {
        static const ExprKind kind = E_JUMP;
        JumpExpr(LabelSymbol* label) :
            Expr(E_JUMP),
            label_(label)
        {
        }
        inline LabelSymbol* label() const
        {
            return label_;
        }
        inline void setLabel(LabelSymbol *label)
        {
            label_ = label_;
        }

    protected:
        LabelSymbol*  label_;
    };
    //JTrue expression, this hold an expression like this : JTRUE(condition, label)
    struct JTrueExpr : Expr
    {
        static const ExprKind kind = E_JTRUE;
        JTrueExpr(BinExprSubKind subkind, LabelSymbol* label = nullptr, Expr* left = nullptr, Expr* right = nullptr) :
            Expr(E_JTRUE),
            label_(label),
            subkind_(subkind),
            left_(left),
            right_(right)
        {
        }
        //the subkind expression is where we store, what kind of jump
        // this one of BinExprSubkind, LT,LTE..etc
        inline BinExprSubKind subKind() const
        {
            return subkind_;
        }
        //label to jump too
        inline LabelSymbol* label() const
        {
            return label_;
        }
        inline void setLabel(LabelSymbol *label)
        {
            label_ = label_;
        }
        //left/right are expressions when this Expression holds
        //a non binary condition example: a == 0 || b == c
        inline Expr* left() const
        {
            return left_;
        }
        inline Expr* right() const
        {
            return right_;
        }
        inline void setLeft(Expr *left)
        {
            left_ = left;
        }
        inline void setRight(Expr* right)
        {
            right_ = right;
        }
    protected:
        Expr*           left_;
        Expr*           right_;
        LabelSymbol*  label_;
        BinExprSubKind subkind_;
    };
    //return statement expression
    struct ReturnExpr : Expr
    {
        static const ExprKind kind = E_RETURN;
        ReturnExpr(Expr* expr = nullptr) :
            Expr(E_RETURN),
            expr_(expr)
        {
        }
        inline Expr* expr() const
        {
            return expr_;
        }
        inline void setExpr(Expr* expr)
        {
            expr_ = expr;
        }

    protected:
        Expr   *expr_;
    };

    //Call expression, store the function expression and the arguments list
    struct CallExpr : Expr
    {
        static const ExprKind kind = E_CALL;
        CallExpr(Expr* func, Expr* args)
            : Expr(E_CALL),
              function_(func),
              args_(args)
        {
        }
        inline Expr* function() const
        {
            return function_;
        }
        inline Expr* args() const
        {
            return args_;
        }

    protected:
        Expr * function_;
        Expr * args_;
    };
    //Argument expression for function parameters
    struct ArgExpr : Expr
    {
        static const ExprKind kind = E_ARG;
        ArgExpr(Expr* arg):
            Expr(E_ARG),
            arg_(arg)
        {}
        inline Expr* arg() const
        {
            return arg_;
        }
        inline void setArg(Expr* arg)
        {
            arg_ = arg;
        }
    protected:
        Expr* arg_;
    };

    struct ExprList : Expr
    {
        static const ExprKind kind = E_LIST;
        ExprList(Expr* head = nullptr, Expr* tail = nullptr)
            : Expr(E_LIST),
              head_(head),
              tail_(tail)
        {
        }

        inline Expr* head() const
        {
            return head_;
        }
        inline Expr* tail() const
        {
            return tail_;
        }
        inline void setHead(Expr*h)
        {
            head_ = h;
        }
        inline void setTail(Expr*t)
        {
            tail_ = t;
        }
        static void Accept(Expr *expr, ExprVisitor* visitor);

    protected:
        Expr* head_;
        Expr* tail_;
    };

    //Expression visitor, a typed visitor
    struct ExprVisitor
    {
        virtual void OnBinray(BinaryExpr& e) = 0;
        virtual void OnAssign(AssignExpr& e) = 0;
        virtual void OnValue(ValueExpr& e) = 0;
        virtual void OnUnary(UnaryExpr& e) = 0;
        virtual void OnArray(ArrayExpr& e) = 0;
        virtual void OnSymbol(SymbolExpr& e) = 0;
        virtual void OnJump(JumpExpr& e) {};
        virtual void OnJTrue(JTrueExpr& e) = 0;
        virtual void OnCall(CallExpr& e) = 0;
        virtual void OnArg(ArgExpr& e) = 0;
        virtual void OnList(ExprList& e) {};
        virtual void OnLabel(LabelExpr& e) {};
        virtual void OnReturn(ReturnExpr& e) = 0;
    };

    //a catch all visitor
    struct ExprSimpleVisitor
    {
        //catch all
        virtual void Visit(Expr * e) {}
    };


    /// <summary>
    /// struct holding information about an Expression.
    /// used during type check step
    /// </summary>
    struct ExprInfo
    {
        ExprInfo(Expr*e = nullptr, Type*t = nullptr):
            blvalue_(false),
            bside_effect_(false) {}

        //return true if the expression is a value
        inline bool islvalue() const
        {
            return blvalue_;
        }
        inline void setlvalue(bool lvalue)
        {
            blvalue_ = lvalue;
        }
        /// <summary>
        /// a side effect expression is any expression the affect the output of a statement.
        /// assignment, function call are examples or expressions with side effect
        /// </summary>
        /// <returns>true if the expression has a side effect</returns>
        inline bool hasSideEffect() const
        {
            return bside_effect_;
        }
        inline void setSideEffect(bool flag)
        {
            bside_effect_ = flag;
        }
    protected:
        bool   blvalue_;
        bool   bside_effect_;
    };

#if 0


    //Everything is an expression
    struct Expr
    {
        Expr *left, *right;
        Expr *next;
        ExprKind  kind;
        int     subKind; //one of the token IDs
        Value   val;
        Symbol  *symbol;
        TypeSpec type;

        Location location;
        int id;

        inline bool IsConstant()
        {
            return kind == E_CONSTANT;
        }
        inline bool IsFloat()
        {
            return IsConstant() && subKind == FCONST;
        }
        inline bool IsInt()
        {
            return IsConstant() && subKind == ICONST;
        }
        //value
        inline float Float()
        {
            return val.Float();
        }
        inline int   Int()
        {
            return val.Int();
        }


        inline Expr* Left()
        {
            return left;
        }
        inline Expr* Right()
        {
            return right;
        }
        inline Expr* Head()
        {
            ASSERT(kind == E_LIST);
            return left;
        }
        inline Expr* Tail()
        {
            ASSERT(kind == E_LIST);
            return right;
        }

        static void Traverse(Expr *expr, ExprVisitor* visitor)
        {
            for (Expr *e = expr; e != nullptr; e = e->next)
            {
                //visitor->Visit(e);
                switch (e->kind)
                {
                case E_IDENT:
                    visitor->OnIdent(e);
                    break;
                case E_BINARY:
                    visitor->OnBinray(e);
                    break;
                case E_CONSTANT:
                    visitor->OnConstant(e);
                    break;
                case E_SYMBOL:
                    visitor->OnSymbol(e);
                    break;
                case E_ASSIGN:
                    visitor->OnAssign(e);
                    break;
                case E_CALL:
                    visitor->OnCall(e);
                    break;
                case E_LABEL:
                    visitor->OnLabel(e);
                    break;
                case E_JTRUE:
                    visitor->OnJTrue(e);
                    break;
                case E_JUMP:
                    visitor->OnJump(e);
                    break;
                default:

                    throw std::exception("Not implemented");
                    break;
                }
            }
        }

        static void Traverse(Expr *expr, ExprSimpleVisitor* visitor)
        {
            for (Expr *e = expr; e != nullptr; e = e->next)
            {
                visitor->Visit(e);
            }
        }

    };

    struct ExprList
    {
        Expr *first;
        Expr *last;
    };


    struct Declaration
    {
        Symbol*  symbol;
        Expr*    proto;
        Expr*    body;
        //Declaration* next;
    };
#endif
    /*struct FunctionStateList
    {
        FunctionState *first;
        FunctionState *last;
    };*/

}

void TraverseExpr(trill::Expr *expr, trill::ExprVisitor *visitor);
void DumpExpr(trill::Expr *, FILE *);





