//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Scope.h
// PURPOSE:
// DATE: 2015/08/22
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef Scope_h__
#define Scope_h__



namespace trill {

    struct Symbol;

    enum ScopeKind
    {
        GLOBAL_SCOPE,
        LOCAL_SCOPE,
        //FUNCTION_SCOPE,
        //CLASS_SCOPE,
    };
    //scope
    struct Scope
    {
        Scope(ScopeKind kind, Scope* parent)
            : parent_(parent),
              kind_(kind)
        {

        }
        inline Scope* parent() const
        {
            return parent_;
        }
        inline void setParent(Scope* parent)
        {
            parent_ = parent;
        }
        ScopeKind kind()
        {
            return kind_;
        }
        virtual Symbol* findSymbol(char* name) = 0;
        virtual void  addSymbol(Symbol* sym) = 0;

    protected:
        Scope*    parent_;
        ScopeKind kind_;

    };

    struct LocalScope : Scope
    {
        LocalScope(Scope* parent = nullptr) :
            Scope(LOCAL_SCOPE, parent)
        {
        }



        void    clear()
        {
            _symbols.clear();
        }
        inline const std::vector<Symbol*>& symbols() const
        {
            return _symbols;
        }
        virtual Symbol* findSymbol(char* name);
        virtual void addSymbol(Symbol *sym)
        {
            _symbols.push_back(sym);
        }
    private:
        std::vector<Symbol*> _symbols;
    };


    struct GlobalScope : Scope
    {
        GlobalScope() :
            Scope(GLOBAL_SCOPE, nullptr)
        {
        }

        virtual Symbol* findSymbol(char* name);
        virtual void addSymbol(Symbol* sym);


    private:
        std::unordered_map<char*, Symbol*> _symbols;
    };

}

#endif // Scope_h__
