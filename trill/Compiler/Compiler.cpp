//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Compiler.cpp
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "../ErrorReporter.h"
#include "../SourceInfo.h"
#include "../internal.h"
#include "../Utils/Hash.h"
#include "Compiler.h"
#include "Parser.h"

#include "../Runtime.h"
#include "../Context.h"




/*

#include "Common.h"
#include "../Location.h"
#include "../internal.h"
#include "../Context.h"
#include "Symbol.h"

#include "Scope.h"
#include "Semantics.h"
*/

using namespace trill;

void trill::Compiler::DumpSymbols(const char* symFile)
{
    using namespace std;
    ofstream out;
    out.open(symFile);
    if (out.fail())
    {
        char buffer[256];
        strerror_s(buffer, sizeof(buffer), errno);
        cerr << buffer;
        return;
    }
    out << "Symbols: in " << _source->filename << endl;
    for (auto sym : _scriptScope.symbols())
    {
        out << sym->name() << "\t" << sym->id() << "\t";
        if (sym->is<FunctionSymbol>())
            out << "function ";// endl;
        else if (sym->is<VariableSymbol>())
        {
            out << "variable ";// endl;
            VariableSymbol& local = sym->as<VariableSymbol>();
            switch (local.storage())
            {
            case trill::SS_UNKNOWN:
                out << "unknown storage";// endl;
                break;
            case SS_GLOBAL:
                out << "global";
                break;
            case trill::SS_MEMBER:
                out << "member";
                break;
            case trill::SS_LOCAL:
                out << "local";
                break;
            case trill::SS_OUTER:
                out << "outer";
                break;
            default:
                INTERNAL_ERROR("Shouldn't reach this");
                break;
            }
        }
        else
            out << "UKNOWN SYMBOL !" << endl;

        if (sym && sym->parent())
            out << "\t" << sym->parent()->name() << endl;
        else
            out << endl;
    }
    out.close();
    //AST
    //dump functions body
    FILE *f;
    fopen_s(&f, symFile, "a");
    for (auto sym : _scriptScope.symbols())
    {
        if (sym->is<FunctionSymbol>())
        {
            fprintf_s(f, "-------------------------------\n");
            FunctionSymbol& fn = sym->as<FunctionSymbol>();
            fprintf_s(f, "function: %s\n", fn.name());
            ExprList::Dump(fn.body(), f);
        }
    }
    fclose(f);
}


trill::Compiler::Compiler(Context* ctx)
    : _context(ctx),
      _nErrors(0),
      _nWarnings(0),
      _nextSymbolId(0),
      _nextLabelId(0),
      _mainFunction(nullptr, Location::empty()),
      _exprChecker(this),
      _exprFold(this)
{
    _mainFunction.setType(GetBaseType(TS_VAR));
    _mainFunction.setParent(nullptr);
    _mainFunction.setName(_context->getRuntime()->newName("$main", 5));
    _mainFunction.setId(_nextSymbolId++);
    _mainFunction.setPrivate(true);
    _mainFunction.setMain(true);
    //TODO: here we can push script command line
    _mainFunction.setnArgs(0);

}


trill::Compiler::~Compiler()
{
}

bool trill::Compiler::compileSource(SourceInfo* srcInfo, Scope* global)
{
    _source = srcInfo;

    //enter global function
    _scriptScope.clear();
    _scriptScope.setParent(global);
    _currentScope = &_scriptScope;

    _currentScope->addSymbol(&_mainFunction);
    _functionsStack.push_back(&_mainFunction);

    Parser parser(this);
    parser.ParseSource(_source->source, _source->bsize);

    return !errorReporter()->HasErrors();
}

void trill::Compiler::ReportError(char *message)
{
    errorReporter()->ReportError(message);
}

void trill::Compiler::ReportError(Location& location, char *message, ...)
{
    char *msg = (char *)alloca(1024);
    va_list arglist;
    va_start(arglist, message);
    vsprintf_s(msg, 1024, message, arglist);
    errorReporter()->ReportError(_source->filename, location, msg);
    va_end(arglist);
}

void trill::Compiler::ReportSyntaxError(Location& location, char *message)
{
    char *msg = (char *)alloca(1024);
    va_list arglist;
    va_start(arglist, message);
    strcpy_s(msg, 1024, " Syntax Error: ");
    vsprintf_s(msg + strlen(msg), 1000, message, arglist);
    errorReporter()->ReportError(_source->filename, location, msg);
    va_end(arglist);
}

void trill::Compiler::ReportWarning(Location& location, char *message, ...)
{
    char *msg = (char *)alloca(1024);
    va_list arglist;
    va_start(arglist, message);
    vsprintf_s(msg, 1024, message, arglist);
    errorReporter()->ReportWarning(_source->filename, location, msg);
    va_end(arglist);
}

char* trill::Compiler::newString(const char *buffer, int len)
{
    unsigned hash = ::GetHash(buffer, len);
    if (_strings.find(hash) == _strings.end())
    {
        _strings[hash] = std::string(buffer, len);
    }
    return  (char*)_strings[hash].data();
}
char* trill::Compiler::newName(const char* name, int len)
{
    return _context->getRuntime()->newName(name, len);
}

ValueExpr * trill::Compiler::newValueExpr(ValueExprSubKind subkind, Value& value, Location& loc)
{
    ValueExpr* e = new ValueExpr(subkind, value);
    e->setLocation(loc);
    return e;
}

VariableSymbol* trill::Compiler::newVariableSymbol(char* name, Type* type, Location& loc)
{
    VariableSymbol* v = new VariableSymbol(type, loc);
    v->setName(name);
    //TODO: this might need to be unique per runtime
    v->setId(_nextSymbolId++);
    return v;
}

FunctionSymbol* trill::Compiler::newFunctionSymbol(char* name, Type* type, Location& loc)
{
    FunctionSymbol* f = new FunctionSymbol(type, loc);
    f->setName(name);
    f->setId(_nextSymbolId++);
    return f;
}

trill::LabelSymbol* trill::Compiler::newLabelSymbol(char *name)
{
    LabelSymbol *l = new LabelSymbol();
    static char label[120];
    sprintf_s(label, sizeof(label), "%s_%d", name, _nextLabelId);
    l->setName(newString(label, strlen(label)));
    l->setId(_nextLabelId++);
    return l;
}

LabelExpr* trill::Compiler::newLabelExpr(LabelSymbol* label)
{
    LabelExpr *e = new LabelExpr(label);
    e->setLocation(Location::empty());
    return e;
}

ConstSymbol* trill::Compiler::newConstSymbol(ValueExpr*v, Type* type, Location& loc)
{
    ConstSymbol* sym = new ConstSymbol(type, loc);
    sym->setValue(v);
    return sym;
}


LabelExpr* trill::Compiler::newLabel(char *name)
{
    return newLabelExpr(newLabelSymbol(name));
}


SymbolExpr* trill::Compiler::newSymbolExpr(Symbol *symbol, Location& loc)
{
    SymbolExpr* e = new SymbolExpr(symbol);
    e->setLocation(loc);
    return e;

}

BinaryExpr* trill::Compiler::newBinaryExpr(BinExprSubKind subkind, Expr* left, Expr*right, Location&loc)
{
    BinaryExpr * e = new BinaryExpr(subkind, left, right);
    e->setLocation(loc);
    return e;

}

CallExpr* trill::Compiler::newCallExpr(Expr* func, Expr* args, Location& loc)
{
    CallExpr* e = new CallExpr(func, args);
    e->setLocation(loc);
    return e;

}

ExprList* trill::Compiler::newExprList(Expr* head, Expr*tail)
{
    //make sure head->tail are linked
    if (head && tail && head != tail)
    {
        Expr* p = head;
        while (p->next()) p = p->next();
        p->setNext(tail);
        p = tail;
        while (p->next()) p = p->next();
        tail = p;
    }
    ExprList* e = new ExprList(head, tail);
    return e;
}

UnaryExpr* trill::Compiler::newUnaryExpr(UnaryExprSubKind subkind, Expr* expr, Location& loc)
{
    UnaryExpr *e = new UnaryExpr(subkind, expr);
    e->setLocation(loc);
    return e;
}

AssignExpr* trill::Compiler::newAssignExpr(Expr* left, Expr*right, Location& loc)
{
    AssignExpr* e = new AssignExpr(left, right);
    e->setLocation(loc);
    return e;

}

ArrayExpr* trill::Compiler::newArrayExpr(Expr* fields, Location& loc)
{
    ArrayExpr* e = new ArrayExpr(fields);
    e->setLocation(loc);
    return e;
}

ReturnExpr* trill::Compiler::newReturnExpr(Expr*expr, Location& loc)
{
    ReturnExpr *r = new ReturnExpr(expr);
    r->setLocation(loc);
    return r;
}


//look in current scope and it's parent  if bnestedScopes is true
Symbol* trill::Compiler::findSymbol(char *name, bool bnestedScopes /* = true*/)
{
    Symbol * sym = nullptr;
    Scope* scope = _currentScope;
    do
    {
        sym = scope->findSymbol(name);
        scope = scope->parent();

    }
    while (bnestedScopes && scope != nullptr && sym == nullptr);

    return sym;
}

void trill::Compiler::enterScope()
{
    LocalScope *scope = new LocalScope(_currentScope);
    _currentScope = scope;
}

void trill::Compiler::leaveScope()
{
    ASSERT(_currentScope != &_scriptScope);
    Scope* parent = _currentScope->parent();
    //if (_currentScope != &_scriptScope)
    delete _currentScope;
    _currentScope = parent;
}

void trill::Compiler::pushFunction(FunctionSymbol* fn)
{
    _functionsStack.push_back(fn);
}

FunctionSymbol* trill::Compiler::popFunction()
{
    ASSERT(_functionsStack.size() > 0);
    FunctionSymbol *f = _functionsStack.back();
    _functionsStack.pop_back();
    return f;
}

FunctionSymbol* trill::Compiler::currentFunction()
{
    return _functionsStack.back();
}

//declare symbol/variables in the current scope and update type specifier
void trill::Compiler::declareSymbol(SymbolExpr& expr, Type* type)
{
    ASSERT(expr.symbol()->is<VariableSymbol>());
    //check redefinition variables
    VariableSymbol&  sym = expr.symbol()->as<VariableSymbol>();
    //new symbol not declared yet
    if (sym.scope() == nullptr)
    {
        SymbolStorage ss = _currentScope == &_scriptScope ? SS_GLOBAL : SS_LOCAL;
        sym.setStorage(ss);
        if (ss == SS_LOCAL)
            sym.setIndex(currentFunction()->addLocal(&sym));
        sym.setType(type);
        sym.setParent(currentFunction());
        sym.setScope(_currentScope);
        _currentScope->addSymbol(expr.symbol());
    }
    else if (sym.scope() != _currentScope)
    {
        //declared in other scope , declare it in current
        VariableSymbol* var = newVariableSymbol(sym.name(), type, expr.location());
        SymbolStorage ss = _currentScope == &_scriptScope ? SS_GLOBAL : SS_LOCAL;
        var->setStorage(ss);
        //add it to current function if local
        if (ss == SS_LOCAL)
            var->setIndex(currentFunction()->addLocal(var));

        var->setParent(currentFunction());
        var->setScope(_currentScope);
        _currentScope->addSymbol(var);
        //update expression
        expr.setSymbol(var);

    }
    //already been defined
    else
    {
        ReportError(expr.location(), "'%s' redefinition\n\t%s(%d:%d) : see declaration of '%s'", sym.name(),
                    _source->filename, sym.line(), sym.column(), sym.name());
    }

    /*if (sym.storage() == SS_LOCAL)
    {
        sym.setIndex(currentFunction()->addLocal(expr.symbol()));
    }*/

    //TODO: other symbols, functions, classes etc
}

void trill::Compiler::onParsingFinished(Expr *body)
{
    //here we should be in main
    ASSERT(currentFunction() == &_mainFunction);
    ASSERT(_currentScope == &_scriptScope);

    //add a label for main
    if (body)
    {
        //LabelSymbol *label = newLabelSymbol(_mainFunction.name());
        // LabelExpr *labelExpr = newLabel(_mainFunction.name());
        //labelExpr->setNext(body);
        //body = labelExpr;
    }
    _mainFunction.setBody(body);

}

JumpExpr* trill::Compiler::newJumpExpr(Expr* label)
{
    ASSERT(label->is<LabelExpr>());
    JumpExpr *e = new JumpExpr(label->as<LabelExpr>().label());
    e->setLocation(Location::empty());
    return e;
}

JTrueExpr* trill::Compiler::newJTrueExpr(BinExprSubKind subkind, Expr* label)
{
    ASSERT(label->is<LabelExpr>());
    JTrueExpr *e = new JTrueExpr(subkind, label->as<LabelExpr>().label());
    e->setLocation(Location::empty());
    return e;
}

ArgExpr* trill::Compiler::newArgExpr(Expr*arg)
{
    ArgExpr * e = new ArgExpr(arg);
    e->setLocation(arg->location());
    return e;
}



FunctionSymbol* trill::Compiler::declareFunction(char *name, Type* type, Location& loc)
{
    //Lookup/create function symbol
    Symbol* sym = findSymbol(name, false);
    FunctionSymbol *fn = nullptr;
    //not found
    if (sym == nullptr)
    {
        fn = newFunctionSymbol(name, type, loc);
        fn->setScope(_currentScope);
        fn->setParent(currentFunction());
        fn->setPrivate(false);
        _currentScope->addSymbol(fn);
        //build prototype and declare formal parameters

    }
    else
    {
        //redefinition
        ReportError(loc, "function redefinition, see declaration at: %s(%d:%d)",
                    _source->filename, ((SourceSymbol*)sym)->line(), ((SourceSymbol*)sym)->column());
    }
    //TODO: here we can check if it's a function and handle overloaded functions
    return fn;
}

//do type check and l-value check, update info
void trill::Compiler::checkExpression(Expr *e, ExprInfo& info)
{
    _exprChecker.checkExpression(e, info);

}

void trill::Compiler::checkExpression(Expr* e)
{
    ExprInfo info;
    _exprChecker.checkExpression(e, info);
}

Runtime* trill::Compiler::getRuntime() const
{
    return getContext()->getRuntime();
}

ArrayType* trill::Compiler::newArrayType(Type *derivedType, unsigned nelement /*= 0*/)
{
    return _context->newArrayType(derivedType, nelement);
}

ErrorReporter* trill::Compiler::errorReporter()
{
    return getRuntime()->GetErrorReporter();
}

TypeList* trill::Compiler::newTypeList(Type* t /*= nullptr*/)
{
    return _context->newTypeList(t);
}

Expr* trill::Compiler::foldExpression(Expr* e)
{
    return _exprFold.Fold(e);
}

void trill::Compiler::declareConst(char* name, ValueExpr* value, Location& loc)
{
    ASSERT(value->type() != nullptr);
    SourceSymbol *sym = (SourceSymbol*)findSymbol(name, false);

    if (sym != nullptr)
        ReportError(loc, "constant redefinition, see declaration at: %s(%d:%d)",
                    _source->filename, sym->line(), sym->column());
    else
    {
        sym = newConstSymbol(value, value->type(), loc);
        sym->setName(name);
        sym->setParent(currentFunction());
        sym->setScope(_currentScope);
        _currentScope->addSymbol(sym);
    }
}





/*
bool trill::Compiler::checkLeftValue(Expr *e)
{
    ASSERT(e->is<AssignExpr>());

    if (!doLValueCheck(e->as<AssignExpr>()))
    {
        ReportError(e->location(), "l-value expected");
        return false;
    }
    return true;
}

bool trill::Compiler::doLValueCheck(AssignExpr& expr)
{
    Expr* left = expr.left();
    bool bSuccess = false;
    switch (left->kind())
    {
    case E_SYMBOL:
        bSuccess = true;
        break;
    case E_BINARY:
    {
        BinaryExpr& bin = left->as<BinaryExpr>();
        if (bin.subKind() == ES_DOT || bin.subKind() == ES_INDEX)
            bSuccess = true;
    }
    break;
    default:
        bSuccess = false;
        break;
    }
    if (expr.right()->is<AssignExpr>())
    {
        return bSuccess && doLValueCheck(expr.right()->as<AssignExpr>());
    }
    return bSuccess;

}

*/








