//////////////////////////////////////////////////////////////////////////
//
// PROJECT: Trill scripting language
// FILE:  Parser.h
// PURPOSE:
// DATE: 2015/08/19
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#pragma once

namespace trill {

    struct Token;
    struct Compiler;
    struct Expr;
    struct ExprList;

    class Parser
    {
    public:
        Parser(Compiler* compiler);
        ~Parser();
        void ParseSource(char *src, unsigned bsize = 0);

    private:
        Compiler* _compiler;

        //Parsing callbacks
        Expr*   OnPrimary(Token &tk);
        Expr*   OnIdentifier(Token &tk);
        Expr*   OnDotExpr(Expr *expr, Token& ident);
        Expr*   OnCall(Expr *fn, Expr* args);

        ExprList*   OnArgList(ExprList*, Expr* arg);
        ExprList*   OnArg(Expr* arg);

        Expr*   OnIndex(Expr *arry, Expr *index);
        Expr*   OnUnary(UnaryExprSubKind subKind, Expr *expr);
        Expr*   OnBinary(BinExprSubKind subKind, Expr *left, Expr* right);
        Expr*   OnAssign(Expr *left, Expr* right);
        Expr*   OnArrayExpr(Expr *fileds, Location &location);
        Expr*   OnConstExpr(Token& ident, Expr* val);

        //statements
        Expr*   OnExprStatement(Expr *expr);
        Expr*   OnIfStatement(Expr* cond, Expr* trueBlock);
        Expr*   OnIfStatement(Expr* cond, Expr* trueBlock, Expr* falseBlock);
        void    OnBlockBegin();
        Expr*   OnBlockEnd(Expr *list);
        Expr*   OnArgDecl(Token& ident);
        void    OnFunctionProto(Token &ident, Expr* args);
        void    OnFunctionEnd(Expr* body);
        Expr*   OnReturnStatement(Expr*, Location& kloc);
        //loops
        Expr*   OnBreak(Token& tk);
        Expr*   OnContinue(Token& tk);
        void    OnWhileLoopBegin(Token& tk);
        Expr*   OnWhileLoop(Expr* cond, Expr* block);
        //for loop
        void    OnForLoopBegin();
        Expr*   OnForLoop(Expr* initBlock, Expr* condition, Expr* continue_block, Expr* block);

        //variable declaration
        void    OnVarDeclarationBegin(TypeSpec type);
        Expr*   OnVarDeclaration(Expr *var);
        void    OnArrayDeclaration(int n);
        void    OnProgramEnd(Expr *compound);

        Expr*  mergeExpr(Expr *first, Expr* last);
        Expr*  makeCJump(Expr *cond, Expr *trueLabel, Expr* falseLabel);
        Expr*  makeJTrue(Expr *cond, Expr *label);

        Type*   _currentType;

        //loops labels
        std::vector<LabelExpr*> _continueLabels;
        std::vector<LabelExpr*> _breakLabels;
        //for loop scope flag
        int    _nforLoopScope;

        //Lemon stuff, implemented in grammer.y
        void *pLemon;
        void InitLemon();
        void FreeLemon();
        void Parse(int /*TokenID*/, Token&);
        void yy_reduce(int yyruleno /* Number of the rule by which to reduce */);
        void yy_syntax_error(Token&);

    };

}