//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Expr.cpp
// PURPOSE:
// DATE: 2015/03/08
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Symbol.h"
#include "Expr.h"
#include "../internal.h"


using namespace trill;

#if 0
FILE *f;
virtual void Visit(Expr * ex)
{
    if(ex == NULL)
        return;

    /**/if(ex->left != NULL)
        Visit(ex->left);
    if(ex->right != NULL)
        Visit(ex->right);

    char name[80];
    switch(ex->kind)
    {
    case E_BINARY:
        sprintf_s(name, 80, "BINARY");
        break;
    case E_CONSTANT:
        sprintf_s(name, 80, "CONSTANT");
        break;
    case E_SYMBOL  :
        sprintf_s(name, 80, "SYMBOL %s", ex->symbol->name);
        break;
    case E_ASSIGN:
        sprintf_s(name, 80, "ASSIGN");
        break;
    case E_CALL:
        sprintf_s(name, 80, "CALL  -> %s", ex->symbol->name);
        break;
    case E_LABEL:
        sprintf_s(name, 80, "      -> %s", ex->symbol->name);
        break;
    case E_JTRUE:
        sprintf_s(name, 80, "JTRUE -> %s", ex->symbol->name);
        break;
    case E_JUMP:
        sprintf_s(name, 80, "JUMP  -> %s", ex->symbol->name);
        break;
    //case E_JFALSE:   sprintf_s(name,80,"E_JFALSE -> %s",ex->symbol->name);   break;
    default:
        sprintf_s(name, 80, "%s", " UNKNOWN!!");
        break;
    }
    fprintf(f, "\t%-10d %-12s\n", ex->id, name);

}
#endif
struct ExprDumper : public ExprVisitor
{
    FILE* out_;
    int indent_;
    ExprDumper(FILE* out) : out_(out), indent_(0) {}

    void indent(int count)
    {
        indent_ += count;
    }
    void unindent(int count)
    {
        indent_ -= count;
    }
    void padd()
    {
        for (int i = 0; i < indent_; i++)
        {
            fputc(' ', out_);
        }
    }
    void prin_loc(Expr *e)
    {
        fprintf(out_, "(%d:%d) ", e->location().lineno, e->location().column);
    }

    virtual void OnBinray(BinaryExpr& e)
    {
        padd();
        prin_loc(&e);
        fprintf(out_, "BINARY\n");
        indent(4);
        Expr::Accept(e.left(), this);
        Expr::Accept(e.right(), this);
        unindent(4);
    }

    virtual void OnAssign(AssignExpr& e)
    {
        padd();
        prin_loc(&e);
        fprintf(out_, "ASSIGN\n");
        indent(4);
        Expr::Accept(e.left(), this);
        Expr::Accept(e.right(), this);
        unindent(4);
    }

    virtual void OnValue(ValueExpr& e)
    {
        padd();
        if (e.isString())
            fprintf(out_, "VALUE(%s)\n", e.str());
        else if (e.isFloat())
            fprintf(out_, "VALUE(%f)\n", e.Float());
        else
            fprintf(out_, "VALUE(%d)\n", e.value().Int());
    }

    virtual void OnUnary(UnaryExpr& e)
    {
        padd();
        fprintf(out_, "UNARY\n");
    }

    virtual void OnArray(ArrayExpr& e)
    {
        padd();
        fprintf(out_, "ARRAY\n");
    }

    virtual void OnSymbol(SymbolExpr& e)
    {

        padd();
        fprintf(out_, "SYMBOL(%s)\n", e.symbol()->name());

    }

    virtual void OnJump(JumpExpr& e)
    {
        padd();
        fprintf(out_, "JUMP(%s)\n", e.label()->name());
    }

    virtual void OnJTrue(JTrueExpr& e)
    {
        padd();
        fprintf(out_, "JTRUE(%s)\n", e.label()->name());
    }

    virtual void OnCall(CallExpr& e)
    {
        padd();
        fprintf(out_, "CALL\n");
    }

    virtual void OnList(ExprList& e)
    {
        padd();
        fprintf(out_, "LIST\n");
    }
    virtual void OnArg(ArgExpr& e)
    {
        padd();
        fprintf(out_, "ARG\n");
    }

    virtual void OnLabel(LabelExpr& e)
    {
        padd();
        fprintf(out_, "LABEL(%s)\n", e.label()->name());
    }
    virtual void OnReturn(ReturnExpr& e)
    {
        padd();
        fprintf(out_, "RETURN\n");
        indent(4);
        Expr::Accept(e.expr(), this);
        unindent(4);
    }

};


void DumpExpr( Expr *expr, FILE *file )
{
#if 0
    ExprDumper dumper;
    dumper.f = file;
    TraverseExpr(expr, &dumper);
#endif
}




void trill::Expr::Accept(Expr *e, ExprVisitor* visitor)
{

    //for (Expr *e = expr; e != nullptr; e = e->next())
    {
        switch (e->kind())
        {
        case trill::E_UNKNOWN:
            INTERNAL_ERROR("Expression with unknown kind");
            break;
        case trill::E_ASSIGN:
            visitor->OnAssign(e->as<AssignExpr>());
            break;
        case trill::E_BINARY:
            visitor->OnBinray(e->as<BinaryExpr>());
            break;
        case trill::E_UNARY:
            visitor->OnUnary(e->as<UnaryExpr>());
            break;
        case trill::E_VALUE:
            visitor->OnValue(e->as<ValueExpr>());
            break;
        case trill::E_SYMBOL:
            visitor->OnSymbol(e->as<SymbolExpr>());
            break;
        case trill::E_CALL:
            visitor->OnCall(e->as<CallExpr>());
            break;
        case trill::E_ARG:
            visitor->OnArg(e->as<ArgExpr>());
            break;
        case trill::E_ARRAY:
            visitor->OnArray(e->as<ArrayExpr>());
            break;
        case trill::E_JUMP:
            visitor->OnJump(e->as<JumpExpr>());
            break;
        case trill::E_JTRUE:
            visitor->OnJTrue(e->as<JTrueExpr>());
            break;
        case trill::E_LIST:
            visitor->OnList(e->as<ExprList>());
            break;
        case trill::E_LABEL:
            visitor->OnLabel(e->as<LabelExpr>());
            break;
        case trill::E_RETURN:
            visitor->OnReturn(e->as<ReturnExpr>());
            break;
        default:
            INTERNAL_ERROR("shouldn't reach here");
            break;
        }
    }

}


void trill::Expr::Accept(Expr *expr, ExprSimpleVisitor* visitor)
{
    for (Expr *e = expr; e != nullptr; e = e->next())
    {
        visitor->Visit(e);
    }
}

void trill::Expr::Dump(Expr*e, FILE* out /*= stderr*/)
{
    ExprDumper dumper(out);
    ExprList::Accept(e, &dumper);

}

void trill::ExprList::Accept(Expr *expr, ExprVisitor* visitor)
{
    for (Expr *e = expr; e != nullptr; e = e->next())
    {
        switch (e->kind())
        {
        case trill::E_UNKNOWN:
            INTERNAL_ERROR("Expression with unknown kind");
            break;
        case trill::E_ASSIGN:
            visitor->OnAssign(e->as<AssignExpr>());
            break;
        case trill::E_BINARY:
            visitor->OnBinray(e->as<BinaryExpr>());
            break;
        case trill::E_UNARY:
            visitor->OnUnary(e->as<UnaryExpr>());
            break;
        case trill::E_VALUE:
            visitor->OnValue(e->as<ValueExpr>());
            break;
        case trill::E_SYMBOL:
            visitor->OnSymbol(e->as<SymbolExpr>());
            break;
        case trill::E_CALL:
            visitor->OnCall(e->as<CallExpr>());
            break;
        case trill::E_ARG:
            visitor->OnArg(e->as<ArgExpr>());
            break;
        case trill::E_ARRAY:
            visitor->OnArray(e->as<ArrayExpr>());
            break;
        case trill::E_JUMP:
            visitor->OnJump(e->as<JumpExpr>());
            break;
        case trill::E_JTRUE:
            visitor->OnJTrue(e->as<JTrueExpr>());
            break;
        case trill::E_LIST:
            visitor->OnList(e->as<ExprList>());
            break;
        case trill::E_LABEL:
            visitor->OnLabel(e->as<LabelExpr>());
            break;
        case trill::E_RETURN:
            visitor->OnReturn(e->as<ReturnExpr>());
            break;
        default:
            INTERNAL_ERROR("shouldn't reach here");
            break;
        }
    }
}

const char* trill::BinaryExpr::operatorName(BinaryExpr& e)
{
    const char* name = nullptr;
    switch (e.subKind())
    {
    case trill::ES_ADD:
        name = "+";
        break;
    case trill::ES_SUB:
        name = "-";
        break;
    case trill::ES_MUL:
        name = "*";
        break;
    case trill::ES_DIV:
        name = "/";
        break;
    case trill::ES_MOD:
        name = "%";
        break;
    case trill::ES_SHL:
        name = "<<";
        break;
    case trill::ES_SHR:
        name = ">>";
        break;
    case trill::ES_AND:
        name = "&";
        break;
    case trill::ES_XOR:
        name = "^";
        break;
    case trill::ES_OR:
        name = "|";
        break;
    case trill::ES_DOT:
        name = ".";
        break;
    case trill::ES_INDEX:
        name = "[]";
        break;
    case trill::ES_LOR:
        name = "||";
        break;
    case trill::ES_LAND:
        name = "&&";
        break;
    case trill::ES_LT:
        name = "<";
        break;
    case trill::ES_LTE:
        name = "<=";
        break;
    case trill::ES_GT:
        name = ">";
        break;
    case trill::ES_GTE:
        name = ">=";
        break;
    case trill::ES_EQ:
        name = "==";
        break;
    case trill::ES_NEQ:
        name = "!=";
        break;
    case trill::ES_IN:
        name = "in";
        break;
    default:
        INTERNAL_ERROR("Unknown subkind");
        break;
    }
    return name;
}
