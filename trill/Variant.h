//////////////////////////////////////////////////////////////////////////
//
// PROJECT:
// FILE:  Variant.h
// PURPOSE:
// DATE: 2015/08/23
// NOTES:
//
//////////////////////////////////////////////////////////////////////////

#ifndef Variant_h__
#define Variant_h__

#include "Types.h"

namespace trill {

    struct Variant;
    struct Context;



    //this must much type specifier
    enum VariantKind : int
    {
        VK_UNDEFINED = -1, //undefined holding nothing
        VK_NULL   = 0,
        VK_CHAR   = TS_CHAR,
        VK_INT    = TS_INT,
        VK_FLOAT  = TS_FLOAT,
        //reference types start from here
        VK_STRING = TS_STRING, //const string object
        VK_OBJECT = TS_VAR, // instance of a class / Array,map..etc

    };

    //RunTimeValue wrapper
    struct Variant
    {

        inline Variant()
        {
            kind_ = VK_UNDEFINED;
            v.i = 0;
        }
        explicit Variant(void *p)
        {
            kind_ = VK_OBJECT;
            v.ptr = p;
        }

        inline const VariantKind kind() const
        {
            return kind_;
        }
        inline bool isReference() const
        {
            return kind_ > VK_STRING;
        }
        inline bool isNull() const
        {
            return kind_ == VK_NULL;
        }
        inline bool isUndefined() const
        {
            return kind_ == VK_UNDEFINED;
        }
        inline bool isChar() const
        {
            return kind_ == VK_CHAR;
        }
        inline bool isInt() const
        {
            return kind_ == VK_INT;
        }
        inline bool isFloat() const
        {
            return kind_ == VK_FLOAT;
        }
        inline bool isString() const
        {
            return kind_ == VK_STRING;
        }
        inline bool isObject() const
        {
            return isReference();
        }
        inline char Char() const
        {
            return v.c;
        }
        inline int Int() const
        {
            return v.i;
        }
        inline float Float() const
        {
            return v.f;
        }
        inline const char * Str() const
        {
            return v.s;
        }
        inline void* Ptr() const
        {
            return v.ptr;
        }

        //TODO: implement other getters


    protected:
        VariantKind kind_;
        union
        {
            char    c;
            int     i;
            float   f;
            const char *s;
            void*   ptr;
        } v;
    };

    static_assert(sizeof(Variant) == 8, "Variant size should be 8 bytes");


    //Native function

    typedef bool(*NativeFunctionPtr) (Context* ctx, Variant* args, unsigned argc);

    struct CallArgs
    {
        CallArgs(Variant* vp, unsigned argc)
            : _vp(vp),
              _argc(argc)
        {}
        void setReturnValue(void *p)
        {
            _vp[_argc] = Variant(p);
        }
        const Variant& operator[](unsigned int index)
        {
            ASSERT(index < _argc);
            return _vp[index];
        }
    private:
        unsigned _argc;
        Variant* _vp;
    };



}



#endif // Variant_h__